# Introduction
MaTcheDD is a case study project I created to test a clean architecture approach towards Unity3D game developement.
It is build upon a custom made MVC platform and employs a TDD methodology. I choose to experiment with (unit)testing in Unity3D so I tried to employ TDD where possible. Before this project I was rather new to Unit testing in Unity3D.

# Lessons Learned
- As expected, proper unit testing in Unity3D is quite hard without removing dependencies to Monobehaviour. Without the MVC abstraction for the business logic of the app testing would have been very difficult.
- For me, this will be the first, and last project where I used RunTime tests. On the next greenfield project I will most likely create a custom implementation of runtime tests that simulate the Unity3D event loop. The problem with the runtime tests in its current state is that they are IEnumerators that, can possibly, 'overlap'. Meaning test `x` can impact the next test `y` removing the expectation that the tests are run in isolation.
- Singleton-monobehaviours really increase difficulty during testing. I ended up creating a Mock canvas that resets the instance each time it is used during testing.
- I tried to implement the app without `models` where I could. I tried to encapsulate all business login in the Usecase objects and keep state in Unity. Next time, I will use proper models to maintain state and use a `Humble Object Pattern` for the View. I chose this approach for performance reasons.
- Unit testing as much as possible made adding functionality really easy.
- Getting Gitlab's CI to run the `runtime tests` properly is difficult. (yeah, they did not succeed even once, yet they do in the editor, both in the editor as a standalone player)

# PlayStore
https://play.google.com/store/apps/details?id=com.HamerSoft.MaTcheDD

# Questions?
Yeah... I did not find the time to properly document everything yet.
If you have any questions or comments, feel free to contact me.

# Setup
1. Clone this repository
2. Run `git submodule init`
3. Run `git submodule update`   