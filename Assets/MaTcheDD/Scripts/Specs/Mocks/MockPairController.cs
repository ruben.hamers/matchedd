using HamerSoft.UnityMvc;
using MaTcheDD.Controllers;

namespace MaTcheDD.Specs.Mocks
{
    public class MockPairController : PairController
    {
        public void MockStarted(View view, object[] args)
        {
            OnStarted(view, args);
        }
    }
}