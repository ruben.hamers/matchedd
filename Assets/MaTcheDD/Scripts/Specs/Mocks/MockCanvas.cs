using System.Reflection;
using HamerSoft.Core.Singletons;

namespace MaTcheDD.Specs.Mocks
{
    public class MockCanvas : HamerSoft.Core.Singletons.Canvas
    {
        protected override void Awake()
        {
            //base.Awake();
            typeof(AbstractSingleton<Canvas>).GetField("_instance", BindingFlags.Static | BindingFlags.NonPublic)
                .SetValue(this, this);
        }

        public void CallAwake()
        {
            Awake();
        }
    }
}