using HamerSoft.UnityMvc;
using MaTcheDD.Controllers;

namespace MaTcheDD.Specs.Mocks
{
    public class MockPlayAreaController : PlayAreaController
    {

        public void MockStarted(View view, object[] args)
        {
            OnStarted(view, args);
        }

    }
}