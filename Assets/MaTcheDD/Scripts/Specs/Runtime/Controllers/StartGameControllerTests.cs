using System;
using System.Collections;
using System.Linq;
using System.Reflection;
using MaTcheDD.Controllers;
using MaTcheDD.Specs.Mocks;
using MaTcheDD.Views;
using MaTcheDD.Views.Menu;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;
using UnityEngine.UI;

namespace MaTcheDD.Specs.Runtime
{
    public class MockStartGameController : StartGameController
    {
        
        public void InvokeGameStarted(StartGameView view)
        {
            typeof(StartGameController)
                .GetMethod("ViewOnGameStarted", BindingFlags.Instance | BindingFlags.NonPublic)
                .Invoke(this, new object[2] {view, new object[1]{"3"}});
        }
        public void InvokeBack(StartGameView view)
        {
            typeof(StartGameController)
                .GetMethod("ViewOnBackPressed", BindingFlags.Instance | BindingFlags.NonPublic)
                .Invoke(this, new object[2] {view, new object[0]});
        }

        protected override void LoadGame()
        {
            GameLoaded = true;
        }

        public bool GameLoaded { get; set; }
    }

    public class StartGameControllerTests : AbstractControllerTest<MockStartGameController, StartGameView>
    {
        private MockCanvas _mockCanvas;
        protected override bool SpawnDefaultView => false;

        protected override MockStartGameController NewController()
        {
            return new MockStartGameController();
        }

        public override void Setup()
        {
            base.Setup();
            _mockCanvas = new GameObject("Canvas").AddComponent<MockCanvas>();
            View = Controller.SpawnView(_mockCanvas.transform,null ,new int[4]{3, 4, 5, 6});
        }
        [UnityTest]
        public IEnumerator Upon_Init_The_DropDown_Values_Are_Set()
        {
            yield return new WaitForEndOfFrame();
            var dropDown = View.GetComponentInChildren<Dropdown>();
            int[] tracks = dropDown.options.Select(op => System.Convert.ToInt32(op.text)).OrderBy(i=>i).ToArray();
            Assert.AreEqual(new []{3, 4, 5, 6}, tracks);
            yield return new WaitForEndOfFrame();
        }
        [UnityTest]
        public IEnumerator When_StartGameButton_Is_Pressed_The_Track_Amount_Is_Set()
        {
            yield return new WaitForEndOfFrame();
            Controller.InvokeGameStarted(View);
            Assert.AreEqual(3,Main.GetPlugin<MatchedPlugin>().GetTracks);
            yield return new WaitForEndOfFrame();
        }
        
        [UnityTest]
        public IEnumerator When_StartGameButton_Is_Pressed_The_Game_Is_Loaded()
        {
            yield return new WaitForEndOfFrame();
            Controller.InvokeGameStarted(View);
            Assert.True(Controller.GameLoaded);
            yield return new WaitForEndOfFrame();
        }
        
        [UnityTest]
        public IEnumerator When_Back_Is_Pressed_The_StartGame_View_Is_Destroyed()
        {
            Controller.InvokeBack(View);
            yield return new WaitForEndOfFrame();
            Assert.Catch(() =>
            {
                var test = View.name;
            });
            yield return new WaitForEndOfFrame();
        }

        [UnityTest]
        public IEnumerator When_Back_Is_Pressed_The_MenuView_View_Is_Shown()
        {
            Controller.InvokeBack(View);
            Assert.NotNull(_mockCanvas.GetComponentInChildren<MenuView>());
            yield return new WaitForEndOfFrame();
        }
    }
}