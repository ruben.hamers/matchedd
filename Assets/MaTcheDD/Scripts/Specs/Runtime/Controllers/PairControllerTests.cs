using System.Collections;
using System.Reflection;
using MaTcheDD.Lines;
using MaTcheDD.Specs.Mocks;
using MaTcheDD.Views;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace MaTcheDD.Specs.Runtime
{
    public class PairControllerTests : AbstractControllerTest<MockPairController, PairView>
    {
        protected override MockPairController NewController()
        {
            return new MockPairController();
        }

        [UnityTest]
        public IEnumerator When_PairController_Starts_It_Will_Setup_The_Pairs_StaticLines()
        {
            yield return SetupViewsAndStartController(out var pv);
            Assert.IsTrue(pv.GetLine().GetStart() == Vector3.zero);
            Assert.IsTrue(pv.GetLine().GetEnd() != Vector3.zero);
        }

        [UnityTest]
        public IEnumerator When_Static_Lines_Are_Spawned_The_Thickness_Is_Taken_From_The_MatchedPlugin()
        {
            yield return SetupViewsAndStartController(out var pv);
            FieldInfo fi = typeof(Line).GetField("_thickness", BindingFlags.NonPublic | BindingFlags.Instance);
            float thickness = (float) fi.GetValue(pv.GetLine());
            Assert.AreEqual(Main.GetPlugin<MatchedPlugin>().GetLineThickness, thickness);
        }

        [UnityTest]
        public IEnumerator When_Static_Lines_Are_Spawned_The_Color_Is_Taken_From_The_MatchedPlugin()
        {
            yield return SetupViewsAndStartController(out var pv);
            yield return new WaitForSeconds(1);
            FieldInfo fi = typeof(Line).GetField("_color", BindingFlags.NonPublic | BindingFlags.Instance);
            Gradient color = (Gradient) fi.GetValue(pv.GetLine());
            Assert.AreEqual(Main.GetPlugin<MatchedPlugin>().GetLineColor, color);
        }

        private object SetupViewsAndStartController(out PairView pv)
        {
            GameObject parent = new GameObject();
            ColumnView cv = Controller.SpawnCustomView<ColumnView>(parent.transform);
            RectTransform cvTransform = cv.GetComponent<RectTransform>() ?? cv.gameObject.AddComponent<RectTransform>();
            pv = Controller.SpawnView(cvTransform);
            pv.transform.localPosition = new Vector3(0, -930, 0);
            Controller.SetMain(Main);
            Controller.MockStarted(pv, new[] {pv.GetComponentInChildren<StaticLine>()});
            return new WaitForEndOfFrame();
        }
    }
}