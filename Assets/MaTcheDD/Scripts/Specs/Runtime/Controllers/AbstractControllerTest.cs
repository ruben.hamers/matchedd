﻿using System;
using HamerSoft.Core;
using HamerSoft.UnityMvc;
using NUnit.Framework;
using NUnit.Framework.Constraints;
using UnityEngine;
using Object = UnityEngine.Object;

namespace MaTcheDD.Specs.Runtime
{
    public abstract class AbstractControllerTest<C, V> where C : Controller where V : BaseView
    {
        protected C Controller;
        protected V View;
        protected Main Main;
        protected virtual bool SpawnDefaultView => true;

        [SetUp]
        public virtual void Setup()
        {
            GetMain();
            Controller = NewController();
            GameObject root = new GameObject("Root");
            if (SpawnDefaultView)
                try
                {
                    View = new GameObject(typeof(V).Name).AddComponent<V>();
                    View.transform.SetParent(root.transform);
                }
                catch (Exception)
                {
                    View = null;
                }

            Controller.SetMain(Main);
            Controller.RegisterView(View);
        }

        protected void GetMain()
        {
            Main = Main.Load();
            Main.Init();
        }

        protected abstract C NewController();

        [TearDown]
        public virtual void TearDown()
        {
            Controller?.Dispose();
            if (View)
                Object.DestroyImmediate(View.gameObject);
        }
    }
}