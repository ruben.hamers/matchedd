﻿using System.Collections;
using System.Linq;
using MaTcheDD.Entities;
using MaTcheDD.Specs.Mocks;
using MaTcheDD.Views;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.UI;

namespace MaTcheDD.Specs.Runtime
{
    public class PlayAreaControllerTests : AbstractControllerTest<MockPlayAreaController, PlayAreaView>
    {
        public override void Setup()
        {
            base.Setup();
            View.gameObject.AddComponent<DrawingLayer>();
            Main.GetPlugin<MatchedPlugin>().SetPlayer(new Player("Foo"));
        }

        [UnityTest]
        public IEnumerator When_Controller_Starts_The_Correct_Number_Of_Columns_Are_Spawned()
        {
            View.gameObject.AddComponent<HorizontalLayoutGroup>();
            Controller.MockStarted(View, new object[] {View.GetComponentInChildren<LayoutGroup>()});
            yield return new WaitForSeconds(1);
            Assert.AreEqual(Main.GetPlugin<MatchedPlugin>().GetTracks,
                View.GetComponentsInChildren<ColumnView>().Length);
        }

       [UnityTest]
        public IEnumerator When_Controller_Is_Started_Columns_Will_Be_Linked()
        {
            View.gameObject.AddComponent<HorizontalLayoutGroup>();
            Controller.MockStarted(View, new object[] {View.GetComponentInChildren<LayoutGroup>()});
            yield return new WaitForSeconds(1);
            Assert.IsTrue(CheckIfColumnsAreLinked());
        }

        [UnityTest]
        public IEnumerator When_Controller_Is_Started_The_Pair_Heads_Will_Be_Swapped()
        {
            View.gameObject.AddComponent<HorizontalLayoutGroup>();
            Controller.MockStarted(View, new object[] {View.GetComponentInChildren<LayoutGroup>()});
            yield return new WaitForSeconds(1);
            PairView[] pairs = View.GetComponentsInChildren<PairView>();
            Assert.True(pairs.Any(p => !p.IsMatched()));
        }

        private bool CheckIfColumnsAreLinked()
        {
            ColumnView[] columns = View.GetComponentsInChildren<ColumnView>();
            bool linked = true;
            for (int i = 0; i < columns.Length; i++)
            {
                if (i == 0)
                    linked = columns[i].GetRightAdjacent();
                else if (linked && i > 0)
                {
                    if (i < columns.Length - 1)
                        linked = columns[i].GetRightAdjacent() && columns[i].GetLeftAdjacent();
                    else if (i < columns.Length)
                        linked = columns[i].GetLeftAdjacent();
                }
            }

            return linked;
        }

        [UnityTest]
        public IEnumerator When_Columns_Are_Spawned_They_Will_Be_Aligned_And_Centered_CorrectLly()
        {
            var layoutGroup = View.gameObject.AddComponent<HorizontalLayoutGroup>();

            layoutGroup.GetComponent<RectTransform>().sizeDelta = new Vector2(1920, 1080);
            View.GetComponent<RectTransform>().sizeDelta = new Vector2(1920, 1080);
            Controller.MockStarted(View, new object[] {View.GetComponentInChildren<LayoutGroup>()});
            yield return new WaitForSeconds(1);
            float desiredSpacing =
                (1920 - (View.GetComponentInChildren<ColumnView>().GetComponentInChildren<LayoutElement>()
                             .preferredWidth * Main.GetPlugin<MatchedPlugin>().GetTracks)) /
                (Main.GetPlugin<MatchedPlugin>().GetTracks + 1);
            Assert.AreEqual(desiredSpacing, layoutGroup.spacing);
            Assert.AreEqual(desiredSpacing, layoutGroup.padding.left);
            Assert.AreEqual(desiredSpacing, layoutGroup.padding.right);
        }

        protected override MockPlayAreaController NewController()
        {
            return new MockPlayAreaController();
        }
    }
}