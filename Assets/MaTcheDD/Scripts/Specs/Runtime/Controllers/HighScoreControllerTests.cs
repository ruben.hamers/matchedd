using System.Collections;
using System.Reflection;
using MaTcheDD.Controllers;
using MaTcheDD.Entities;
using MaTcheDD.Entities.Repositories;
using MaTcheDD.Specs.Mocks;
using MaTcheDD.Views;
using MaTcheDD.Views.Menu;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Canvas = HamerSoft.Core.Singletons.Canvas;

namespace MaTcheDD.Specs.Runtime
{
    public class MockHighScoreController : HighScoresController
    {
        public void InvokeNormal(HighScoresView view)
        {
          var function  =  typeof(HighScoresController)
              .GetMethod("ViewOnNormalPressed", BindingFlags.Instance | BindingFlags.NonPublic);
                function.Invoke(this, new object[2] {view, new object[0]});
        }

        public void InvokeIntermediate(HighScoresView view)
        {
            typeof(HighScoresController)
                .GetMethod("ViewOnIntermediatePressed", BindingFlags.Instance | BindingFlags.NonPublic)
                .Invoke(this, new object[2] {view, new object[0]});
        }

        public void InvokeHard(HighScoresView view)
        {
            typeof(HighScoresController)
                .GetMethod("ViewOnHardPressed", BindingFlags.Instance | BindingFlags.NonPublic)
                .Invoke(this, new object[2] {view, new object[0]});
        }

        public void InvokeExtraHard(HighScoresView view)
        {
            typeof(HighScoresController)
                .GetMethod("ViewOnExtraHardPressed", BindingFlags.Instance | BindingFlags.NonPublic)
                .Invoke(this, new object[2] {view, new object[0]});
        }

        public void InvokeBack(HighScoresView view)
        {
            typeof(HighScoresController)
                .GetMethod("ViewOnBackPressed", BindingFlags.Instance | BindingFlags.NonPublic)
                .Invoke(this, new object[2] {view, new object[0]});
        }
    }

    public class HighScoreControllerTests : AbstractControllerTest<MockHighScoreController, HighScoresView>
    {
        private PlayerRepository _playerRepository;
        private SessionRepository _sessionRepository;
        private MockCanvas _canvas;
        private Player _playerFoo;
        private Player _playerBar;
        private Player _playerBaz;
        private Session _sessionFoo3;
        private Session _sessionFoo4;
        private Session _sessionBar4;
        private Session _sessionFoo5;
        private Session _sessionBar5;
        private Session _sessionBaz5;
        private Session _sessionFoo6;
        private Session _sessionBar6;
        private Session _sessionBaz6;
        private Session _sessionBaz26;

        protected override MockHighScoreController NewController()
        {
            return new MockHighScoreController();
        }

        protected override bool SpawnDefaultView => false;

        public override void Setup()
        {
            base.Setup();
            _canvas = new GameObject("Canvas").AddComponent<MockCanvas>();
            View = Controller.SpawnView(_canvas.transform);
            _playerRepository = Main.GetRepository<PlayerRepository>();
            _playerFoo = new Player("Foo");
            _playerBar = new Player("Bar");
            _playerBaz = new Player("Baz");
            _playerRepository.AddObject(_playerFoo);
            _playerRepository.AddObject(_playerBar);
            _playerRepository.AddObject(_playerBaz);

            _sessionRepository = Main.GetRepository<SessionRepository>();
            _sessionFoo3 = new Session(_playerRepository.GetPlayerByName("Foo").Id, 3);
            _sessionFoo3.AddRound();
            _sessionFoo4 = new Session(_playerRepository.GetPlayerByName("Foo").Id, 4);
            _sessionFoo4.AddRound();
            _sessionBar4 = new Session(_playerRepository.GetPlayerByName("Bar").Id, 4);
            _sessionBar4.AddRound();
            _sessionFoo5 = new Session(_playerRepository.GetPlayerByName("Foo").Id, 5);
            _sessionFoo5.AddRound();
            _sessionBar5 = new Session(_playerRepository.GetPlayerByName("Bar").Id, 5);
            _sessionBar5.AddRound();
            _sessionBaz5 = new Session(_playerRepository.GetPlayerByName("Baz").Id, 5);
            _sessionBaz5.AddRound();
            _sessionFoo6 = new Session(_playerRepository.GetPlayerByName("Foo").Id, 6);
            _sessionFoo6.AddRound();
            _sessionBar6 = new Session(_playerRepository.GetPlayerByName("Bar").Id, 6);
            _sessionBar6.AddRound();
            _sessionBaz6 = new Session(_playerRepository.GetPlayerByName("Baz").Id, 6);
            _sessionBaz6.AddRound();
            _sessionBaz26 = new Session(_playerRepository.GetPlayerByName("Baz").Id, 6);
            _sessionBaz26.AddRound();
            _sessionRepository.AddObject(_sessionFoo3);
            _sessionRepository.AddObject(_sessionBar4);
            _sessionRepository.AddObject(_sessionFoo4);
            _sessionRepository.AddObject(_sessionFoo5);
            _sessionRepository.AddObject(_sessionBar5);
            _sessionRepository.AddObject(_sessionBaz5);
            _sessionRepository.AddObject(_sessionFoo6);
            _sessionRepository.AddObject(_sessionBar6);
            _sessionRepository.AddObject(_sessionBaz6);
            _sessionRepository.AddObject(_sessionBaz26);
        }

        [UnityTest]
        public IEnumerator When_Normal_Is_Pressed_The_Normal_HighScores_Are_Shown()
        {
            yield return new WaitForEndOfFrame();
            Controller.InvokeNormal(View);
            Assert.LessOrEqual(1, View.GetComponentsInChildren<HighScoreRow>().Length);
            yield return new WaitForEndOfFrame();
        }

        [UnityTest]
        public IEnumerator When_Intermediate_Is_Pressed_The_Intermediate_HighScores_Are_Shown()
        {
            Controller.InvokeIntermediate(View);
            Assert.LessOrEqual(2, View.GetComponentsInChildren<HighScoreRow>().Length);
            yield return new WaitForEndOfFrame();
        }

        [UnityTest]
        public IEnumerator When_Hard_Is_Pressed_The_Hard_HighScores_Are_Shown()
        {
            Controller.InvokeHard(View);
            Assert.LessOrEqual(3, View.GetComponentsInChildren<HighScoreRow>().Length);
            yield return new WaitForEndOfFrame();
        }

        [UnityTest]
        public IEnumerator When_ExtraHard_Is_Pressed_The_ExtraHard_HighScores_Are_Shown()
        {
            Controller.InvokeExtraHard(View);
            Assert.LessOrEqual(4, View.GetComponentsInChildren<HighScoreRow>().Length);
            yield return new WaitForEndOfFrame();
        }

        [UnityTest]
        public IEnumerator When_Back_Is_Pressed_The_Highscore_View_Is_Destroyed()
        {
            Controller.InvokeBack(View);
            yield return new WaitForEndOfFrame();
            Assert.Catch(() =>
            {
                var test = View.name;
            });
            yield return new WaitForEndOfFrame();
        }

        [UnityTest]
        public IEnumerator When_Back_Is_Pressed_The_MainMenu_View_Is_Shown()
        {
            Controller.InvokeBack(View);
            Assert.NotNull(_canvas.GetComponentInChildren<MenuView>());
            yield return new WaitForEndOfFrame();
        }


        public override void TearDown()
        {
            _playerRepository.RemoveObject(ref _playerFoo);
            _playerRepository.RemoveObject(ref _playerBar);
            _playerRepository.RemoveObject(ref _playerBaz);
            _playerRepository.Flush();
            _sessionRepository.RemoveObject(ref _sessionFoo3);
            _sessionRepository.RemoveObject(ref _sessionBar4);
            _sessionRepository.RemoveObject(ref _sessionFoo4);
            _sessionRepository.RemoveObject(ref _sessionFoo5);
            _sessionRepository.RemoveObject(ref _sessionBar5);
            _sessionRepository.RemoveObject(ref _sessionBaz5);
            _sessionRepository.RemoveObject(ref _sessionFoo6);
            _sessionRepository.RemoveObject(ref _sessionBar6);
            _sessionRepository.RemoveObject(ref _sessionBaz6);
            _sessionRepository.RemoveObject(ref _sessionBaz26);
            _sessionRepository.Flush();
            base.TearDown();
        }
    }
}