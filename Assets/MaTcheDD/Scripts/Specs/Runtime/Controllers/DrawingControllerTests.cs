using System;
using System.Collections;
using System.Linq;
using System.Reflection;
using geniikw.DataRenderer2D;
using MaTcheDD.Controllers;
using MaTcheDD.Lines;
using MaTcheDD.Views;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.TestTools;

namespace MaTcheDD.Specs.Runtime
{
    public class MockDrawingController : DrawingController
    {
        public MatchedPlugin GetMatchedPlugin()
        {
            return typeof(DrawingController).GetField("_matchedPlugin", BindingFlags.Instance | BindingFlags.NonPublic)
                    .GetValue(this)
                as MatchedPlugin;
        }

        public void CallStart(DrawingView view, object[] args)
        {
            OnStarted(view, args);
        }

        public void CallOnDrag(DrawingView view, object[] args)
        {
            PointerDragged(view, args);
        }

        public void CallPointerUp(DrawingView view, object[] args)
        {
            PointerUp(view, args);
        }

        public void CallPointerDown(DrawingView view, object[] args)
        {
            PointerDown(view, args);
        }
    }

    public class MockDrawingView : DrawingView
    {
    }

    public class DrawingControllerTests : AbstractControllerTest<MockDrawingController, MockDrawingView>
    {
        protected override MockDrawingController NewController()
        {
            return new MockDrawingController();
        }

        protected override bool SpawnDefaultView => false;


        [Test]
        public void When_Main_Is_Set_The_Matched_Plugin_Is_Set()
        {
            Assert.NotNull(Controller.GetMatchedPlugin());
        }

        [UnityTest]
        public IEnumerator When_View_Is_Started_The_View_Is_Init()
        {
            DrawingView dv = Controller.SpawnView(null);
            yield return new WaitForEndOfFrame();
            var uiLine = dv.GetComponent<UILine>();
            Assert.AreEqual(Controller.GetMatchedPlugin().GetLineColor, uiLine.line.option.color);
        }

        [UnityTest]
        public IEnumerator When_PointerDown_Is_Called_A_New_Position_Is_Pushed_To_DynamicLine()
        {
            DrawingView dv = Controller.SpawnView(null);
            yield return new WaitForEndOfFrame();
            var uiLine = dv.GetComponent<UILine>();
            var pointCount = uiLine.line.Count;
            Controller.CallPointerDown(dv, new object[] {dv.GetComponentInChildren<DynamicLine>(), Vector2.one});
            Assert.Less(pointCount, uiLine.line.Count);
        }

        [UnityTest]
        public IEnumerator When_PointerDown_Is_Called_The_Given_Position_Is_Pushed_To_DynamicLine()
        {
            DrawingView dv = Controller.SpawnView(null);
            yield return new WaitForEndOfFrame();
            var uiLine = dv.GetComponent<UILine>();
            Controller.CallPointerDown(dv, new object[] {dv.GetComponentInChildren<DynamicLine>(), Vector2.one});
            yield return new WaitForEndOfFrame();
            Assert.AreEqual(new Vector3(1, 1, 0), uiLine.line.Pop().position);
        }

        [UnityTest]
        public IEnumerator
            When_Dragged_Is_Called_A_New_Position_Is_Pushed_To_DynamicLine_When_Greater_Than_AddDistance()
        {
            DrawingView dv = Controller.SpawnView(null);
            yield return new WaitForEndOfFrame();
            var uiLine = dv.GetComponent<UILine>();
            Controller.CallPointerDown(dv, new object[] {dv.GetComponentInChildren<DynamicLine>(), Vector2.one});
            var pointCount = uiLine.line.Count;
            Controller.CallOnDrag(dv,
                new object[] {dv.GetComponentInChildren<DynamicLine>(), Vector2.one, new Vector2(10, 10)});
            yield return new WaitForEndOfFrame();
            Assert.Less(pointCount, uiLine.line.Count);
        }

        [UnityTest]
        public IEnumerator
            When_Dragged_Is_Called_No_New_Position_Is_Pushed_To_DynamicLine_When_Smaller_Than_AddDistance()
        {
            DrawingView dv = Controller.SpawnView(null);
            yield return new WaitForEndOfFrame();
            var uiLine = dv.GetComponent<UILine>();
            Controller.CallPointerDown(dv, new object[] {dv.GetComponentInChildren<DynamicLine>(), Vector2.one});
            var pointCount = uiLine.line.Count;
            Controller.CallOnDrag(dv,
                new object[] {dv.GetComponentInChildren<DynamicLine>(), Vector2.one, new Vector2(2, 2)});
            yield return new WaitForEndOfFrame();
            Assert.AreEqual(pointCount, uiLine.line.Count);
        }

        [UnityTest]
        public IEnumerator When_Dragged_Is_Called_The_Current_Position_Is_Edited_When_Smaller_Than_AddDistance()
        {
            DrawingView dv = Controller.SpawnView(null);
            yield return new WaitForEndOfFrame();
            var uiLine = dv.GetComponent<UILine>();
            Controller.CallPointerDown(dv, new object[] {dv.GetComponentInChildren<DynamicLine>(), Vector2.one});
            Controller.CallOnDrag(dv,
                new object[] {dv.GetComponentInChildren<DynamicLine>(), Vector2.one, new Vector2(2, 2)});
            yield return new WaitForEndOfFrame();
            Assert.AreEqual(new Vector3(2, 2, 0), uiLine.line.Pop().position);
        }

        [UnityTest]
        public IEnumerator When_ViewPointerDown_Is_Called_The_Controller_Is_Notified()
        {
            DrawingView dv = Controller.SpawnView(null);
            EventSystem e = new GameObject("EventSystem").AddComponent<EventSystem>();
            yield return new WaitForEndOfFrame();
            dv.OnPointerDown(new PointerEventData(e));
            Assert.IsNotEmpty(dv.GetComponentInChildren<UILine>().line);
        }

        [UnityTest]
        public IEnumerator When_ViewDragged_Is_Called_The_Controller_Is_Notified()
        {
            DrawingView dv = Controller.SpawnView(null);
            EventSystem e = new GameObject("EventSystem").AddComponent<EventSystem>();
            yield return new WaitForEndOfFrame();
            dv.OnPointerDown(new PointerEventData(e));
            var pointerEvent = new PointerEventData(e);
            pointerEvent.position = new Vector2(10, 10);
            dv.OnDrag(pointerEvent);
            Assert.IsNotEmpty(dv.GetComponentInChildren<UILine>().line);
        }

        [UnityTest]
        public IEnumerator When_ViewPointerUp_Is_Called_The_Controller_Is_Notified()
        {
            yield return new WaitForEndOfFrame();
            Assert.Catch(() =>
            {
                DrawingView dv = Controller.SpawnView(null);
                EventSystem e = new GameObject("EventSystem").AddComponent<EventSystem>();
                dv.OnPointerUp(new PointerEventData(e));
            });
        }
    }
}