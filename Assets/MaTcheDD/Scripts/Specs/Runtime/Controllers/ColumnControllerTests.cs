using System.Collections;
using MaTcheDD.Specs.Mocks;
using MaTcheDD.Views;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.UI;

namespace MaTcheDD.Specs.Runtime
{
    public class ColumnControllerTests : AbstractControllerTest<MockColumnController,ColumnView>
    {
        [SetUp]
        public override void Setup()
        {
            base.Setup();
        }

        [UnityTest]
        public IEnumerator When_Column_Controller_Starts_It_Spawns_Pairs()
        {
            yield return new WaitForEndOfFrame();
            Assert.NotNull(View.GetComponentInChildren<PairView>());
            Object.Destroy(View.gameObject);
        }

        protected override MockColumnController NewController()
        {
            return new MockColumnController();
        }
    }
}