using System.Collections;
using System.Reflection;
using HamerSoft.UnityMvc;
using MaTcheDD.Controllers;
using MaTcheDD.Specs.Mocks;
using MaTcheDD.Views;
using MaTcheDD.Views.Menu;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace MaTcheDD.Specs.Runtime
{
    public class MockMenuController : MenuController
    {
        public bool Quitted, Highscored, Started;

        protected override void ViewOnQuited(View view, object[] outputArguments)
        {
            Quitted = true;
        }

        protected override void ViewOnHighScored(View view, object[] outputArguments)
        {
            base.ViewOnHighScored(view, outputArguments);
            Highscored = true;
        }

        protected override void ViewOnGameStarted(View view, object[] outputArguments)
        {
            base.ViewOnGameStarted(view, outputArguments);
            Started = true;
        }
    }

    public class MockMenuView : MenuView
    {
        public void InvokeQuit()
        {
            typeof(MenuView).GetMethod("QuitGame", BindingFlags.Instance | BindingFlags.NonPublic).Invoke(this, null);
        }

        public void InvokeHighScores()
        {
            typeof(MenuView).GetMethod("ShowHighScoreView", BindingFlags.Instance | BindingFlags.NonPublic)
                .Invoke(this, null);
        }

        public void InvokeStartGame()
        {
            typeof(MenuView).GetMethod("ShowStartGameView", BindingFlags.Instance | BindingFlags.NonPublic)
                .Invoke(this, null);
        }
    }

    public class MenuControllerTests : AbstractControllerTest<MockMenuController, MockMenuView>
    {
        private MockCanvas _canvas;

        protected override MockMenuController NewController()
        {
            return new MockMenuController();
        }

        protected override bool SpawnDefaultView => false;

        public override void Setup()
        {
            base.Setup();
            _canvas = new GameObject("Canvas").AddComponent<MockCanvas>();
            View = new GameObject("View").AddComponent<MockMenuView>();
            View.transform.SetParent(_canvas.transform);
            Controller.RegisterView(View);
        }

        [UnityTest]
        public IEnumerator When_Quit_Is_Called_The_MenuController_Catches_The_Quit_Event()
        {
            yield return new WaitForEndOfFrame();
            View.InvokeQuit();
            Assert.True(Controller.Quitted);
        }

        [UnityTest]
        public IEnumerator When_Highscored_Is_Called_The_MenuController_Catches_The_HighScore_Event()
        {
            yield return new WaitForEndOfFrame();
            View.InvokeHighScores();
            Assert.True(Controller.Highscored);
        }

        [UnityTest]
        public IEnumerator When_Highscored_Is_Called_The_MenuView_Is_Destroyed()
        {
            View.InvokeHighScores();
            yield return new WaitForEndOfFrame();
            Assert.Catch(() =>
            {
                var test = View.name;
            });
        }

        [UnityTest]
        public IEnumerator When_Highscored_Is_Called_The_HighScoreView_IS_Spawned()
        {
            View.InvokeHighScores();
            yield return new WaitForEndOfFrame();
            Assert.NotNull(_canvas.GetComponentInChildren<HighScoresView>());
        }

        [UnityTest]
        public IEnumerator When_StartGame_Is_Called_The_MenuController_Catches_The_StartGame_Event()
        {
            yield return new WaitForEndOfFrame();
            View.InvokeStartGame();
            Assert.True(Controller.Started);
        }

        [UnityTest]
        public IEnumerator When_StartGame_Is_Called_The_MenuView_Is_Destroyed()
        {
            View.InvokeStartGame();
            yield return new WaitForEndOfFrame();
            Assert.Catch(() =>
            {
                var test = View.name;
            });
        }

        [UnityTest]
        public IEnumerator When_StartGame_Is_Called_The_StartGameView_Is_Spawned()
        {
            View.InvokeStartGame();
            yield return new WaitForEndOfFrame();
            Assert.NotNull(_canvas.GetComponentInChildren<StartGameView>());
        }
    }
}