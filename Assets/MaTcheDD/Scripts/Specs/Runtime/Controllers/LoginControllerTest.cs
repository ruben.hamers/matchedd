using System;
using System.Collections;
using MaTcheDD.Controllers;
using MaTcheDD.Entities;
using MaTcheDD.Specs.Mocks;
using MaTcheDD.Views;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.UI;

namespace MaTcheDD.Specs.Runtime
{
    public class MockLoginController : LoginController
    {
        public void InvokeLogin(LoginView view, string name)
        {
            ViewOnLocalLoggedIn(view, new[] {name});
        }

        public void InvokeStarted(LoginView view)
        {
            OnStarted(view, new object[0]);
        }
    }

    public class LoginControllerTest : AbstractControllerTest<MockLoginController, LoginView>

    {
        private MockCanvas _canvas;

        protected override MockLoginController NewController()
        {
            return new MockLoginController();
        }

        protected override bool SpawnDefaultView => false;

        public override void Setup()
        {
            base.Setup();
            _canvas = new GameObject("Canvas").AddComponent<MockCanvas>();
            var input = new GameObject("InputField").AddComponent<InputField>();
            input.gameObject.AddComponent<Button>();
            input.transform.SetParent(_canvas.transform);
            View = input.gameObject.AddComponent<LoginView>();
        }

        [UnityTest]
        public IEnumerator
            When_LoginView_Is_Started_The_LoginController_Does_Not_Destroy_The_View_When_There_Is_No_Player()
        {
            yield return new WaitForEndOfFrame();
            Assert.NotNull(View);
        }

        [UnityTest]
        public IEnumerator
            When_LoginView_Is_Started_The_LoginController_Destroys_The_View_When_There_Is_A_Player()
        {
            yield return new WaitForEndOfFrame();
            Main.GetPlugin<MatchedPlugin>().SetPlayer(new Player("Foo"));
            Controller.InvokeStarted(View);
            yield return new WaitForEndOfFrame();
            Assert.Catch(() =>
            {
                var test = View.name;
            });
            yield return new WaitForEndOfFrame();
            Main.GetPlugin<MatchedPlugin>().SetPlayer(null);
        }

        [UnityTest]
        public IEnumerator
            When_LoginView_Is_Started_The_LoginController_Shows_The_MenuView_When_There_Is_A_Player()
        {
            yield return new WaitForEndOfFrame();
            Main.GetPlugin<MatchedPlugin>().SetPlayer(new Player("Foo"));
            Controller.InvokeStarted(View);
            Assert.NotNull(_canvas.GetComponentInChildren<MenuView>());
            yield return new WaitForEndOfFrame();
            Main.GetPlugin<MatchedPlugin>().SetPlayer(null);
        }

        [UnityTest]
        public IEnumerator When_Login_Is_Called_With_Empty_Player_Name_There_Is_No_PLayer_Instantiated()
        {
            Controller.InvokeLogin(View, String.Empty);
            yield return new WaitForEndOfFrame();
            Assert.Null(Main.GetPlugin<MatchedPlugin>().Player);
        }

        [UnityTest]
        public IEnumerator When_Login_Is_Called_With_Player_Name_There_Is_A_PLayer_Instantiated()
        {
            Controller.InvokeLogin(View, "Foo");
            yield return new WaitForEndOfFrame();
            Assert.NotNull(Main.GetPlugin<MatchedPlugin>().Player);
            Main.GetPlugin<MatchedPlugin>().SetPlayer(null);
        }

        [UnityTest]
        public IEnumerator When_Login_Is_Called_With_Player_Name_The_Login_View_Is_Destroyed()
        {
            Controller.InvokeLogin(View, "Foo");
            yield return new WaitForEndOfFrame();
            Assert.Catch(() =>
            {
                var test = View.name;
            });
            Main.GetPlugin<MatchedPlugin>().SetPlayer(null);
            yield return new WaitForEndOfFrame();
        }

        [UnityTest]
        public IEnumerator When_Login_Is_Called_With_Player_Name_MenuView_Is_Shown()
        {
            Controller.InvokeLogin(View, "Foo");
            yield return new WaitForEndOfFrame();
            Assert.NotNull(_canvas.GetComponentInChildren<MenuView>());
            Main.GetPlugin<MatchedPlugin>().SetPlayer(null);
            yield return new WaitForEndOfFrame();
        }
    }
}