using System.Collections;
using System.Reflection;
using HamerSoft.UnityMvc;
using MaTcheDD.Controllers;
using MaTcheDD.Specs.Mocks;
using MaTcheDD.Views;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Canvas = HamerSoft.Core.Singletons.Canvas;

namespace MaTcheDD.Specs.Runtime
{
    public class MockHeaderController : HeaderController
    {
        public void Started(HeaderView view, object[] args)
        {
            OnStarted(view, args);
        }

        public bool IsSliding;

        protected override void GoSlide(View view, object[] outputArguments)
        {
            IsSliding = true;
        }
    }

    public class MockHeaderView : HeaderView
    {
        public void InvokeGoed()
        {
            MethodInfo mi = typeof(HeaderView).GetMethod("Go", BindingFlags.Instance | BindingFlags.NonPublic);
            mi?.Invoke(this, null);
        }
        public void InvokeBackToMenu()
        {
            MethodInfo mi = typeof(HeaderView).GetMethod("BackToMenu", BindingFlags.Instance | BindingFlags.NonPublic);
            mi?.Invoke(this, null);
        }
    }

    [TestFixture]
    public class HeaderControllerTests : AbstractControllerTest<MockHeaderController, MockHeaderView>
    {
        private MockCanvas _canvas;

        protected override MockHeaderController NewController()
        {
            return new MockHeaderController();
        }

        public override void Setup()
        {
            base.Setup();
            _canvas = new GameObject("Canvas").AddComponent<MockCanvas>();
        }

        [UnityTest]
        public IEnumerator When_View_Fires_Goed_Event_The_Controller_Will_Start_The_Sliding_Usecase()
        {
            yield return new WaitForEndOfFrame();
            View.InvokeGoed();
            Assert.True(Controller.IsSliding);
        }
        
        [UnityTest]
        public IEnumerator When_View_Fires_Goed_Event_The_Controller_Will_Remove_The_DrawingView()
        {
            yield return new WaitForEndOfFrame();
            View.InvokeGoed();
            Assert.Null(_canvas.GetComponentInChildren<DrawingView>());
        }

        [UnityTest]
        public IEnumerator When_BackTo_Menu_Is_Pressed_The_Game_Will_Spawn_A_Choice_Dialog()
        {
            yield return new WaitForEndOfFrame();
            View.InvokeBackToMenu();
            Assert.NotNull(_canvas.GetComponentInChildren<ChoiceView>());
        }
    }
}