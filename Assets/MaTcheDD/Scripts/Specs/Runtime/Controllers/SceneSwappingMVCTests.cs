using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using HamerSoft.Core;
using HamerSoft.Core.Services;
using MaTcheDD.Views;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;
using Object = UnityEngine.Object;

namespace HamerSoft.UnityMvc.Specs.RunTime
{
    public class SceneSwappingMVCTests
    {
        private const string SceneName = "SceneSwappingTestsMVC_Scene";
        private Main _main;
        private BaseController<DrawingView> _drawingController;

        public IEnumerator Setup()
        {
            yield return LoadLevel(SceneName);
            yield return new WaitForEndOfFrame();
            _main =  Main.Load();
            yield return new WaitForEndOfFrame();
            _drawingController = _main.GetPlugin<MvcPlugin>().GetControllerByView<DrawingView>();
        }

        private IEnumerator LoadLevel(string levelName)
        {
            var loadSceneOperation = SceneManager.LoadSceneAsync(levelName);
            loadSceneOperation.allowSceneActivation = true;

            while (!loadSceneOperation.isDone)
                yield return null;
            yield return new WaitForSeconds(1);
        }

        private Dictionary<string, DrawingView> GetViews()
        {
            return typeof(BaseController<DrawingView>).GetField("Views", BindingFlags.Instance | BindingFlags.NonPublic)
                .GetValue(_drawingController) as Dictionary<string, DrawingView>;
        }

        [UnityTest]
        public IEnumerator When_Scene_Is_Loaded_Views_Are_Unregistered_From_Their_Controller()
        {
            yield return Setup();
            yield return new WaitForEndOfFrame();
            Assert.AreEqual(1, GetViews().Count);
            yield return Setup();
            yield return new WaitForEndOfFrame();
            Assert.AreEqual(1, GetViews().Count);
        }

        [UnityTest]
        public IEnumerator When_Scene_Is_Loaded_Views_Are_Unregistered_From_Their_Controller_And_Destroyed()
        {
            yield return Setup();
            yield return new WaitForEndOfFrame();
            DrawingView v = GetViews().FirstOrDefault().Value;
            yield return Setup();
            yield return new WaitForEndOfFrame();
            DrawingView v2 = GetViews().FirstOrDefault().Value;
            Assert.Catch(() =>
            {
                var test = v.name;
            });
            Assert.NotNull(v2);
        }

        [TearDown]
        public virtual void TearDown()
        {
        }
    }
}