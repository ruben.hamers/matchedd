﻿using System;
using System.Collections;
using System.Linq;
using System.Reflection;
using MaTcheDD.Controllers;
using MaTcheDD.Entities;
using MaTcheDD.Entities.Repositories.Bridge;
using MaTcheDD.Specs.Mocks;
using MaTcheDD.Views;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.UI;
using Canvas = HamerSoft.Core.Singletons.Canvas;
using Object = UnityEngine.Object;

namespace MaTcheDD.Specs.Runtime
{
    public class MockBridgeController : BridgeController
    {
        public Bridge Added { get; set; }
        public BridgeView AddedView { get; set; }


        public MatchedPlugin GetMatchedPlugin()
        {
            return typeof(BridgeController).GetField("_matchedPlugin", BindingFlags.Instance | BindingFlags.NonPublic)
                    .GetValue(this)
                as MatchedPlugin;
        }

        public override void AddedObject(Bridge added)
        {
            Added = added;
            base.AddedObject(added);
            AddedView = Canvas.GetInstance()?.GetComponentInChildren<BridgeView>();
        }
    }

    public class BridgeControllerTests : AbstractControllerTest<MockBridgeController, BridgeView>
    {
        private Bridge _simpleBridge;
        private Bridge _simpleBridge2;
        private Bridge _simpleBridge3;
        protected override bool SpawnDefaultView => false;

        protected override MockBridgeController NewController()
        {
            return new MockBridgeController();
        }

        [Test]
        public void When_Main_Is_Set_The_Matched_Plugin_Is_Set()
        {
            Assert.NotNull(Controller.GetMatchedPlugin());
        }

        [UnityTest]
        public IEnumerator When_Bridge_Is_Added_To_The_Repository_The_Controller_Is_Notified_As_Listener()
        {
            Canvas canvas = new GameObject("Canvas").AddComponent<MockCanvas>();
            canvas.gameObject.AddComponent<BridgeLayer>();
            yield return new WaitForEndOfFrame();
            AddSimpleBridge();
            Assert.AreSame(_simpleBridge, Controller.Added);
            yield return new WaitForEndOfFrame();
        }

        [UnityTest]
        public IEnumerator When_Bridge_Is_Added_To_The_Repository_The_Controller_Will_Spawn_A_New_View()
        {
            Canvas canvas = new GameObject("Canvas").AddComponent<MockCanvas>();
            canvas.gameObject.AddComponent<BridgeLayer>();
            yield return new WaitForEndOfFrame();
            AddSimpleBridge();
            yield return new WaitForEndOfFrame();
            Assert.NotNull(canvas.GetComponentInChildren<BridgeView>());
            yield return new WaitForEndOfFrame();
        }

        [UnityTest]
        public IEnumerator When_Bridge_Is_Removed_From_The_Repository_The_Controller_Will_Destroy_The_View()
        {
            Canvas canvas = new GameObject("Canvas").AddComponent<MockCanvas>();
            canvas.gameObject.AddComponent<BridgeLayer>();
            Controller.Dispose();
            Controller = null;
            yield return new WaitForEndOfFrame();
            _simpleBridge3 = new Bridge(new BridgeNode("a", Vector3.zero), new BridgeNode("b", Vector3.zero));
            Main.GetRepository<BridgeRepository>().AddObject(_simpleBridge3);
            yield return new WaitForEndOfFrame();
            Debug.Log($"simplebridge3 = {_simpleBridge3}");
            Debug.Log($"Canvas = {MockCanvas.GetInstance().GetInstanceID()}, canvas {canvas.GetInstanceID()}");
            var views = canvas.GetComponentsInChildren<BridgeView>();
            Main.GetRepository<BridgeRepository>().RemoveObject(ref _simpleBridge3);
            yield return new WaitForEndOfFrame();
            Assert.Less(canvas.GetComponentsInChildren<BridgeView>().Length,views.Length);
            yield return new WaitForEndOfFrame();
        }

        [UnityTest]
        public IEnumerator When_BridgeView_Is_Spawned_It_Will_Link_A_Bridge_To_The_PairViews()
        {
            var canvas = new GameObject("Canvas").AddComponent<MockCanvas>();
            canvas.gameObject.AddComponent<BridgeLayer>();
            canvas.gameObject.AddComponent<PlayAreaView>();
            canvas.gameObject.AddComponent<HorizontalLayoutGroup>();
            var pairs = new PairView[2];
            for (int i = 0; i < 2; i++)
            {
                var go = new GameObject("pairview", typeof(Head), typeof(Base));
                go.transform.SetParent(canvas.transform);
                pairs[i] = go.AddComponent<PairView>();
            }

            yield return new WaitForEndOfFrame();
            _simpleBridge2 = new Bridge(new BridgeNode(pairs[0].Id, Vector3.zero),
                new BridgeNode(pairs[1].Id, Vector3.zero));
            Main.GetRepository<BridgeRepository>().AddObject(_simpleBridge2);
            yield return new WaitForEndOfFrame();
            Assert.NotNull(pairs[0].GetBridges().FirstOrDefault());
            Assert.NotNull(pairs[1].GetBridges().FirstOrDefault());
            yield return new WaitForEndOfFrame();
        }

        [UnityTest]
        public IEnumerator When_BridgeView_Is_Spawned_It_Will_Link_The_Correct_Bridge_To_The_PairViews()
        {
            var canvas = new GameObject("Canvas").AddComponent<MockCanvas>();
            canvas.gameObject.AddComponent<BridgeLayer>();
            canvas.gameObject.AddComponent<PlayAreaView>();
            canvas.gameObject.AddComponent<HorizontalLayoutGroup>();
            var pairs = new PairView[2];
            for (int i = 0; i < 2; i++)
            {
                var go = new GameObject("pairview", typeof(Head), typeof(Base));
                go.transform.SetParent(canvas.transform);
                pairs[i] = go.AddComponent<PairView>();
            }

            yield return new WaitForEndOfFrame();
            _simpleBridge2 = new Bridge(new BridgeNode(pairs[0].Id, Vector3.zero),
                new BridgeNode(pairs[1].Id, Vector3.zero));
            Main.GetRepository<BridgeRepository>().AddObject(_simpleBridge2);
            yield return new WaitForEndOfFrame();
            Assert.AreEqual(Controller.AddedView.Id, pairs[0].GetBridges().FirstOrDefault()?.Id);
            Assert.AreEqual(Controller.AddedView.Id, pairs[1].GetBridges().FirstOrDefault()?.Id);
            yield return new WaitForEndOfFrame();
        }

        private void AddSimpleBridge()
        {
            _simpleBridge = new Bridge(new BridgeNode("a", Vector3.zero), new BridgeNode("b", Vector3.zero));
            Main.GetRepository<BridgeRepository>().AddObject(_simpleBridge);
        }

        public override void TearDown()
        {
            if (_simpleBridge != null)
                Main.GetRepository<BridgeRepository>().RemoveObject(ref _simpleBridge);
            if (_simpleBridge2 != null)
                Main.GetRepository<BridgeRepository>().RemoveObject(ref _simpleBridge2);
            if (_simpleBridge != null)
                Main.GetRepository<BridgeRepository>().RemoveObject(ref _simpleBridge3);
            Main.GetRepository<BridgeRepository>().Flush();
            base.TearDown();
        }
    }
}