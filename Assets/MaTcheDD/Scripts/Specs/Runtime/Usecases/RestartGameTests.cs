using System.Collections;
using System.Linq;
using System.Reflection;
using HamerSoft.Core;
using MaTcheDD.Entities;
using MaTcheDD.Entities.Repositories.Bridge;
using MaTcheDD.Specs.Mocks;
using MaTcheDD.UseCases;
using MaTcheDD.Views;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.UI;

namespace MaTcheDD.Specs.Runtime.Usecases
{
    [TestFixture]
    public class RestartGameTests
    {
        private RestartGame _restartGame;
        private Main _main;
        private MockCanvas _root;
        private HeaderView _header;

        [SetUp]
        public void Setup()
        {
            _main = Main.Load();
            _main.Init();
            SetupScene();
            _restartGame = new RestartGame(_main, _header);
            _main.GetPlugin<MatchedPlugin>().SetPlayer(new Player("Foo"));
        }

        [UnityTest]
        public IEnumerator When_Restart_Game_Is_Executed_All_Bridges_Are_Deleted_From_The_Bridge_Repository()
        {
            var repo = _main.GetRepository<BridgeRepository>();
            repo.AddObject(new Bridge(new BridgeNode("", Vector3.up), new BridgeNode("", Vector3.back)));
            repo.AddObject(new Bridge(new BridgeNode("", Vector3.up), new BridgeNode("", Vector3.back)));
            repo.AddObject(new Bridge(new BridgeNode("", Vector3.up), new BridgeNode("", Vector3.back)));
            yield return new WaitForEndOfFrame();
            _restartGame.Execute(() => { Assert.IsEmpty(repo.GetAll()); });
        }

        [UnityTest]
        public IEnumerator When_Restart_Game_Is_Executed_A_New_Session_Is_Created()
        {
            yield return new WaitForEndOfFrame();
            _restartGame.Execute(() => { Assert.NotNull(_main.GetPlugin<MatchedPlugin>().Session); });
        }

        [UnityTest]
        public IEnumerator When_Restart_Game_Is_Executed_A_New_Round_Is_Started()
        {
            yield return new WaitForEndOfFrame();
            _restartGame.Execute(() =>
            {
                var pairs = _root.GetComponentsInChildren<PairView>();
                Assert.NotNull(pairs.Any(p => !p.IsMatched()));
            });
        }

        private void SetupScene()
        {
            _root = new GameObject("Root").AddComponent<MockCanvas>();
            _root.CallAwake();
            _root.gameObject.AddComponent<BridgeLayer>();
            _root.gameObject.AddComponent<PlayAreaView>();
            _root.gameObject.AddComponent<HorizontalLayoutGroup>();
            var score = new GameObject("score", typeof(Text)).AddComponent<Score>();
            typeof(Score).GetField("Value", BindingFlags.Instance | BindingFlags.NonPublic)
                .SetValue(score, score.GetComponentInChildren<Text>());
            _header = score.gameObject.AddComponent<HeaderView>();
            _header.transform.SetParent(_root.transform);
            var mockPairController = new MockPairController();
            mockPairController.SetMain(_main);
            for (int i = 0; i < 3; i++)
            {
                mockPairController.SpawnView(_root.transform);
            }
        }
    }
}