﻿using System.Collections;
using System.Linq;
using HamerSoft.Core;
using HamerSoft.Core.Services;
using MaTcheDD.Lines;
using MaTcheDD.Specs.Mocks;
using MaTcheDD.UseCases;
using MaTcheDD.Views;
using NUnit.Framework;
using UnityEditor;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.UI;

namespace MaTcheDD.Specs.Runtime.Usecases
{
    public class SwapPairHeadTests
    {
        private PlayAreaView _playAreaView;
        private Main _main;

        /// <summary>
        /// use 6 as upper bound since that is the max tracks we support
        /// </summary>
        private const int Tracks = 6;

        [SetUp]
        public void Setup()
        {
            _main = Main.Load();
            _main.Init();
            SetupView();
        }

        private void SetupView()
        {
            var playAreaController = new MockPlayAreaController();
            playAreaController.SetMain(_main);
            _playAreaView = new GameObject("PlayAreaView", typeof(MockCanvas), typeof(HorizontalLayoutGroup))
                .AddComponent<PlayAreaView>();
            for (int i = 0; i < Tracks; i++)
            {
                playAreaController.SpawnCustomView<PairView>(_playAreaView.transform);
//                GameObject go = new GameObject($"PairView{i}", typeof(Head), typeof(Base));
//                StaticLine s = new GameObject("sl").AddComponent<StaticLine>();
//                s.transform.SetParent(go.transform);
//                go.AddComponent<PairView>().transform.SetParent(_playAreaView.transform);
            }
        }

        [UnityTest]
        public IEnumerator SwapPairHeads_Will_Switch_Heads_Among_PairViews()
        {
            yield return new WaitForEndOfFrame();
            bool done = false;
            int waited = 0;
            new SwapPairHeads(_playAreaView, Tracks).Execute(() => { done = true; });
            while (!done)
            {
                yield return new WaitForSeconds(1);
                waited++;
                if (waited == 5)
                    done = true;
            }

            PairView[] pairs = _playAreaView.GetComponentsInChildren<PairView>();
            Assert.True(pairs.Any(p => !p.IsMatched()));
            yield return new WaitForEndOfFrame();
        }

        [UnityTest]
        public IEnumerator SwapPairHeads_Will_Move_The_Heads_Up_To_The_End_Of_The_StaticLine()
        {
            yield return new WaitForEndOfFrame();
            bool done = false;
            int waited = 0;
            new SwapPairHeads(_playAreaView, Tracks).Execute(() => { done = true; });
            while (!done)
            {
                yield return new WaitForSeconds(1);
                waited++;
                if (waited == 5)
                    done = true;
            }

            PairView[] pairs = _playAreaView.GetComponentsInChildren<PairView>();
            Assert.True(pairs.All(p => p.GetHead().transform.localPosition == p.GetLine().GetEnd() +
                                       new Vector3(0, (p.GetHead().transform as RectTransform).sizeDelta.y / 2,
                                           0)));
            yield return new WaitForEndOfFrame();
        }

        [TearDown]
        public void Teardown()
        {
        }
    }
}