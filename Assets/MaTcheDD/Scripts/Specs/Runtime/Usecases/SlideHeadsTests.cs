using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using HamerSoft.Core;
using MaTcheDD.Entities;
using MaTcheDD.Lines;
using MaTcheDD.Specs.Mocks;
using MaTcheDD.UseCases;
using MaTcheDD.UseCases.Sliding;
using MaTcheDD.Views;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.UI;
using Canvas = HamerSoft.Core.Singletons.Canvas;
using Object = UnityEngine.Object;

namespace MaTcheDD.Specs.Runtime.Usecases
{
    [TestFixture]
    public class SlideHeadsTests
    {
        private const int Maxpairs = 3;
        private MockSlideHeads _slide;
        private static MockCanvas _canvas;
        private static MockPairController _mockPairController;

        internal class MockSlideHeads : SlideHeads
        {
            public bool CallbackIsInvoked;
            public Dictionary<int, bool> updatingHeads;

            public MockSlideHeads(float speed) : base(speed, null,new SlideSounds())
            {
                updatingHeads = new Dictionary<int, bool>();
            }

            public PairView[] GetPairs()
            {
                FieldInfo fi =
                    typeof(SlideHeads).GetField("_pairViews", BindingFlags.Instance | BindingFlags.NonPublic);
                return fi?.GetValue(this) as PairView[] ?? new PairView[0];
            }

            public Path CalculateSinglePath(PairView pv, PairView[] others)
            {
                return base.CalculatePath(pv, others);
            }

            protected override void UpdateHead(Path p, int loopId)
            {
                base.UpdateHead(p, loopId);
                if (!updatingHeads.ContainsKey(loopId))
                    updatingHeads.Add(loopId, true);
            }

            public void SetPaths()
            {
                FieldInfo fi = typeof(SlideHeads).GetField("_paths", BindingFlags.Instance | BindingFlags.NonPublic);
                fi.SetValue(this, CalculateAllPaths());
            }

            public List<Path> CalculateAllPaths()
            {
                return base.CalculatePaths();
            }

            public void CheckPaths()
            {
                CheckAllPathsFinished(0);
            }

            protected override void DisposeUseCase()
            {
                base.DisposeUseCase();
                CallbackIsInvoked = true;
            }
        }

        [SetUp]
        public void Setup()
        {
            SetupScene();
            _slide = new MockSlideHeads(0.1f);
        }

        private static void SetupScene()
        {
            _canvas = new GameObject("Canvas", typeof(UnityEngine.Canvas)).AddComponent<MockCanvas>();
            var canvasScaler = _canvas.gameObject.AddComponent<CanvasScaler>();
            canvasScaler.referenceResolution = new Vector2(1920, 1080);
            Main main = Main.Load();
            main.Init();
            var playArea = _canvas.gameObject.AddComponent<PlayAreaView>();
            playArea.gameObject.AddComponent<DrawingLayer>();
            _mockPairController = new MockPairController();
            _mockPairController.SetMain(main);
            ColumnView cv = new GameObject("ColumnView",typeof(RectTransform)).AddComponent<ColumnView>();
            cv.transform.SetParent(_canvas.transform);
            RectTransform rt = cv.gameObject.GetComponent<RectTransform>();
            rt.pivot = new Vector2(0, 1);
            rt.anchorMin = new Vector2(0, 1);
            rt.anchorMax = new Vector2(0, 1);
            rt.localPosition = new Vector3(0, 0, 0);
            rt.sizeDelta = new Vector2(300, 930);
            var mp = main.GetPlugin<MatchedPlugin>();
            for (int i = 0; i < Maxpairs; i++)
            {
                PairView pv = _mockPairController.SpawnView(cv.transform);
                pv.transform.localPosition = new Vector3(0, 0, 0);
                pv.GetLine().Init(mp.GetLineThickness, mp.GetLineColor);
                pv.GetLine().Set(Vector3.zero, new Vector3(0, 730, 0));
                pv.MoveHeadToTop();
            }
        }

        [UnityTest]
        public IEnumerator When_Slide_Heads_Is_Constructed_The_PairViews_Will_Be_Queried()
        {
            yield return new WaitForEndOfFrame();
            Assert.AreEqual(Maxpairs, _slide.GetPairs().Length);
        }

        [UnityTest]
        public IEnumerator When_There_Are_No_Bridges_Calculate_Path_Returns_A_Path_With_TWO_Positions()
        {
            yield return new WaitForEndOfFrame();
            var queue = GetPathQueue();
            Assert.AreEqual(2, queue.Count);
        }

        private Queue<Vector3> GetPathQueue()
        {
            PairView pv = _canvas.GetComponentInChildren<PairView>();
            Path p = _slide.CalculateSinglePath(pv, _canvas.GetComponentsInChildren<PairView>());
            FieldInfo fi = typeof(Path).GetField("_path", BindingFlags.Instance | BindingFlags.NonPublic);
            Queue<Vector3> queue = fi?.GetValue(p) as Queue<Vector3> ?? new Queue<Vector3>();
            return queue;
        }

        private Queue<Vector3> GetPathQueue(PairView pairView)
        {
            Path p = _slide.CalculateSinglePath(pairView, _canvas.GetComponentsInChildren<PairView>());
            FieldInfo fi = typeof(Path).GetField("_path", BindingFlags.Instance | BindingFlags.NonPublic);
            Queue<Vector3> queue = fi?.GetValue(p) as Queue<Vector3> ?? new Queue<Vector3>();
            return queue;
        }

        [UnityTest]
        public IEnumerator
            When_There_Are_No_Bridges_Calculate_Path_Returns_A_Path_With_TWO_Positions_With_Start_Equal_To_StaticLine_End_Plus_HeadHeight()
        {
            yield return new WaitForEndOfFrame();
            var queue = GetPathQueue();
            PairView pv = _canvas.GetComponentInChildren<PairView>();
            Assert.AreEqual(pv.GetLine().GetEnd() + new Vector3(0,(pv.GetHead().transform as RectTransform).sizeDelta.y/2,0), queue.Peek());
            Assert.AreEqual(pv.GetHead().transform.position, queue.Peek());
        }

        [UnityTest]
        public IEnumerator
            When_There_Are_No_Bridges_Calculate_Path_Returns_A_Path_With_TWO_Positions_With_End_Equal_To_StaticLine_Start()
        {
            yield return new WaitForEndOfFrame();
            var queue = GetPathQueue();
            PairView pv = _canvas.GetComponentInChildren<PairView>();
            queue.Dequeue();
            Vector3 pos = pv.GetLine().GetStart() + new Vector3(0,
                              (pv.GetBase().transform as RectTransform).sizeDelta.y, 0);
            Assert.AreEqual(pos, queue.Peek());
        }

        [UnityTest]
        public IEnumerator Calculate_Paths_Returns_Equal_Number_Of_Paths_Compared_To_PairViews()
        {
            yield return new WaitForEndOfFrame();
            Assert.AreEqual(_slide.CalculateAllPaths().Count, _slide.GetPairs().Length);
        }

        [UnityTest]
        public IEnumerator SlideHeads_Only_Completes_When_All_Paths_Are_Finished()
        {
            yield return new WaitForEndOfFrame();
            _slide.SetPaths();
            _slide.CheckPaths();
            yield return new WaitForEndOfFrame();
            Assert.False(_slide.CallbackIsInvoked);
        }

        [UnityTest]
        public IEnumerator When_SlideHeads_Is_Executed_The_Heads_Of_All_Paths_Will_Navigate()
        {
            yield return new WaitForEndOfFrame();
            bool done = false;
            int waited = 0;
            _slide.Execute(b => { done = true; });

            while (!done)
            {
                yield return new WaitForSeconds(1);
                waited++;
                if (waited == 5)
                    done = true;
            }

            Assert.True(_slide.updatingHeads.Count == Maxpairs);
        }

        [UnityTest]
        public IEnumerator When_SlideHeads_Is_Executed_With_No_Bridges_The_Slide_Usecase_Will_Always_Fail()
        {
            yield return new WaitForEndOfFrame();
            bool done = false;
            bool success = false;
            int waited = 0;
            new SwapPairHeads(_canvas.GetComponentInChildren<PlayAreaView>(), Maxpairs).Execute(() =>
            {
                _slide.Execute(b =>
                {
                    done = true;
                    success = b;
                });
            });


            while (!done)
            {
                yield return new WaitForSeconds(1);
                waited++;
                if (waited == 5)
                    done = true;
            }

            Assert.False(success);
        }

        [UnityTest]
        public IEnumerator When_Paths_Are_Calculated_The_Bridges_Are_Included()
        {
            yield return new WaitForEndOfFrame();
            var pairs = _slide.GetPairs();
            BridgeView bv = new GameObject("BridgeView", typeof(StaticLine)).AddComponent<BridgeView>();
            yield return new WaitForSeconds(1);
            bv.Init("foo",new BridgeNode(pairs[0].Id, new Vector3(0, 480, 0)),
                new BridgeNode(pairs[1].Id, new Vector3(0, 350, 0)), 2, new Gradient());
            pairs[0].AddBridge(bv);
            pairs[1].AddBridge(bv);
            Assert.AreEqual(4, GetPathQueue(pairs[0]).Count);
        }

        [UnityTest]
        public IEnumerator
            When_Paths_Are_Calculated_And_A_Bridge_Is_Included_Both_The_Start_And_End_Position_Are_Added_To_The_Path()
        {
            yield return new WaitForEndOfFrame();
            var pairs = _slide.GetPairs();
            BridgeView bv = new GameObject("BridgeView", typeof(StaticLine)).AddComponent<BridgeView>();
            yield return new WaitForSeconds(1);
            bv.Init("foo", new BridgeNode(pairs[0].Id, new Vector3(0, 480, 0)),
                new BridgeNode(pairs[1].Id, new Vector3(0, 350, 0)), 2, new Gradient());
            pairs[0].AddBridge(bv);
            pairs[1].AddBridge(bv);
            var queue = GetPathQueue(pairs[0]);
            Assert.AreEqual(780f, queue.Dequeue().y);
            Assert.AreEqual(480f, queue.Dequeue().y);
            Assert.AreEqual(350f, queue.Dequeue().y);
            Assert.AreEqual(150f, queue.Dequeue().y);
        }

        [TearDown]
        public void TearDown()
        {
            _slide?.Dispose();
            Object.DestroyImmediate(_canvas.gameObject);
            _mockPairController.Dispose();
        }

        [UnityTest]
        public IEnumerator
            When_Paths_Are_Calculated_And_There_Are_Bridges_Among_Multiple_Pairs_The_Bridges_Are_Included()
        {
            yield return new WaitForEndOfFrame();
            var pairs = _slide.GetPairs();
            yield return new WaitForSeconds(1);
            yield return AddBridge(pairs, 0, 1, 500, 450);
            yield return AddBridge(pairs, 1, 2, 430, 300);
            yield return AddBridge(pairs, 2, 1, 250, 180);
            yield return new WaitForSeconds(1);
            Assert.AreEqual(8, GetPathQueue(pairs[0]).Count);
        }

        [UnityTest]
        public IEnumerator
            When_Paths_Are_Calculated_And_There_Are_Bridges_Among_Multiple_Pairs_The_Positions_Are_Correct()
        {
            yield return new WaitForEndOfFrame();
            var pairs = _slide.GetPairs();
            yield return new WaitForSeconds(1);
            yield return AddBridge(pairs, 0, 1, 500, 450);
            yield return AddBridge(pairs, 1, 2, 430, 300);
            yield return AddBridge(pairs, 2, 1, 250, 180);
            var queue = GetPathQueue(pairs[0]);
            Assert.AreEqual(780f, queue.Dequeue().y);
            Assert.AreEqual(500f, queue.Dequeue().y);
            Assert.AreEqual(450f, queue.Dequeue().y);
            Assert.AreEqual(430f, queue.Dequeue().y);
            Assert.AreEqual(300f, queue.Dequeue().y);
            Assert.AreEqual(250f, queue.Dequeue().y);
            Assert.AreEqual(180, queue.Dequeue().y);
            Assert.AreEqual(150, queue.Dequeue().y);
        }

        private IEnumerator AddBridge(PairView[] pairs, int index1, int index2, float start, float end)
        {
            BridgeView bv = new GameObject("BridgeView", typeof(StaticLine)).AddComponent<BridgeView>();
            yield return new WaitForEndOfFrame();
            bv.Init("foo" ,new BridgeNode(pairs[index1].Id, new Vector3(0, start, 0)),
                new BridgeNode(pairs[index2].Id, new Vector3(0, end, 0)), 2, new Gradient());
            pairs[index1].AddBridge(bv);
            pairs[index2].AddBridge(bv);
        }
    }
}