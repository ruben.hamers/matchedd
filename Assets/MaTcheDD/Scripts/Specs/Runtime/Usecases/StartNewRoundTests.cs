using System.Collections;
using System.Linq;
using System.Reflection;
using HamerSoft.Core;
using HamerSoft.Core.Services;
using MaTcheDD.Entities;
using MaTcheDD.Specs.Mocks;
using MaTcheDD.UseCases;
using MaTcheDD.Views;
using NUnit.Framework;
using TMPro;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.UI;

namespace MaTcheDD.Specs.Runtime.Usecases
{
    public class StartNewRoundTests
    {
        private PlayAreaView _playAreaView;
        private Main _main;
        private MockPairController _mockPairController;

        [SetUp]
        public void Setup()
        {
            _main = Main.Load();
            _main.Init();
            SetupView();
        }

        private void SetupView()
        {
            _playAreaView = new GameObject("PlayAreaView").AddComponent<PlayAreaView>();
            var canvas = _playAreaView.gameObject.AddComponent<MockCanvas>();
            var score = new GameObject("score", typeof(Text)).AddComponent<Score>();
            typeof(Score).GetField("Value", BindingFlags.Instance | BindingFlags.NonPublic)
                .SetValue(score, score.GetComponentInChildren<Text>());
            score.gameObject.AddComponent<HeaderView>();
            score.transform.SetParent(canvas.transform);
            _main.GetPlugin<MatchedPlugin>().Session = new Session("Foo",3);
            score.SetRounds(_main.GetPlugin<MatchedPlugin>().Session.GetRounds());
            _mockPairController = new MockPairController();
            _mockPairController.SetMain(_main);
            ColumnView cv = new GameObject("ColumnView", typeof(RectTransform)).AddComponent<ColumnView>();
            cv.transform.SetParent(canvas.transform);
            RectTransform rt = cv.gameObject.GetComponent<RectTransform>();
            rt.pivot = new Vector2(0, 1);
            rt.anchorMin = new Vector2(0, 1);
            rt.anchorMax = new Vector2(0, 1);
            rt.localPosition = new Vector3(0, 0, 0);
            rt.sizeDelta = new Vector2(300, 930);
            var mp = _main.GetPlugin<MatchedPlugin>();
            for (int i = 0; i < 3; i++)
            {
                PairView pv = _mockPairController.SpawnView(cv.transform);
                pv.transform.localPosition = new Vector3(0, 0, 0);
                pv.GetLine().Init(mp.GetLineThickness, mp.GetLineColor);
                pv.GetLine().Set(Vector3.zero, new Vector3(0, 730, 0));
                pv.MoveHeadToTop();
            }
        }

        [UnityTest]
        public IEnumerator StartNewRounds_Will_Update_The_Score_UI_In_The_Header()
        {
            yield return new WaitForEndOfFrame();
            var score = _playAreaView.GetComponentInChildren<Score>();
            var rounds = score.GetComponentInChildren<Text>().text;
            yield return new WaitForEndOfFrame();
            _main.GetPlugin<MatchedPlugin>().Session.AddRound();
            new StartNewRound(_playAreaView.GetComponentInChildren<HeaderView>(), _main.GetPlugin<MatchedPlugin>())
                .Execute(() =>
                {
                    int startRounds = System.Convert.ToInt32(rounds);
                    int afterRounds = System.Convert.ToInt32(score.GetComponentInChildren<Text>().text);
                    Debug.Log($"Before={startRounds}, afterrounds={afterRounds}");
                    Assert.Less(startRounds, afterRounds);
                });
            yield return new WaitForEndOfFrame();
        }

        [UnityTest]
        public IEnumerator StartNewRounds_Will_Swap_The_The_PairHeads_Until_They_Do_Not_Match()
        {
            yield return new WaitForEndOfFrame();
            new StartNewRound(_playAreaView.GetComponentInChildren<HeaderView>(), _main.GetPlugin<MatchedPlugin>())
                .Execute(() =>
                {
                    var pairs = _playAreaView.GetComponentsInChildren<PairView>();
                    Assert.True(pairs.Any(p => !p.IsMatched()));
                });
            yield return new WaitForEndOfFrame();
        }

        [TearDown]
        public void Teardown()
        {
            _playAreaView.ForceDestroyObject();
        }
    }
}