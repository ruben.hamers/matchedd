using geniikw.DataRenderer2D;
using MaTcheDD.Lines;
using NUnit.Framework;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace MaTcheDD.Specs.Mono.Lines
{
    public class MockDynamicLine : DynamicLine
    {
        public MockUiLine UILine => UiLine as MockUiLine;

        public void CallAwake()
        {
            Awake();
        }

        public void CallStart()
        {
            Start();
        }
    }

    [TestFixture]
    public class DynamicLineTests : LineTests<MockDynamicLine>
    {
        protected override MockDynamicLine GetLine()
        {
            MockUiLine uiLine = new GameObject("DynamicLine").AddComponent<MockUiLine>();
            uiLine.CallAwake();
            uiLine.CallStart();
            uiLine.line = Spline.Default;
            uiLine.line.Clear();
            return uiLine.gameObject.AddComponent<MockDynamicLine>();
        }

        public override void Setup()
        {
            base.Setup();
            Line.CallAwake();
            Line.CallStart();
        }

        [Test]
        public void PushPosition_Adds_A_New_Point_To_The_Line()
        {
            int points = Line.UILine.line.Count;
            Line.PushPosition();
            Assert.Greater(Line.UILine.line.Count, points);
        }
        
        [Test]
        public void EditPosition_Edits_The_Position_To_Desired_Position()
        {
            Vector3 desired = Vector3.one;
            Line.PushPosition();
            Assert.AreEqual(Vector3.zero, Line.UILine.line.GetPosition(0));
            Line.EditPosition(desired);
            Assert.AreEqual(desired, Line.UILine.line.GetPosition(0));
        }
        
        [Test]
        public void GetPositions_Returns_All_Positions_In_The_Line()
        {
            Line.PushPosition();
            Line.PushPosition();
            Line.PushPosition();
            Line.PushPosition();
            Assert.AreEqual(4, Line.GetPositions().Length);
        }
    }
}