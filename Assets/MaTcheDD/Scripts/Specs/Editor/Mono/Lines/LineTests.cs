using MaTcheDD.Lines;
using NUnit.Framework;
using UnityEngine;

namespace MaTcheDD.Specs.Mono.Lines
{
    [TestFixture]
    public abstract class LineTests<T> where T : Line
    {
        protected T Line;

        [SetUp]
        public virtual void Setup()
        {
            Line = GetLine();
        }

        protected abstract T GetLine();

        public void TearDown()
        {
            if (Line)
                Object.DestroyImmediate(Line.gameObject);
        }
    }
}