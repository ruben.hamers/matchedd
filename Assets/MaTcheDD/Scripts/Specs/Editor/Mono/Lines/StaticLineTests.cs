using geniikw.DataRenderer2D;
using MaTcheDD.Lines;
using NUnit.Framework;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace MaTcheDD.Specs.Mono.Lines
{
    public class MockStaticLine : StaticLine
    {
        public MockUiLine UILine => UiLine as MockUiLine;

        public void CallAwake()
        {
            Awake();
        }

        public void CallStart()
        {
            Start();
        }
    }

    public class MockUiLine : UILine
    {
        public void CallAwake()
        {
            Awake();
        }

        public void CallStart()
        {
            Start();
        }
    }

    [TestFixture]
    public class StaticLineTests : LineTests<MockStaticLine>
    {
        protected override MockStaticLine GetLine()
        {
            MockUiLine uiLine = new GameObject("StaticLine").AddComponent<MockUiLine>();
            uiLine.CallAwake();
            uiLine.CallStart();
            uiLine.line = Spline.Default;
            uiLine.line.Clear();
            return uiLine.gameObject.AddComponent<MockStaticLine>();
        }

        public override void Setup()
        {
            base.Setup();
            Line.CallAwake();
            Line.CallStart();
        }

        [Test]
        public void When_Init_Is_Called_The_Color_Is_Set()
        {
            var gradient = CreateGradient();
            Line.Init(2, gradient);
            Assert.AreEqual(gradient, Line.UILine.line.option.color);
        }

        [Test]
        public void When_StaticLine_Is_Set_The_Start_And_End_Positions_Are_Set()
        {
            Line.Set(Vector3.one, Vector3.one);
            Assert.AreEqual(Vector3.one, Line.GetStart());
            Assert.AreEqual(Vector3.one, Line.GetEnd());
        }

        [Test]
        public void When_Init_Is_Called_The_Thickness_Is_Set()
        {
            var gradient = new Gradient();
            Line.Init(2, gradient);
            Line.Set(Vector3.one, Vector3.one);
            Assert.AreEqual(2, Line.UILine.line.Pop().width);
        }

        private static Gradient CreateGradient()
        {
            var gradient = new Gradient();
            var blueCol = new GradientColorKey(new Color(0, 0, 0.7f), 0);
            var yellowCol = new GradientColorKey(new Color(1, 1, 0), 1);
            var blueAlpha = new GradientAlphaKey(1, 0);
            var yellowAlpha = new GradientAlphaKey(1, 1);
            gradient.SetKeys(new GradientColorKey[2] {blueCol, yellowCol},
                new GradientAlphaKey[2] {blueAlpha, yellowAlpha});
            return gradient;
        }
    }
}