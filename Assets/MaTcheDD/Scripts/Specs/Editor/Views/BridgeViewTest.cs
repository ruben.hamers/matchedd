using System.Reflection;
using geniikw.DataRenderer2D;
using MaTcheDD.Entities;
using MaTcheDD.Lines;
using MaTcheDD.Specs.Mono.Lines;
using MaTcheDD.Views;
using NUnit.Framework;
using UnityEngine;

namespace MaTcheDD.Specs.Views
{
    public class BridgeViewTest
    {
        private class MockBridgeView : BridgeView
        {
            public StaticLine GetLine()
            {
                return (StaticLine) typeof(BridgeView).GetField("_line", BindingFlags.Instance | BindingFlags.NonPublic)
                    .GetValue(this);
            }
        }


        private MockBridgeView _view;
        private Bridge _bridge;

        [SetUp]
        public void Setup()
        {
            _bridge = new Bridge(new BridgeNode("a", Vector3.zero), new BridgeNode("b", Vector3.one));

            var staticLine = new GameObject("BridgeView", typeof(MockUiLine)).AddComponent<MockStaticLine>();
            var mockUiLine = staticLine.GetComponent<MockUiLine>();
            mockUiLine.CallAwake();
            mockUiLine.CallStart();
            mockUiLine.line = Spline.Default;
            mockUiLine.line.Clear();
            staticLine.CallAwake();
            staticLine.CallStart();
            _view = staticLine.gameObject.AddComponent<MockBridgeView>();
            _view.Init(_bridge.Id, _bridge.GetStart(), _bridge.GetEnd(), 2, new Gradient());
        }

        [Test]
        public void Upon_View_Init_The_StartPosition_Is_Set()
        {
            Assert.AreEqual(Vector3.zero, _view.GetStartPosition());
        }

        [Test]
        public void Upon_View_Init_The_EndPosition_Is_Set()
        {
            Assert.AreEqual(Vector3.one, _view.GetEndPosition());
        }

        [Test]
        public void Upon_View_Init_The_StartPair_Is_Set()
        {
            Assert.NotNull(_view.GetStartPairId());
        }

        [Test]
        public void Upon_View_Init_The_EndPair_Is_Set()
        {
            Assert.NotNull(_view.GetEndPairId());
        }

        [Test]
        public void Upon_View_Init_The_StartPair_Is_Set_Correctly()
        {
            Assert.AreEqual("a", _view.GetStartPairId());
        }

        [Test]
        public void Upon_View_Init_The_EndPair_Is_Set_Correctly()
        {
            Assert.AreEqual("b", _view.GetEndPairId());
        }

        [Test]
        public void Upon_View_Init_The_StaticLine_Is_Set()
        {
            Assert.NotNull(_view.GetLine());
        }

        [Test]
        public void Upon_View_Init_The_StaticLine_Positions_Are_Set()
        {
            Assert.AreEqual(Vector3.zero, _view.GetLine().GetStart());
            Assert.AreEqual(Vector3.one, _view.GetLine().GetEnd());
        }
    }
}