using System.Collections.Generic;
using System.Reflection;
using HamerSoft.Core;
using HamerSoft.UnityMvc.Specs.RunTime.Mocks;
using MaTcheDD.Views;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace MaTcheDD.Specs.Views
{
    public class ChoiceViewTests
    {
        private ChoiceView _choiceView;
        private Main _main;

        [SetUp]
        public void Setup()
        {
            GetMain();
            MockController mc = new MockController();
            mc.SetMain(_main);
            _choiceView = mc.SpawnCustomView<ChoiceView>(null);
        }

        [Test]
        public void Upon_Init_The_Title_Is_Set()
        {
            _choiceView.Init("a",
                "b",
                new Dictionary<string, UnityAction>
                {
                    {"1", () => { }},
                    {"2", () => { }}
                });
            Assert.AreEqual(GetViewText("Title").text, "a");
        }

        [Test]
        public void Upon_Init_The_Content_Is_Set()
        {
            _choiceView.Init("a",
                "b",
                new Dictionary<string, UnityAction>
                {
                    {"1", () => { }},
                    {"2", () => { }}
                });
            Assert.AreEqual(GetViewText("Content").text, "b");
        }

        [Test]
        public void Upon_Init_Buttons_Are_Spawned_When_Dictionary_Is_Given()
        {
            CallViewAwake();
            _choiceView.Init("a",
                "b",
                new Dictionary<string, UnityAction>
                {
                    {"1", () => { }},
                    {"2", () => { }}
                });
            Assert.AreEqual(2, _choiceView.GetComponentsInChildren<NotificationButton>().Length);
        }

        private void CallViewAwake()
        {
            CallAwake(typeof(NotificationView), _choiceView, "Awake");
        }

        private void CallButtonAwake()
        {
            foreach (NotificationButton button in _choiceView.GetComponentsInChildren<NotificationButton>())
                CallAwake(typeof(NotificationButton), button, "Awake");
        }

        private void CallAwake(System.Type type, object target, string method)
        {
            type.GetMethod(method, BindingFlags.Instance | BindingFlags.NonPublic)
                .Invoke(target, null);
        }

        private Text GetViewText(string name)
        {
            return GetText(typeof(NotificationView), _choiceView, name);
        }

        private Text GetButtonText(NotificationButton button, string name)
        {
            return GetText(typeof(NotificationButton), button, name);
        }

        private Text GetText(System.Type type, object target, string name)
        {
            return type.GetField(name, BindingFlags.Instance | BindingFlags.NonPublic).GetValue(target) as Text;
        }

        protected void GetMain()
        {
            _main = Main.Load();
            _main.Init();
        }
    }
}