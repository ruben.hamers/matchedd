using MaTcheDD.Entities;
using NUnit.Framework;

namespace MaTcheDD.Scripts.Specs.Entities
{
    [TestFixture]
    public class SessionTests
    {
        private Session _session;

        [SetUp]
        public void Setup()
        {
            _session = new Session("Foo",3);
        }

        [Test]
        public void When_Session_Is_Constructed_The_Id_Is_Set()
        {
            Assert.NotNull(_session.Id);
        }
        [Test]
        public void When_Session_Is_Constructed_The_PlayerId_Is_Set()
        {
            Assert.NotNull(_session.GetPlayerId());
        }
        [Test]
        public void When_Session_Is_Constructed_The_PlayerId_Is_Not_Finished()
        {
            Assert.False(_session.GetFinished());
        }
        [Test]
        public void When_Session_Is_Constructed_The_PlayerId_Is_Set_Correctly()
        {
            Assert.AreEqual("Foo",_session.GetPlayerId());
        }
        [Test]
        public void When_Session_Is_Constructed_The_Tracks_Are_Set_Correctly()
        {
            Assert.AreEqual(3,_session.GetTracks());
        }
        [Test]
        public void When_Session_Is_Constructed_The_Rounds_Are_0()
        {
            Assert.AreEqual(0,_session.GetRounds());
        }

        [Test]
        public void Session_AddRound_Will_Increment_The_Rounds()
        {
            int currentRound = _session.GetRounds();
            _session.AddRound();
            Assert.AreEqual(currentRound+1,_session.GetRounds());
        }
        
        [Test]
        public void Session_SetFinished_Will_Finish_The_Session()
        {
            _session.SetFinished();
            Assert.True(_session.GetFinished());
        }

        [Test]
        public void Session_AddRound_Will_Not_Increment_The_Rounds_When_Session_Is_Finished()
        {
            int currentRound = _session.GetRounds();
            _session.SetFinished();
            _session.AddRound();
            Assert.AreEqual(currentRound,_session.GetRounds());
        }

    }
}