using MaTcheDD.Entities;
using NUnit.Framework;

namespace MaTcheDD.Scripts.Specs.Entities
{
    [TestFixture]
    public class PlayerTests
    {
        private Player _player;

        [SetUp]
        public void Setup()
        {
            _player = new Player("Foo");
        }

        [Test]
        public void When_Player_Is_Constructed_The_Name_Is_Set()
        {
            Assert.NotNull(_player.GetName());
        }

        [Test]
        public void When_Player_Is_Constructed_The_Name_Is_Set_Correctly()
        {
            Assert.AreEqual("Foo", _player.GetName());
        }

        [Test]
        public void When_Player_Is_Constructed_The_Id_Is_Set()
        {
            Assert.NotNull(_player.Id);
        }
    }
}