using MaTcheDD.Entities;
using NUnit.Framework;
using UnityEngine;

namespace MaTcheDD.Specs.Entities
{
    [TestFixture]
    public class BridgeTests
    {
        private Bridge _bridge;

        [SetUp]
        public void Setup()
        {
            _bridge = new Bridge(new BridgeNode("a", Vector3.one), new BridgeNode("b", Vector3.down));
        }

        [Test]
        public void When_Bridge_Is_Constructed_The_Id_Is_Set()
        {
            Assert.NotNull(_bridge.Id);
        }

        [Test]
        public void When_Bridge_Is_Constructed_The_StartPair_Is_Set()
        {
            Assert.NotNull(_bridge.GetStart().PairView);
        }

        [Test]
        public void When_Bridge_Is_Constructed_The_EndPair_Is_Set()
        {
            Assert.NotNull(_bridge.GetEnd().PairView);
        }

        [Test]
        public void When_Bridge_Is_Constructed_The_StartPosition_Is_Set_Correctly()
        {
            Assert.AreNotEqual(Vector3.zero, _bridge.GetStart().Position);
        }

        [Test]
        public void When_Bridge_Is_Constructed_The_EndPosition_Is_Set_Correctly()
        {
            Assert.AreNotEqual(Vector3.zero, _bridge.GetEnd().Position);
        }

        [Test]
        public void When_Bridge_Is_Constructed_The_StartPair_Is_Set_Correctly()
        {
            Assert.AreEqual("a", _bridge.GetStart().PairView);
        }

        [Test]
        public void When_Bridge_Is_Constructed_The_EndPair_Is_Set_Correctly()
        {
            Assert.AreEqual("b", _bridge.GetEnd().PairView);
        }
    }
}