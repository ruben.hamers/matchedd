using MaTcheDD.Entities;
using MaTcheDD.Entities.Repositories;
using NUnit.Framework;

namespace MaTcheDD.Specs.Entities.Repositories
{
    [TestFixture]
    public class PlayerRepositoryTests
    {
        private PlayerRepository _playerRepository;
        [SetUp]
        public void Setup()
        {
            _playerRepository = new PlayerRepository();
            _playerRepository.AddObject(new Player("Foo"));
        }

        [Test]
        public void GetPlayerByName_Finds_Player_With_Correct_Name()
        {
            Assert.NotNull(_playerRepository.GetPlayerByName("Foo"));
        }
        
        [Test]
        public void GetPlayerByName_Does_Not_Find_Player_With_InCorrect_Name()
        {
            Assert.Null(_playerRepository.GetPlayerByName("Bar"));
        }
        
        [TearDown]
        public void TearDown()
        {
            
        }
    }
}