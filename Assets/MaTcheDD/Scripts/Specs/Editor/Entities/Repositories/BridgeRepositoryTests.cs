using MaTcheDD.Entities;
using MaTcheDD.Entities.Repositories.Bridge;
using NUnit.Framework;
using UnityEngine;

namespace MaTcheDD.Specs.Entities.Repositories
{
    [TestFixture]
    public class BridgeRepositoryTests
    {
        private BridgeRepository _repository;

        [SetUp]
        public void Setup()
        {
            _repository = new BridgeRepository();
        }

        [Test]
        public void GetAll_Returns_All_Bridges_In_Repository()
        {
            _repository.AddObject(new Bridge(new BridgeNode("a", Vector3.zero), new BridgeNode("a", Vector3.zero)));
            _repository.AddObject(new Bridge(new BridgeNode("a", Vector3.zero), new BridgeNode("a", Vector3.zero)));
            _repository.AddObject(new Bridge(new BridgeNode("a", Vector3.zero), new BridgeNode("a", Vector3.zero)));
            _repository.AddObject(new Bridge(new BridgeNode("a", Vector3.zero), new BridgeNode("a", Vector3.zero)));
            Assert.AreEqual(4,_repository.GetAll().Count);
        }
        
        
        [Test]
        public void RemoveAll_Removed_All_Bridges_In_Repository()
        {
            _repository.AddObject(new Bridge(new BridgeNode("a", Vector3.zero), new BridgeNode("a", Vector3.zero)));
            _repository.AddObject(new Bridge(new BridgeNode("a", Vector3.zero), new BridgeNode("a", Vector3.zero)));
            _repository.AddObject(new Bridge(new BridgeNode("a", Vector3.zero), new BridgeNode("a", Vector3.zero)));
            _repository.AddObject(new Bridge(new BridgeNode("a", Vector3.zero), new BridgeNode("a", Vector3.zero)));
            _repository.RemoveAll();
            _repository.Flush();
            Assert.AreEqual(0,_repository.GetAll().Count);
        }
        
        [TearDown]
        public void TearDown()
        {
            _repository.Dispose();
            _repository = null;
        }
    }
}