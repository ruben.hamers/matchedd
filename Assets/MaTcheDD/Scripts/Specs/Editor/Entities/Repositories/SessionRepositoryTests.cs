using System.Linq;
using MaTcheDD.Entities;
using MaTcheDD.Entities.Repositories;
using NUnit.Framework;

namespace MaTcheDD.Specs.Entities.Repositories
{
    [TestFixture]
    public class SessionRepositoryTests
    {
        private SessionRepository _sessionRepository;
        private Session _session1;
        private Session _session2;
        private Session _session3;
        private Session _session4;
        private Session[] tenSessions;

        [SetUp]
        public void Setup()
        {
            _session1 = new Session("1", 3);
            _session2 = new Session("1", 3);
            _session3 = new Session("2", 3);
            _session4 = new Session("3", 3);

            _sessionRepository = new SessionRepository();
            _sessionRepository.AddObject(_session1);
            _sessionRepository.AddObject(_session2);
            _sessionRepository.AddObject(_session3);
            _sessionRepository.AddObject(_session4);
        }

        [Test]
        public void GetSessionsForPlayer_Finds_Sessions_Belonging_To_The_Correct_Player()
        {
            Assert.True(_sessionRepository.GetSessionsForPlayer("1").Count == 2);
        }

        [Test]
        public void GetSessionsForPlayer_Finds_No_Sessions_Belonging_To_The_InCorrect_Player()
        {
            Assert.True(_sessionRepository.GetSessionsForPlayer("4").Count == 0);
        }

        [Test]
        public void GetTopSessions_Find_X_Number_Of_Sessions()
        {
            Generate10Sessions();
            Assert.True(_sessionRepository.GetTopSessions(10, 3).Count == 10);
        }

        [Test]
        public void GetTopSessions_Are_Ordered_By_Number_OF_Rounds()
        {
            Generate10Sessions();
            var sessions = _sessionRepository.GetTopSessions(10, 3);
            var orderedSessions = sessions.OrderBy(s => s.GetRounds()).ToList();
            Assert.True(sessions.SequenceEqual(orderedSessions));
        }

        [Test]
        public void GetTopSessions_Returns_As_Many_As_Possible_When_Max_Parameter_Exceeds_Sessions_In_Repo()
        {
            Generate10Sessions();
            var sessions = _sessionRepository.GetTopSessions(1000000000, 3);
            Assert.True(sessions.Count < 1000000000);
        }

        private void Generate10Sessions()
        {
            tenSessions = new Session[10];
            for (int i = 0; i < 10; i++)
            {
                var session = new Session("1", 3);
                tenSessions[i] = session;
                for (int j = 0; j < i; j++)
                {
                    session.AddRound();
                }

                _sessionRepository.AddObject(session);
            }
        }

        [TearDown]
        public void TearDown()
        {
            _sessionRepository.RemoveObject(ref _session1);
            _sessionRepository.RemoveObject(ref _session2);
            _sessionRepository.RemoveObject(ref _session3);
            _sessionRepository.RemoveObject(ref _session4);
            if (tenSessions != null)
                for (int i = 0; i < tenSessions.Length; i++)
                {
                    if (tenSessions[i] != null)
                        _sessionRepository.RemoveObject(ref tenSessions[i]);
                }

            _sessionRepository.Flush();
            _sessionRepository.Dispose();
        }
    }
}