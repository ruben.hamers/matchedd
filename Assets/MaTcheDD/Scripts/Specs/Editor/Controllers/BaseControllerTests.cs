using System;
using System.Linq;
using HamerSoft.Core;
using HamerSoft.UnityMvc.Specs.RunTime.Mocks;
using MaTcheDD.Entities;
using MaTcheDD.Views;
using NUnit.Framework;
using UnityEngine;
using Object = UnityEngine.Object;

namespace MaTcheDD.Specs.Editor
{
    public class BaseControllerTests
    {
        private MockController mc;
        private MockView mv;
        private Main main;

        [SetUp]
        public void Setup()
        {
            main = Main.Load();
            main.Init();
            mv = new GameObject("MockView").AddComponent<MockView>();
            mc = new MockController();
            mc.SetMain(main);
        }

        [Test]
        public void SpawnCustomView_With_Filter_Returns_The_View_Matching_The_Filter()
        {
            var view = mc.SpawnCustomView(null, views => views.FirstOrDefault(v => v is PairView) as PairView);
            Assert.NotNull(view);
        }

        [Test]
        public void SpawnCustomView_With_Filter_Returns_Null_When_Filter_Does_Not_Match_Any_View()
        {
            var view = mc.SpawnCustomView(null, (views) => views.FirstOrDefault(v => v is MockView) as MockView);
            Assert.Null(view);
        }

        [Test]
        public void SpawnCustomView_With_Init_Arguments_Returns_View_With_Init_Arguments_Passed_To_Init_Function()
        {
            Assert.Catch(() => mc.SpawnCustomView<BridgeView>(null, null, true, "yes", 1));
        }

        [Test]
        public void
            SpawnCustomView_With_Init_Arguments_Returns_View_With_Correct_Init_Arguments_Passed_To_Init_Function()
        {
            var simpleBridge = new Bridge(new BridgeNode("a", Vector3.zero), new BridgeNode("b", Vector3.zero));
            Assert.DoesNotThrow(() => mc.SpawnView(null, null, "a", simpleBridge.GetStart(), simpleBridge.GetEnd(), 2,
                new Gradient()));
        }

        [TearDown]
        public void Teardown()
        {
            if (mv)
                Object.DestroyImmediate(mv.gameObject);

            mc.Dispose();
            mc = null;
        }
    }
}