using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using HamerSoft.Core;
using MaTcheDD.UseCases;
using MaTcheDD.UseCases.Sliding;
using MaTcheDD.Views;
using NUnit.Framework;
using UnityEngine;

namespace MaTcheDD.Specs.UseCases.SlideHeads
{
    [TestFixture]
    public class PathTests
    {
        internal class MockHead : Head
        {
            public void CallAwake()
            {
                Awake();
            }
            
            
        }

        internal class MockBase : Base
        {
            public void CallAwake()
            {
                Awake();
            }
        }

        internal class MockPairView : PairView
        {
            public void CallStart()
            {
                Awake();
                Start();
            }
        }

        internal class MockPath : Path
        {
            public MockPath(PairView pair, Vector3 startPos, float speed, SlideSounds slideSounds) : base(pair, startPos, speed,slideSounds)
            {
            }

            public PairView GetStartPair()
            {
                return typeof(Path).GetField("_startPairView", BindingFlags.Instance | BindingFlags.NonPublic)
                    .GetValue(this) as PairView;
            }

            public PairView GetEndPair()
            {
                return typeof(Path).GetField("_endPairView", BindingFlags.Instance | BindingFlags.NonPublic)
                    .GetValue(this) as PairView;
            }

            public Queue<Vector3> GetQueue()
            {
                return typeof(Path).GetField("_path", BindingFlags.Instance | BindingFlags.NonPublic)
                    .GetValue(this) as Queue<Vector3>;
            }

            public Head GetHead()
            {
                return typeof(Path).GetField("_head", BindingFlags.Instance | BindingFlags.NonPublic)
                    .GetValue(this) as Head;
            }
        }

        private MockPairView[] _pairs;
        private const int MaxPairs = 6;
        private MockPath _mockPath;

        [SetUp]
        public void Setup()
        {
            Main main = Main.Load();
            main.Init();
            SetupScene();
            _mockPath = new MockPath(_pairs[0], Vector3.zero, 1, new SlideSounds());
        }

        private void SetupScene()
        {
            _pairs = new MockPairView[MaxPairs];
            ColumnView cv = new GameObject("columnView").AddComponent<ColumnView>();
            for (int i = 0; i < MaxPairs; i++)
            {
                MockPairView pv = new GameObject("PairView", typeof(MockHead), typeof(MockBase))
                    .AddComponent<MockPairView>();
                pv.transform.SetParent(cv.transform);
                pv.transform.localPosition = new Vector3(0, -930, 0);
                pv.CallStart();
                pv.GetComponent<MockHead>().CallAwake();
                pv.GetComponent<MockBase>().CallAwake();
                _pairs[i] = pv;
            }
        }

        [Test]
        public void When_Path_Is_Constructed_It_Will_Set_The_Start_PairView()
        {
            Assert.NotNull(_mockPath.GetStartPair());
        }

        [Test]
        public void When_Path_Is_Constructed_It_Will_Not_Set_The_End_PairView()
        {
            Assert.Null(_mockPath.GetEndPair());
        }

        [Test]
        public void When_Path_Is_Constructed_It_Enqueue_The_Vector3_Given_In_The_Constructor()
        {
            Assert.AreEqual(Vector3.zero, _mockPath.GetQueue().Peek());
        }

        [Test]
        public void When_Path_Is_Constructed_It_Enqueue_There_Will_Be_One_Vector_In_The_Path_Queue()
        {
            Assert.AreEqual(1, _mockPath.GetQueue().Count);
        }

        [Test]
        public void AddPosition_Adds_A_New_Position_To_The_Path()
        {
            _mockPath.AddPosition(Vector3.one, _pairs[1]);
            Assert.AreEqual(2, _mockPath.GetQueue().Count);
        }

        [Test]
        public void AddPosition_Adds_The_Given_Vector_To_The_Path()
        {
            _mockPath.AddPosition(Vector3.one, _pairs[1]);
            Assert.AreEqual(Vector3.one, _mockPath.GetQueue().Last());
        }

        [Test]
        public void AddPosition_Sets_The_EndPairView()
        {
            _mockPath.AddPosition(Vector3.one, _pairs[1]);
            Assert.NotNull(_mockPath.GetEndPair());
        }

        [Test]
        public void AddPosition_Allows_Start_And_EndPair_To_Be_The_Same_Object()
        {
            _mockPath.AddPosition(Vector3.one, _pairs[0]);
            Assert.AreSame(_mockPath.GetStartPair(), _mockPath.GetEndPair());
        }

        [Test]
        public void AddPosition_Always_Replaces_The_EndPairView()
        {
            _mockPath.AddPosition(Vector3.one, _pairs[1]);
            var endPair = _mockPath.GetEndPair();
            _mockPath.AddPosition(Vector3.one, _pairs[2]);
            Assert.AreNotSame(endPair, _mockPath.GetEndPair());
        }

        [Test]
        public void IsFinished_Is_False_When_There_Is_One_Vector_In_The_Queue()
        {
            Assert.False(_mockPath.IsFinished());
        }

        [Test]
        public void IsFinished_Is_False_When_There_Are_Multiple_Vectors_In_The_Queue()
        {
            _mockPath.AddPosition(Vector3.one, _pairs[1]);
            Assert.False(_mockPath.IsFinished());
        }

        [Test]
        public void IsFinished_Is_True_When_The_Queue_Is_Empty()
        {
            _mockPath.GetQueue().Dequeue();
            Assert.True(_mockPath.IsFinished());
        }

        [Test]
        public void Navigate_Will_Not_Dequeue_Head_PairView_Is_Not_Close_Enough_To_Current_Vector_In_Queue()
        {
            _mockPath.Navigate();
            Assert.False(_mockPath.IsFinished());
        }

        [Test]
        public void Navigate_Will_Move_The_Head_PairView_When_It_Is_Not_Close_Enough_To_Current_Vector_In_Queue()
        {
            var head = _mockPath.GetHead();
            var positionBeforeNavigate = head.transform.localPosition;
            _mockPath.Navigate();
            Assert.AreNotEqual(positionBeforeNavigate, head.transform.localPosition);
        }

        [Test]
        public void Navigate_Will_Dequeue_Head_PairView_When_It_Is_Close_Enough_To_Current_Vector_In_Queue()
        {
            var head = _mockPath.GetHead();
            head.transform.localPosition = Vector3.zero;
            _mockPath.Navigate();
            Assert.AreEqual(0, _mockPath.GetQueue().Count);
        }

        [Test]
        public void Dispose_Will_Nullify_Head_And_Path()
        {
            _mockPath.Dispose();
            Assert.IsNull(_mockPath.GetHead());
            Assert.IsNull(_mockPath.GetQueue());
        }

        [Test]
        public void EqualBases_Returns_True_When_Start_And_End_PairView_Are_The_Same()
        {
            _mockPath.AddPosition(Vector3.one, _pairs[0]);
            Assert.True(_mockPath.EqualBases());
        }

        [Test]
        public void EqualBases_Returns_False_When_Start_And_End_PairView_Are_Not_The_Same()
        {
            _mockPath.AddPosition(Vector3.one, _pairs[1]);
            Assert.False(_mockPath.EqualBases());
        }
        
        [Test]
        public void EqualBases_Returns_False_When_End_PairView_Is_Null()
        {
            Assert.False(_mockPath.EqualBases());
        }

        [Test]
        public void SetHeadToBase_Does_Not_Switch_Head_Position_When_Head_Is_At_The_Correct_Base()
        {
            _mockPath.AddPosition(Vector3.one, _pairs[0]);
            var head = _mockPath.GetHead();
            string instanceId = head.GetComponentInParent<PairView>().Id;
            _mockPath.GetQueue().Dequeue();
            _mockPath.GetQueue().Dequeue();
            _mockPath.SetHeadToEndBase();
            Assert.AreEqual(instanceId, head.GetComponentInParent<PairView>().Id);
        }

        [Test]
        public void SetHeadToBase_Does_Not_Switch_Head_Position_When_The_Queue_is_Not_Empty()
        {
            _mockPath.AddPosition(Vector3.one, _pairs[1]);
            var head = _mockPath.GetHead();
            string instanceId = head.GetComponentInParent<PairView>().Id;
            _mockPath.SetHeadToEndBase();
            Assert.AreEqual(instanceId, head.GetComponentInParent<PairView>().Id);
        }

        [Test]
        public void SetHeadToBase_Does_Switch_Head_Position_When_Head_Is_Not_At_The_Correct_Base()
        {
            _mockPath.AddPosition(Vector3.one, _pairs[1]);
            var head = _mockPath.GetHead();
            _mockPath.GetQueue().Dequeue();
            _mockPath.GetQueue().Dequeue();
            _mockPath.SetHeadToEndBase();
            Assert.AreEqual(head.GetId(), _mockPath.GetEndPair().GetHead().GetId());
        }

        [Test]
        public void Path_IsCorrect_When_PairView_IsMatched()
        {
            _mockPath.AddPosition(Vector3.one, _pairs[0]);
            _mockPath.GetQueue().Dequeue();
            _mockPath.GetQueue().Dequeue();
            Assert.True(_mockPath.IsCorrect());
        }
        
        [Test]
        public void Path_Is_Not_IsCorrect_When_PairView_Not_IsMatched()
        {
            _mockPath.AddPosition(Vector3.one, _pairs[1]);
            _mockPath.GetQueue().Dequeue();
            _mockPath.GetQueue().Dequeue();
            _mockPath.SetHeadToEndBase();
            Assert.False(_mockPath.IsCorrect());
        }

        [TearDown]
        public void Teardown()
        {
        }
    }
}