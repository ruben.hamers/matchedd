using System.Reflection;
using HamerSoft.Core;
using HamerSoft.UnityMvc;
using MaTcheDD.UseCases;
using MaTcheDD.UseCases.Menu;
using MaTcheDD.Views;
using NUnit.Framework;
using UnityEngine;

namespace MaTcheDD.Specs.UseCases.Menu
{
    public class BackToMenuTests
    {
        private BackToMenu _backToMenu;
        private Main _main;
        private MockController _mockController;
        private GameObject _root;

        private class MockView : BaseView
        {
            protected override object[] CustomStartArgs { get; }
        }

        private class MockController : BaseController<MockView>
        {
            protected override void AssignViewEvents(MockView view)
            {
            }

            protected override void UnAssignViewEvents(MockView view)
            {
                
            }
        }

        [SetUp]
        public void Setup()
        {
            _main = Main.Load();
            _main.Init();
            _mockController = new MockController();
            _mockController.SetMain(_main);
            _backToMenu = null;
            _root = new GameObject("Root");
        }

        ChoiceView SpawnView(object[] args)
        {
            return _mockController.SpawnCustomView<ChoiceView>(_root.transform.transform, null, args);
        }

        [Test]
        public void Upon_Construction_The_SpawnView_Callback_Is_Set()
        {
            _backToMenu = new BackToMenu(SpawnView);
            Assert.NotNull(typeof(BackToMenu).GetField("_spawnAction", BindingFlags.Instance | BindingFlags.NonPublic)
                .GetValue(_backToMenu));
        }

        [Test]
        public void Upon_Execute_The_View_Is_Spawned()
        {
            _backToMenu = new BackToMenu(SpawnView);
            _backToMenu.Execute(b => { Assert.NotNull(_root.GetComponentInChildren<ChoiceView>()); });
        }

        [Test]
        public void When_UseCase_Is_Disposed_The_View_Is_Destroyed()
        {
            _backToMenu = new BackToMenu(SpawnView);
            _backToMenu.Execute(b =>
            {
                _backToMenu.Dispose();
                Assert.Null(_root.GetComponentInChildren<ChoiceView>());
            });
        }
    }
}