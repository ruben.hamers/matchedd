using HamerSoft.Core;
using HamerSoft.UnityMvc.Specs.RunTime.Mocks;
using MaTcheDD.UseCases.Menu;
using MaTcheDD.Views;
using NUnit.Framework;
using UnityEngine;

namespace MaTcheDD.Specs.UseCases.Menu
{
    public class QuitGameTests
    {
        private QuitGame _quitGame;
        private Main _main;
        private MockController _mockController;
        private GameObject _root;

        [SetUp]
        public void Setup()
        {
            _main = Main.Load();
            _main.Init();
            _mockController = new MockController();
            _mockController.SetMain(_main);
            _root = new GameObject("Root");
            _quitGame = new QuitGame(SpawnView);
        }

        ChoiceView SpawnView(object[] args)
        {
            return _mockController.SpawnCustomView<ChoiceView>(_root.transform.transform, null, args);
        }
        
        [Test]
        public void Upon_Execute_The_View_Is_Spawned()
        {
            _quitGame = new QuitGame(SpawnView);
            _quitGame.Execute(() => { Assert.NotNull(_root.GetComponentInChildren<ChoiceView>()); });
        }
    }
}