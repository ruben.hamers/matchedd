using System.Reflection;
using HamerSoft.Core;
using MaTcheDD.Entities;
using MaTcheDD.Entities.Repositories.Bridge;
using MaTcheDD.UseCases.Lines.Bridge;
using NUnit.Framework;
using UnityEngine;

namespace MaTcheDD.Specs.UseCases.Drawing
{
    [TestFixture]
    public class AddBridgeTests
    {
        private class MockAddBridge : AddBridge
        {
            public BridgeRepository GetRepo()
            {
                return typeof(AddBridge).GetField("_repository", BindingFlags.Instance | BindingFlags.NonPublic)
                    .GetValue(this) as BridgeRepository;
            }

            public BridgeNode GetStart()
            {
                return typeof(AddBridge).GetField("_start", BindingFlags.Instance | BindingFlags.NonPublic)
                    .GetValue(this) as BridgeNode;
            }

            public BridgeNode GetEnd()
            {
                return typeof(AddBridge).GetField("_end", BindingFlags.Instance | BindingFlags.NonPublic)
                    .GetValue(this) as BridgeNode;
            }

            public MockAddBridge(Main main, BridgeNode start, BridgeNode end) : base(main, start, end)
            {
            }
        }

        private class RepositoryListener : IRepositoryListener<Bridge>
        {
            public Bridge Added { get; set; }

            public void AddedObject(Bridge added)
            {
                Added = added;
            }

            public void RemovedObject(Bridge removed)
            {
                throw new System.NotImplementedException();
            }

            public void UpdatedObject(Bridge updated)
            {
                throw new System.NotImplementedException();
            }
        }

        private MockAddBridge _addBridge;
        private Main _main;
        private BridgeNode _start, _end;
        private RepositoryListener _listener;

        [SetUp]
        public void Setup()
        {
            _main = Main.Load();
            _main.Init();
            _listener = new RepositoryListener();
            _main.GetRepository<BridgeRepository>().AddListener(_listener);
            _end = new BridgeNode("b", Vector3.down);
            _start = new BridgeNode("a", Vector3.one);
            _addBridge = new MockAddBridge(_main, _start, _end);
        }

        [Test]
        public void When_AddBridge_Is_Constructed_The_Repository_Is_Set()
        {
            Assert.NotNull(_addBridge.GetRepo());
        }

        [Test]
        public void When_AddBridge_Is_Constructed_StartNode_Is_Set()
        {
            Assert.NotNull(_addBridge.GetStart());
        }

        [Test]
        public void When_AddBridge_Is_Constructed_EndNode_Is_Set()
        {
            Assert.NotNull(_addBridge.GetEnd());
        }

        [Test]
        public void When_AddBridge_Is_Constructed_StartNode_Is_Set_Correctly()
        {
            Assert.AreEqual(_start, _addBridge.GetStart());
        }

        [Test]
        public void When_AddBridge_Is_Constructed_EndNode_Is_Set_Correctly()
        {
            Assert.AreEqual(_end, _addBridge.GetEnd());
        }
        
        [Test]
        public void When_AddBridge_Is_Executed_A_Bridge_Is_Added_To_The_Repository()
        {
            _addBridge.Execute(() =>
            {
                Assert.AreEqual(_start.PairView, _listener.Added.GetStart().PairView);
                Assert.AreEqual(_start.Position, _listener.Added.GetStart().Position);
                Assert.AreEqual(_end.PairView, _listener.Added.GetEnd().PairView);
                Assert.AreEqual(_end.Position, _listener.Added.GetEnd().Position);
            });
        }
    }
}