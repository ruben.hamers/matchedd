using System;
using System.Reflection;
using geniikw.DataRenderer2D;
using HamerSoft.Core;
using HamerSoft.Core.UseCases;
using MaTcheDD.Entities;
using MaTcheDD.Lines;
using MaTcheDD.Specs.Mocks;
using MaTcheDD.UseCases.Lines.Drawing;
using MaTcheDD.Views;
using NUnit.Framework;
using UnityEngine;

namespace MaTcheDD.Specs.UseCases.Drawing
{
    [TestFixture]
    public class ValidateDynamicLineTests
    {
        private class MockValidateDynamicLine : ValidateDynamicLine
        {
            public MockValidateDynamicLine(Main main, DynamicLine dynamicLine, PairView[] pairs) : base(main,
                dynamicLine, pairs)
            {
            }

            public PairView GetStartPairView()
            {
                return typeof(ValidateDynamicLine)
                    .GetField("_startPair", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(this) as PairView;
            }

            public PairView GetEndPairView()
            {
                return typeof(ValidateDynamicLine).GetField("_endPair", BindingFlags.Instance | BindingFlags.NonPublic)
                    .GetValue(this) as PairView;
            }

            public void SetStartPairView(PairView view)
            {
                typeof(ValidateDynamicLine)
                    .GetField("_startPair", BindingFlags.Instance | BindingFlags.NonPublic).SetValue(this, view);
            }

            public void SetEndPairView(PairView view)
            {
                typeof(ValidateDynamicLine).GetField("_endPair", BindingFlags.Instance | BindingFlags.NonPublic)
                    .SetValue(this, view);
            }

            public Vector3 GetStartPosition()
            {
                return (Vector3) typeof(ValidateDynamicLine)
                    .GetField("_startPosition", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(this);
            }

            public Vector3 GetEndPosition()
            {
                return (Vector3) typeof(ValidateDynamicLine)
                    .GetField("_endPosition", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(this);
            }

            public void SetPairView(PairView pair, Vector3 position)
            {
                typeof(ValidateDynamicLine).GetMethod("SetPairView", BindingFlags.Instance | BindingFlags.NonPublic)
                    .Invoke(this, new object[] {pair, position});
            }

            public bool IsPositionRightOfStaticLine(StaticLine line, Vector3 position)
            {
                return (bool) typeof(ValidateDynamicLine).GetMethod("IsPositionRightOfStaticLine",
                        BindingFlags.Instance | BindingFlags.NonPublic)
                    .Invoke(this, new object[] {line, position});
            }

            public bool IsPositionLeftOfStaticLine(StaticLine line, Vector3 position)
            {
                return (bool) typeof(ValidateDynamicLine).GetMethod("IsPositionLeftOfStaticLine",
                        BindingFlags.Instance | BindingFlags.NonPublic)
                    .Invoke(this, new object[] {line, position});
            }

            public bool DoesLineIntersect(Vector2 startLine1, Vector2 endLine1, Vector2 startLine2, Vector2 endLine2)
            {
                return (bool) typeof(ValidateDynamicLine).GetMethod("DoesLineIntersect",
                        BindingFlags.Instance | BindingFlags.NonPublic)
                    .Invoke(this, new object[] {startLine1, endLine1, startLine2, endLine2});
            }

            public void Finish()
            {
                typeof(ValidateDynamicLine).GetMethod("Finish",
                        BindingFlags.Instance | BindingFlags.NonPublic)
                    .Invoke(this, null);
            }

            public void SetCallback(Action<BridgeNode, BridgeNode> callback)
            {
                var fi = typeof(UseCase<BridgeNode, BridgeNode>)
                    .GetField("_callback", BindingFlags.Instance | BindingFlags.NonPublic);
                fi.SetValue(this, callback);
            }
        }


        private MockValidateDynamicLine _validateDynamicLine;
        private MockCanvas _canvas;

        [SetUp]
        public void Setup()
        {
            Main main = Main.Load();
            main.Init();
            _canvas = new GameObject("Canvas").AddComponent<MockCanvas>();
            _validateDynamicLine = new MockValidateDynamicLine(main, null, null);
        }

        [Test]
        public void IsPositionRightOfStaticLine_Returns_True_When_Position_Is_Right_Of_Static_Line()
        {
            var staticLine = new GameObject("StaticLine", typeof(UILine)).AddComponent<StaticLine>();
            Assert.True(_validateDynamicLine.IsPositionRightOfStaticLine(staticLine, new Vector3(100, 0, 0)));
        }

        [Test]
        public void IsPositionRightOfStaticLine_Returns_False_When_Position_Is_Equal_To_Static_Line()
        {
            var staticLine = new GameObject("StaticLine", typeof(UILine)).AddComponent<StaticLine>();
            Assert.False(_validateDynamicLine.IsPositionRightOfStaticLine(staticLine, new Vector3(0, 0, 0)));
        }

        [Test]
        public void IsPositionRightOfStaticLine_Returns_False_When_Position_Is_Left_Of_Static_Line()
        {
            var staticLine = new GameObject("StaticLine", typeof(UILine)).AddComponent<StaticLine>();
            Assert.False(_validateDynamicLine.IsPositionRightOfStaticLine(staticLine, new Vector3(-100, 0, 0)));
        }

        [Test]
        public void IsPositionLeftOfStaticLine_Returns_True_When_Position_Is_Left_Of_Static_Line()
        {
            var staticLine = new GameObject("StaticLine", typeof(UILine)).AddComponent<StaticLine>();
            Assert.True(_validateDynamicLine.IsPositionLeftOfStaticLine(staticLine, new Vector3(-100, 0, 0)));
        }

        [Test]
        public void IsPositionLeftOfStaticLine_Returns_False_When_Position_Is_Equal_To_Static_Line()
        {
            var staticLine = new GameObject("StaticLine", typeof(UILine)).AddComponent<StaticLine>();
            Assert.False(_validateDynamicLine.IsPositionLeftOfStaticLine(staticLine, new Vector3(0, 0, 0)));
        }

        [Test]
        public void IsPositionLeftOfStaticLine_Returns_False_When_Position_Is_Right_Of_Static_Line()
        {
            var staticLine = new GameObject("StaticLine", typeof(UILine)).AddComponent<StaticLine>();
            Assert.False(_validateDynamicLine.IsPositionLeftOfStaticLine(staticLine, new Vector3(100, 0, 0)));
        }

        [Test]
        public void DoesLineIntersect_Does_Not_Intersect_When_Lines_Are_Not_Crossing_Each_Other()
        {
            Assert.False(_validateDynamicLine.DoesLineIntersect(Vector2.zero, new Vector2(0, 1), new Vector2(2, 2),
                new Vector2(2, 3)));
        }

        [Test]
        public void DoesLineIntersect_Does_Intersect_When_Lines_Are_Crossing_Each_Other()
        {
            Assert.True(_validateDynamicLine.DoesLineIntersect(Vector2.zero, new Vector2(0, 1), new Vector2(-1, 0),
                new Vector2(2, 1)));
        }

        [Test]
        public void DoesLineIntersect_Does_Intersect_When_Lines_Are_Equal()
        {
            Assert.True(_validateDynamicLine.DoesLineIntersect(Vector2.zero, new Vector2(0, 1), Vector2.zero,
                new Vector2(0, 1)));
        }

        [Test]
        public void SetPairView_Sets_The_StartPair_When_It_Is_Null()
        {
            PairView p = new GameObject("PairView").AddComponent<PairView>();
            _validateDynamicLine.SetPairView(p, Vector2.one);
            Assert.NotNull(_validateDynamicLine.GetStartPairView());
        }

        [Test]
        public void SetPairView_Sets_The_StartPosition_When_PairView_Is_Null()
        {
            PairView p = new GameObject("PairView").AddComponent<PairView>();
            _validateDynamicLine.SetPairView(p, Vector2.one);
            Assert.AreEqual(new Vector3(0, 1, 0), _validateDynamicLine.GetStartPosition());
        }

        [Test]
        public void SetPairView_Sets_The_StartPair_Correct_StartPair()
        {
            PairView p = new GameObject("PairView").AddComponent<PairView>();
            _validateDynamicLine.SetPairView(p, Vector2.one);
            Assert.AreEqual(p.GetInstanceID(), _validateDynamicLine.GetStartPairView().GetInstanceID());
        }

        [Test]
        public void SetPairView_Does_Not_Set_EndView_When_It_Is_Equal_To_StartView()
        {
            PairView p = new GameObject("PairView").AddComponent<PairView>();
            _validateDynamicLine.SetPairView(p, Vector2.one);
            _validateDynamicLine.SetPairView(p, Vector2.one);
            Assert.Null(_validateDynamicLine.GetEndPairView());
        }

        [Test]
        public void SetPairView_Does_Not_Set_StarView_When_StartView_Is_Already_Set()
        {
            PairView p = new GameObject("PairView").AddComponent<PairView>();
            PairView pEnd = new GameObject("PairView").AddComponent<PairView>();
            _validateDynamicLine.SetPairView(p, Vector2.one);
            _validateDynamicLine.SetPairView(pEnd, Vector2.zero);
            Assert.AreEqual(p.GetInstanceID(), _validateDynamicLine.GetStartPairView().GetInstanceID());
        }

        [Test]
        public void SetPairView_Does_Not_Set_StartPosition_When_StartView_Is_Already_Set()
        {
            PairView p = new GameObject("PairView").AddComponent<PairView>();
            PairView pEnd = new GameObject("PairView").AddComponent<PairView>();
            _validateDynamicLine.SetPairView(p, Vector2.one);
            _validateDynamicLine.SetPairView(pEnd, Vector2.zero);
            Assert.AreEqual(new Vector3(0, 1, 0), _validateDynamicLine.GetStartPosition());
        }

        [Test]
        public void SetPairView_Does_Set_EndView_When_StartView_Is_Already_Set()
        {
            PairView p = new GameObject("PairView").AddComponent<PairView>();
            PairView pEnd = new GameObject("PairView").AddComponent<PairView>();
            _validateDynamicLine.SetPairView(p, Vector2.one);
            _validateDynamicLine.SetPairView(pEnd, Vector2.zero);
            Assert.NotNull(_validateDynamicLine.GetEndPairView());
        }

        [Test]
        public void SetPairView_Does_Set_EndView_When_StartView_Is_Already_Set_Correctly()
        {
            PairView p = new GameObject("PairView").AddComponent<PairView>();
            PairView pEnd = new GameObject("PairView").AddComponent<PairView>();
            _validateDynamicLine.SetPairView(p, Vector2.one);
            _validateDynamicLine.SetPairView(pEnd, Vector2.zero);
            Assert.AreEqual(pEnd.GetInstanceID(), _validateDynamicLine.GetEndPairView().GetInstanceID());
        }

        [Test]
        public void SetPairView_Does_Set_EndPosition_When_StartView_Is_Already_Set_Correctly()
        {
            PairView p = new GameObject("PairView").AddComponent<PairView>();
            PairView pEnd = new GameObject("PairView").AddComponent<PairView>();
            _validateDynamicLine.SetPairView(p, Vector2.one);
            _validateDynamicLine.SetPairView(pEnd, Vector2.zero);
            Assert.AreEqual(Vector3.zero, _validateDynamicLine.GetEndPosition());
        }

        [Test]
        public void Finish_Returns_Null_For_Both_BridgeNodes_When_Start_And_EndPair_Are_Null()
        {
            BridgeNode b1 = null, b2 = null;
            Action<BridgeNode, BridgeNode> callback = (node, bridgeNode) =>
            {
                b1 = node;
                b2 = node;
            };

            _validateDynamicLine.SetCallback(callback);
            _validateDynamicLine.Finish();

            Assert.Null(b1);
            Assert.Null(b2);
        }
        
        [Test]
        public void Finish_Returns_Null_For_Both_BridgeNodes_When_EndPair_Is_Null()
        {
            BridgeNode b1 = null, b2 = null;
            Action<BridgeNode, BridgeNode> callback = (node, bridgeNode) =>
            {
                b1 = node;
                b2 = node;
            };
            _validateDynamicLine.SetStartPairView(new GameObject("pair1").AddComponent<PairView>());
            _validateDynamicLine.SetCallback(callback);
            _validateDynamicLine.Finish();

            Assert.Null(b1);
            Assert.Null(b2);
        }
        
        [Test]
        public void Finish_Returns_Null_For_Both_BridgeNodes_When_StartPair_Is_Null()
        {
            BridgeNode b1 = null, b2 = null;
            Action<BridgeNode, BridgeNode> callback = (node, bridgeNode) =>
            {
                b1 = node;
                b2 = node;
            };
            _validateDynamicLine.SetEndPairView(new GameObject("pair1").AddComponent<PairView>());
            _validateDynamicLine.SetCallback(callback);
            _validateDynamicLine.Finish();

            Assert.Null(b1);
            Assert.Null(b2);
        }

        [Test]
        public void Finish_Returns_Values_For_Both_BridgeNodes_When_Start_And_EndPair_Are_Set()
        {
            Action<BridgeNode, BridgeNode> callback = (node, bridgeNode) =>
            {
                Assert.NotNull(node);
                Assert.NotNull(bridgeNode);
            };
            _validateDynamicLine.SetStartPairView(new GameObject("pair1").AddComponent<PairView>());
            _validateDynamicLine.SetEndPairView(new GameObject("pair2").AddComponent<PairView>());
            _validateDynamicLine.SetCallback(callback);
            _validateDynamicLine.Finish();
        }


        [TearDown]
        public void TearDown()
        {
        }
    }
}