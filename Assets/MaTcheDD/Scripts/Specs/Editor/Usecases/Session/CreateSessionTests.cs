using System.IO;
using System.Reflection;
using HamerSoft.Core;
using MaTcheDD.Entities.Repositories;
using MaTcheDD.Entities;
using MaTcheDD.UseCases;
using NUnit.Framework;
using UnityEngine;

namespace MaTcheDD.Specs.UseCases.Session
{
    [TestFixture]
    public class CreateSessionTests
    {
        internal class MockCreateSession : CreateSession
        {
            public MockCreateSession(Main main, MaTcheDD.Entities.Player player, int tracks) : base(main, player,tracks)
            {
            }

            public SessionRepository GetRepo()
            {
                return typeof(CreateSession).GetField("_repository", BindingFlags.Instance | BindingFlags.NonPublic)
                    .GetValue(this) as SessionRepository;
            }
        }

        private MockCreateSession _createSession;
        private MaTcheDD.Entities.Player _player;
        private MaTcheDD.Entities.Session _session;
        private Main _main;
        private readonly string filePath = $"{Application.persistentDataPath}/Entities/Sessions";

        [SetUp]
        public void Setup()
        {
            _main = Main.Load();
            _main.Init();
            _player = new MaTcheDD.Entities.Player("Foo");
            _createSession = new MockCreateSession(_main, _player, 3);
        }

        [Test]
        public void When_CreateSession_Is_Constructed_The_Repository_Is_Set()
        {
            Assert.NotNull(_createSession.GetRepo());
        }

        [Test]
        public void When_CreateSession_Is_Executed_A_New_Session_Is_Created()
        {
            _createSession.Execute(() => { Assert.NotNull(_main.GetPlugin<MatchedPlugin>().Session); });
        }

        [Test]
        public void When_CreateSession_Is_Executed_A_New_Session_For_Given_Player_Is_Created()
        {
            _createSession.Execute(() =>
            {
                Assert.AreEqual(_player.Id, _main.GetPlugin<MatchedPlugin>().Session.GetPlayerId());
            });
        }

        [Test]
        public void When_CreateSession_Is_Executed_The_Session_Object_Is_Not_Persisted()
        {
            string[] files = Directory.GetFiles(filePath);
            _createSession.Execute(() => { Assert.AreEqual(files, Directory.GetFiles(filePath)); });
        }

        [TearDown]
        public void Teardown()
        {
            _createSession.Dispose();
        }
    }
}