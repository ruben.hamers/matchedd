using System.IO;
using System.Reflection;
using HamerSoft.Core;
using MaTcheDD.Entities.Repositories;
using MaTcheDD.Entities;
using MaTcheDD.UseCases;
using NUnit.Framework;
using UnityEngine;

namespace MaTcheDD.Specs.UseCases.Session
{
    [TestFixture]
    public class UpdateSessionTests
    {
        internal class MockUpdateSession : UpdateSession
        {
            public MockUpdateSession(Main main, bool succeeded) : base(main, succeeded)
            {
            }

            public void OverwriteSuccess(bool success)
            {
                typeof(UpdateSession).GetField("_succeeded", BindingFlags.Instance | BindingFlags.NonPublic)
                    .SetValue(this, success);
            }

            public bool GetSucceeded()
            {
                return (bool) typeof(UpdateSession)
                    .GetField("_succeeded", BindingFlags.Instance | BindingFlags.NonPublic)
                    .GetValue(this);
            }


            public SessionRepository GetRepo()
            {
                return typeof(UpdateSession).GetField("_repository", BindingFlags.Instance | BindingFlags.NonPublic)
                    .GetValue(this) as SessionRepository;
            }
        }

        private MockUpdateSession _updateSession;
        private Main _main;
        private readonly string filePath = $"{Application.persistentDataPath}/Entities/Sessions";
        private MaTcheDD.Entities.Session _session;

        [SetUp]
        public void Setup()
        {
            _main = Main.Load();
            _main.Init();
            _session = new MaTcheDD.Entities.Session("Foo",3);
            _main.GetPlugin<MatchedPlugin>().Session = _session;
            _updateSession = new MockUpdateSession(_main, true);
        }

        [Test]
        public void When_UpdateSession_Is_Constructed_The_Repository_Is_Set()
        {
            Assert.NotNull(_updateSession.GetRepo());
        }

        [Test]
        public void When_UpdateSession_Is_Constructed_Succeeded_Is_Set()
        {
            Assert.True(_updateSession.GetSucceeded());
        }

        [Test]
        public void When_UpdateSession_Is_Executed_While_Not_Succeeded_The_Session_Is_Persisted()
        {
            _updateSession.GetRepo().AddObject(_session);
            _updateSession.OverwriteSuccess(false);
            string[] files = Directory.GetFiles(filePath);
            _updateSession.Execute(done => { Assert.True(files.Length < Directory.GetFiles(filePath).Length); });
        }

        [Test]
        public void When_UpdateSession_Is_Executed_While_Not_Succeeded_The_Session_Is_Finished()
        {
            _updateSession.OverwriteSuccess(false);
            _updateSession.Execute(done => { Assert.True(_session.GetFinished()); });
        }

        [Test]
        public void When_UpdateSession_Is_Executed_While_Succeeded_The_Session_Is_Not_Persisted()
        {
            string[] files = Directory.GetFiles(filePath);
            _updateSession.Execute(done => { Assert.True(files.Length == Directory.GetFiles(filePath).Length); });
        }

        [Test]
        public void When_UpdateSession_Is_Executed_While_Succeeded_The_Session_Is_Not_Finished()
        {
            _updateSession.Execute(done => { Assert.False(_session.GetFinished()); });
        }

        [Test]
        public void When_UpdateSession_Is_Executed_While_Succeeded_The_Session_Is_A_Round_Is_Added()
        {
            _updateSession.Execute(done => { Assert.True(_session.GetRounds() > 0); });
        }

        [TearDown]
        public void Teardown()
        {
            if (_session != null)
            {
                _updateSession.GetRepo().RemoveObject(ref _session);
                _updateSession.GetRepo().Flush();
            }

            _updateSession.Dispose();
        }
    }
}