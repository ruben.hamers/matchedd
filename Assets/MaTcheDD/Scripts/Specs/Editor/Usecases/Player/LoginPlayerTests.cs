using System;
using System.IO;
using System.Reflection;
using HamerSoft.Core;
using MaTcheDD.Entities.Repositories;
using MaTcheDD.UseCases.Player;
using Newtonsoft.Json;
using NUnit.Framework;
using UnityEngine;

namespace MaTcheDD.Specs.UseCases.Player
{
    public class LoginPlayerTests
    {
        internal class MockLoginPlayer : LoginPlayer
        {
            public PlayerRepository GetRepo()
            {
                return typeof(LoginPlayer).GetField("_repository", BindingFlags.Instance | BindingFlags.NonPublic)
                    .GetValue(this) as PlayerRepository;
            }

            public string GetName()
            {
                return typeof(LoginPlayer).GetField("_name", BindingFlags.Instance | BindingFlags.NonPublic)
                    .GetValue(this) as string;
            }

            public MockLoginPlayer(Main main, string name) : base(main, name)
            {
            }
        }

        private MockLoginPlayer _loginPlayer;
        private MaTcheDD.Entities.Player _player;
        private Main _main;
        private readonly string filePath = $"{Application.persistentDataPath}/Entities/Player";

        [SetUp]
        public void Setup()
        {
            _main = Main.Load();
            _main.Init();
            var repo = _main.GetRepository<PlayerRepository>();
            var fooPlayer = repo.GetPlayerByName("Foo");
            if (fooPlayer != null)
            {
                repo.RemoveObject(ref fooPlayer);
                repo.Flush();
            }
            _loginPlayer = new MockLoginPlayer(_main, "Foo");
        }

        [Test]
        public void When_LoginPlayer_Is_Constructed_Name_Is_Set()
        {
            Assert.NotNull(_loginPlayer.GetName());
        }

        [Test]
        public void When_LoginPlayer_Is_Constructed_Name_Is_Set_Correctly()
        {
            Assert.AreEqual("Foo", _loginPlayer.GetName());
        }

        [Test]
        public void When_LoginPlayer_Is_Constructed_Repository_Is_Set()
        {
            Assert.NotNull(_loginPlayer.GetRepo());
        }

        [Test]
        public void When_LoginPlayer_Is_Executed_And_There_Is_No_Player_In_Memory_A_New_One_Will_Be_Created()
        {
            _loginPlayer.Execute(player =>
            {
                Assert.NotNull(player);
                _player = player;
            });
        }

        [Test]
        public void When_LoginPlayer_Is_Executed_And_There_Is_A_Player_In_Memory_No_New_Player_Will_Be_Created()
        {
            var player = new MaTcheDD.Entities.Player("Foo");
            _loginPlayer.GetRepo().AddObject(player);
            _loginPlayer.Execute(usecasePlayer =>
            {
                _player = usecasePlayer;
                Assert.AreEqual(player.Id, usecasePlayer.Id);
            });
        }

        [Test]
        public void When_LoginPlayer_Is_Executed_And_There_Is_A_Player_On_Disk_No_New_Player_Will_Be_Created()
        {
            var player = new MaTcheDD.Entities.Player("Foo");
            File.WriteAllText(Path.Combine(filePath,
                $"{player.Id}.json"), JsonConvert.SerializeObject(player));
            
            _loginPlayer.Execute(usecasePlayer =>
            {
                _player = usecasePlayer;
                Assert.AreEqual(player.Id, usecasePlayer.Id);
            });
        }

        [TearDown]
        public void TearDown()
        {
            try
            {
                if (_player != null)
                {
                    _loginPlayer.GetRepo().RemoveObject(ref _player);
                    _loginPlayer.GetRepo().Flush();
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            _loginPlayer.Dispose();
        }
    }
}