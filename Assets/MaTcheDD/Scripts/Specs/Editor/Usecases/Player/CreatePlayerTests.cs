﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using HamerSoft.Core;
using MaTcheDD.Entities.Repositories;
using MaTcheDD.UseCases.Player;
using NUnit.Framework;
using UnityEngine;

namespace MaTcheDD.Specs.UseCases.Player
{
    [TestFixture]
    public class CreatePlayerTests
    {
        internal class MockCreatePlayer : CreatePlayer
        {
            public PlayerRepository GetRepo()
            {
                return typeof(CreatePlayer).GetField("_repository", BindingFlags.Instance | BindingFlags.NonPublic)
                    .GetValue(this) as PlayerRepository;
            }

            public string GetName()
            {
                return typeof(CreatePlayer).GetField("_name", BindingFlags.Instance | BindingFlags.NonPublic)
                    .GetValue(this) as string;
            }

            public MockCreatePlayer(Main main, string name) : base(main, name)
            {
            }
        }

        private MockCreatePlayer _createPlayer;
        private MaTcheDD.Entities.Player _player;
        private Main _main;
        private readonly string filePath = $"{Application.persistentDataPath}/Entities/Player";

        [SetUp]
        public void Setup()
        {
            _main = Main.Load();
            _main.Init();
            _createPlayer = new MockCreatePlayer(_main, "Foo");
        }

        [Test]
        public void When_CreatePlayer_Is_Constructed_Name_Is_Set()
        {
            Assert.NotNull(_createPlayer.GetName());
        }

        [Test]
        public void When_CreatePlayer_Is_Constructed_Name_Is_Set_Correctly()
        {
            Assert.AreEqual("Foo", _createPlayer.GetName());
        }

        [Test]
        public void When_CreatePlayer_Is_Constructed_Repository_Is_Set()
        {
            Assert.NotNull(_createPlayer.GetRepo());
        }

        [Test]
        public void When_CreatePlayer_Is_Executed_Player_Is_Added_To_Repository()
        {
            _createPlayer.Execute(player =>
            {
                _player = player;
                Assert.NotNull(_createPlayer.GetRepo().GetObject(player.Id)); });
        }

        [Test]
        public void When_CreatePlayer_Is_Executed_Player_With_Correct_Name_Is_Created()
        {
            _createPlayer.Execute(player =>
            {
                _player = player;
                Assert.AreEqual(_createPlayer.GetName(), player.GetName()); });
        }

        [Test]
        public void When_CreatePlayer_Is_Executed_Player_Is_Persisted()
        {
            string[] files = Directory.GetFiles(filePath);
            _createPlayer.Execute(player =>
            {
                _player = player;
                Assert.Less(files.Length, Directory.GetFiles(filePath).Length); });
        }
        
        [TearDown]
        public void TearDown()
        {
            if (_player != null)
            {
                _createPlayer.GetRepo().RemoveObject(ref _player);
                _createPlayer.GetRepo().Flush();
            }
            _createPlayer.Dispose();
        }
    }
}