using System.Reflection;
using HamerSoft.Core;
using HamerSoft.UnityMvc;
using MaTcheDD.UseCases;
using MaTcheDD.Views;
using NUnit.Framework;
using UnityEngine;

namespace MaTcheDD.Specs.UseCases.Game
{
    [TestFixture]
    public class LostGameTests
    {
        private LostGame _lostGame;
        private Main _main;
        private MockController _mockController;
        private GameObject _root;

        private class MockView : BaseView
        {
            protected override object[] CustomStartArgs { get; }
        }

        private class MockController : BaseController<MockView>
        {
            protected override void AssignViewEvents(MockView view)
            {
            }

            protected override void UnAssignViewEvents(MockView view)
            {
                
            }
        }

        [SetUp]
        public void Setup()
        {
            _main = Main.Load();
            _main.Init();
            _mockController = new MockController();
            _mockController.SetMain(_main);
            _lostGame = null;
            _root = new GameObject("Root");
        }

        ChoiceView SpawnView(object[] args)
        {
            return _mockController.SpawnCustomView<ChoiceView>(_root.transform.transform, null, args);
        }

        [Test]
        public void Upon_Construction_The_SpawnView_Callback_Is_Set()
        {
            _lostGame = new LostGame(SpawnView);
            Assert.NotNull(typeof(LostGame).GetField("_spawnAction", BindingFlags.Instance | BindingFlags.NonPublic)
                .GetValue(_lostGame));
        }

        [Test]
        public void Upon_Execute_The_View_Is_Spawned()
        {
            _lostGame = new LostGame(SpawnView);
            _lostGame.Execute(b => { Assert.NotNull(_root.GetComponentInChildren<ChoiceView>()); });
        }

        [Test]
        public void When_UseCase_Is_Disposed_The_View_Is_Destroyed()
        {
            _lostGame = new LostGame(SpawnView);
            _lostGame.Execute(b =>
            {
                _lostGame.Dispose();
                Assert.Null(_root.GetComponentInChildren<ChoiceView>());
            });
        }
    }
}