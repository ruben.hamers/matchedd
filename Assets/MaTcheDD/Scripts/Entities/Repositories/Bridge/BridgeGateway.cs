using System;
using System.Collections.Generic;
using HamerSoft.Core;

namespace MaTcheDD.Entities.Repositories.Bridge
{
    /// <summary>
    /// we create a stub gateway since we do not want any bridges to be persisted to disk, they are just in memory objects
    /// </summary>
    public class BridgeGateway : IGateway<Entities.Bridge>
    {
        public void Persist(Entities.Bridge value)
        {
            
        }

        public void Delete(Entities.Bridge value)
        {
        }

        public void Load(string id, Action<Entities.Bridge> callback)
        {
        }

        public bool Exists(string id)
        {
            return false;
        }

        public IEnumerable<Entities.Bridge> LoadLazy(List<string> idsInMemory)
        {
          return new Entities.Bridge[0];
        }
    }
}