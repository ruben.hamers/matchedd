using System.Collections.Generic;
using HamerSoft.Core;

namespace MaTcheDD.Entities.Repositories.Bridge
{
    public class BridgeRepository : Repository<Entities.Bridge>
    {
        protected override IGateway<Entities.Bridge> CreateGateway()
        {
            return new BridgeGateway();
        }

        public List<Entities.Bridge> GetAll()
        {
            return GetObjectsBy(b => true);
        }

        public void RemoveAll()
        {
            List<Entities.Bridge> bridges = GetAll();
            bridges.ForEach(b=>RemoveObject(ref b));
        }
    }
}