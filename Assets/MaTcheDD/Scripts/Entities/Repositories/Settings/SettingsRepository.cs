using HamerSoft.Core;

namespace MaTcheDD.Entities.Repositories
{
    public class SettingsRepository : Repository<Settings>
    {
        protected override IGateway<Settings> CreateGateway()
        {
            return new SettingsGateway();
        }
        
        public Settings GetSettingsForPlayer(string playerId)
        {
            return GetObjectBy(settings => settings.PlayerId == playerId);
        }

        public void LoadSettings(string playerId)
        {
            var playerSettings = GetSettingsForPlayer(playerId);
            UpdateObject(playerSettings);
        }
    }
}