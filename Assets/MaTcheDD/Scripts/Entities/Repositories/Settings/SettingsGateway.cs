using HamerSoft.UnityJsonPersistence;

namespace MaTcheDD.Entities.Repositories
{
    public class SettingsGateway : JsonGateway<Settings>
    {
        protected override string SubDirectory => "Entities/Settings";
    }
}