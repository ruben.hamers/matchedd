﻿using System.Collections.Generic;
using System.Linq;
using HamerSoft.Core;
using MaTcheDD.Entities.Repositories.Gateways;

namespace MaTcheDD.Entities.Repositories
{
    public class SessionRepository : Repository<Session>
    {
        protected override IGateway<Session> CreateGateway()
        {
            return new SessionGateway();
        }

        public List<Session> GetSessionsForPlayer(string playerId)
        {
            return GetObjectsBy(s => s.GetPlayerId() == playerId);
        }

        public List<Session> GetTopSessions(int amount, int tracks)
        {
            List<Session> sessions = GetObjectsBy(s => s.GetTracks() == tracks).Where(s => s.GetRounds() > 0)
                .OrderByDescending(s => s.GetRounds()).ThenByDescending(s=>s.GetRetries()).ToList();
            int range = sessions.Count > amount ? amount : sessions.Count;
            return sessions.GetRange(0, range).ToList();
        }
    }
}