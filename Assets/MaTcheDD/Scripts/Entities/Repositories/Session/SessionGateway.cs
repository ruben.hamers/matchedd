﻿using HamerSoft.UnityJsonPersistence;

namespace MaTcheDD.Entities.Repositories.Gateways
{
    public class SessionGateway : JsonGateway<Session>
    {
        protected override string SubDirectory => "Entities/Sessions";
    }
}