using HamerSoft.Core;
using Newtonsoft.Json;

namespace MaTcheDD.Entities
{
    public class Player : IIdentifiable
    {
        [JsonProperty("Name")] private string _name;
        [JsonProperty("Id")] public string Id { get; private set; }

        public Player(string name)
        {
            Id = System.Guid.NewGuid().ToString();
            _name = name;
        }

        public string GetName()
        {
            return _name;
        }

        public void Dispose()
        {
        }
    }
}