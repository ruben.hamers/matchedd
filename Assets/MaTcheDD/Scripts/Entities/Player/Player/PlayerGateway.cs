using HamerSoft.UnityJsonPersistence;

namespace MaTcheDD.Entities.Repositories.Gateways
{
    public class PlayerGateway : JsonGateway<Player>
    {
        protected override string SubDirectory => "Entities/Player";
    }
}