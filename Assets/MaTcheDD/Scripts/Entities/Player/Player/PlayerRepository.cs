﻿using HamerSoft.Core;
using MaTcheDD.Entities.Repositories.Gateways;

namespace MaTcheDD.Entities.Repositories
{
    public class PlayerRepository : Repository<Player>
    {
        protected override IGateway<Player> CreateGateway()
        {
            return new PlayerGateway();
        }

        public Player GetPlayerByName(string name)
        {
            return GetObjectBy(p => p.GetName() == name);
        }

        public Player GetPlayerById(string id)
        {
            return GetObjectBy(p => p.Id == id);
        }
    }
}