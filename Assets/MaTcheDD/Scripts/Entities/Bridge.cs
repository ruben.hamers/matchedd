using HamerSoft.Core;
using UnityEngine;

namespace MaTcheDD.Entities
{
    public class BridgeNode
    {
        public string PairView;
        public Vector3 Position;

        public BridgeNode(string pairView, Vector3 position)
        {
            PairView = pairView;
            Position = position;
        }
    }

    public class Bridge : IIdentifiable
    {
        public string Id { get; private set; }
        private readonly BridgeNode _start, _end;


        public Bridge(BridgeNode start, BridgeNode end)
        {
            Id = System.Guid.NewGuid().ToString();
            _start = start;
            _end = end;
        }

        public BridgeNode GetStart()
        {
            return _start;
        }
        
        public BridgeNode GetEnd()
        {
            return _end;
        }

        public void Dispose()
        {
        }
    }
}