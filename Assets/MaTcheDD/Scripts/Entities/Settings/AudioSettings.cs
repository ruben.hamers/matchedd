using HamerSoft.Core;
using Newtonsoft.Json;

namespace MaTcheDD.Entities
{
    public class AudioSettings : IIdentifiable
    {
        [JsonProperty("Id")] public string Id { get; private set; }
        [JsonProperty("MasterVolume")] public float MasterVolume { get; private set; }
        [JsonProperty("AmbientVolume")] public float AmbientVolume { get; private set; }
        [JsonProperty("EffectsVolume")] public float EffectsVolume { get; private set; }
        [JsonProperty("VoiceVolume")] public float VoiceVolume { get; private set; }
        [JsonProperty("WorldVolume")] public float WorldVolume { get; private set; }
        [JsonProperty("MusicVolume")] public float MusicVolume { get; private set; }


        public AudioSettings()
        {
            Id = System.Guid.NewGuid().ToString();
            MasterVolume = 1;
            AmbientVolume = 1;
            EffectsVolume = 1;
            VoiceVolume = 1;
            WorldVolume = 1;
            MusicVolume = 1;
        }

        public void SetMasterVolume(float volume)
        {
            MasterVolume = volume;
        }

        public void SetAmbientVolume(float volume)
        {
            AmbientVolume = volume;
        }

        public void SetEffectsVolume(float volume)
        {
            EffectsVolume = volume;
        }

        public void SetVoiceVolume(float volume)
        {
            VoiceVolume = volume;
        }

        public void SetWorldVolume(float volume)
        {
            WorldVolume = volume;
        }

        public void SetMusicVolume(float volume)
        {
            MusicVolume = volume;
        }

        public void Dispose()
        {
        }
    }
}