using HamerSoft.Core;
using Newtonsoft.Json;

namespace MaTcheDD.Entities
{
    public class Settings : IIdentifiable
    {
        [JsonProperty("Id")] public string Id { get; private set; }
        [JsonProperty("PlayerId")] public string PlayerId { get; private set; }
        [JsonProperty("Audio")] public AudioSettings Audio { get; private set; }


        public Settings(string playerId)
        {
            Id = System.Guid.NewGuid().ToString();
            PlayerId = playerId;
            Audio = new AudioSettings();
        }

        public void Dispose()
        {
            Audio.Dispose();
        }
    }
}