using HamerSoft.Core;
using Newtonsoft.Json;

namespace MaTcheDD.Entities
{
    public class Session : IIdentifiable
    {
        [JsonProperty("Id")] public string Id { get; private set; }
        [JsonProperty("PlayerId")] private readonly string _playerId;
        [JsonProperty("Rounds")] private int _rounds;
        [JsonProperty("Retries")] private int _retries;
        [JsonProperty("Tracks")] private int _tracks;
        [JsonProperty] private bool _finished;

        public Session(string playerId, int tracks)
        {
            Id = System.Guid.NewGuid().ToString();
            _playerId = playerId;
            _rounds = 0;
            _retries = 0;
            _tracks = tracks;
        }

        public string GetPlayerId()
        {
            return _playerId;
        }

        public int GetRounds()
        {
            return _rounds;
        }

        public void AddRound()
        {
            if (_finished)
                return;
            _rounds++;
        }

        public void Dispose()
        {
        }

        public void SetFinished()
        {
            _finished = true;
        }

        public bool GetFinished()
        {
            return _finished;
        }

        public int GetTracks()
        {
            return _tracks;
        }

        public void AddRetry()
        {
            if (_finished)
                return;
            _retries++;
        }

        public int GetRetries()
        {
            return _retries;
        }
    }
}