using HamerSoft.Audio;
using HamerSoft.Core;
using HamerSoft.Core.Singletons;
using HamerSoft.UnityAds;
using HamerSoft.UnityMvc;
using MaTcheDD.Entities;
using MaTcheDD.Entities.Repositories;
using MaTcheDD.Views;
using MaTcheDD.Views.Menu;
using UnityEngine.Advertisements;
using UnityEngine.SceneManagement;

namespace MaTcheDD.Controllers
{
    public class SettingsController : BaseController<SettingsView>, IRepositoryListener<Settings>
    {
        private SettingsRepository _settingsRepository;
        private AudioPlugin _audioPlugin;
        private Settings _settings;

        public override void SetMain(Main main)
        {
            base.SetMain(main);
            GetRepos(main);
        }

        private void GetRepos(Main main)
        {
            if (!main)
                return;

            _settingsRepository = main.GetRepository<SettingsRepository>();
            _settingsRepository.AddListener(this);
            _audioPlugin = main.GetPlugin<AudioPlugin>();
        }

        public override void Dispose()
        {
            base.Dispose();

            _settingsRepository?.RemoveListener(this);
        }

        protected override void AssignViewEvents(SettingsView view)
        {
            view.MasterChanged += MasterChanged;
            view.AmbientChanged += AmbientChanged;
            view.EffectsChanged += EffectsChanged;
            view.VoiceChanged += VoiceChanged;
            view.WorldChanged += WorldChanged;
            view.MusicChanged += MusicChanged;
            view.BackPressed += ViewOnBackPressed;
        }

        protected override void UnAssignViewEvents(SettingsView view)
        {
            view.MasterChanged -= MasterChanged;
            view.AmbientChanged -= AmbientChanged;
            view.EffectsChanged -= EffectsChanged;
            view.VoiceChanged -= VoiceChanged;
            view.WorldChanged -= WorldChanged;
            view.MusicChanged -= MusicChanged;
        }

        private void PersistSettings(Settings settings)
        {
            _settingsRepository.UpdateObject(settings);
            _settingsRepository.Flush();
        }

        private void GetSettings()
        {
            if (_settings == null)
                _settings = _settingsRepository.GetSettingsForPlayer(Main.GetPlugin<MatchedPlugin>().Player.Id);
        }

        private void MasterChanged(View view, object[] outputArguments)
        {
            float volume = outputArguments.GetArgument<float>(0);
            SetVolume(AudioGroup.Master, volume);
        }

        private void MusicChanged(View view, object[] outputArguments)
        {
            float volume = outputArguments.GetArgument<float>(0);
            SetVolume(AudioGroup.Music, volume);
        }

        private void WorldChanged(View view, object[] outputArguments)
        {
            float volume = outputArguments.GetArgument<float>(0);
            SetVolume(AudioGroup.World, volume);
        }

        private void VoiceChanged(View view, object[] outputArguments)
        {
            float volume = outputArguments.GetArgument<float>(0);
            SetVolume(AudioGroup.Voice, volume);
        }

        private void EffectsChanged(View view, object[] outputArguments)
        {
            float volume = outputArguments.GetArgument<float>(0);
            SetVolume(AudioGroup.Effects, volume);
        }

        private void AmbientChanged(View view, object[] outputArguments)
        {
            float volume = outputArguments.GetArgument<float>(0);
            SetVolume(AudioGroup.Ambient, volume);
        }

        private void SetVolume(AudioGroup group, float volume)
        {
            GetSettings();
            switch (group)
            {
                case AudioGroup.Ambient:
                    _settings.Audio.SetAmbientVolume(volume);
                    break;
                case AudioGroup.Effects:
                    _settings.Audio.SetEffectsVolume(volume);
                    break;
                case AudioGroup.Master:
                    _settings.Audio.SetMasterVolume(volume);
                    break;
                case AudioGroup.Music:
                    _settings.Audio.SetMusicVolume(volume);
                    break;
                case AudioGroup.Voice:
                    _settings.Audio.SetVoiceVolume(volume);
                    break;
                case AudioGroup.World:
                    _settings.Audio.SetWorldVolume(volume);
                    break;
            }

            _audioPlugin.SetVolume(group, volume);
            PersistSettings(_settings);
        }

        private void ViewOnBackPressed(View view, object[] outputArguments)
        {
            DestroyView(CastTo(view));
            SpawnCustomView<MenuView>(Canvas.GetInstance().transform);
        }

        private void SetAllVolumes()
        {
            _audioPlugin.SetVolume(AudioGroup.Master, _settings.Audio.MasterVolume);
            _audioPlugin.SetVolume(AudioGroup.Ambient, _settings.Audio.AmbientVolume);
            _audioPlugin.SetVolume(AudioGroup.Effects, _settings.Audio.EffectsVolume);
            _audioPlugin.SetVolume(AudioGroup.Voice, _settings.Audio.VoiceVolume);
            _audioPlugin.SetVolume(AudioGroup.World, _settings.Audio.WorldVolume);
            _audioPlugin.SetVolume(AudioGroup.Music, _settings.Audio.MusicVolume);
        }

        public void AddedObject(Settings added)
        {
            _settings = added;
            SetAllVolumes();
        }

        public void RemovedObject(Settings removed)
        {
        }

        public void UpdatedObject(Settings updated)
        {
            _settings = updated;
            SetAllVolumes();
        }
    }
}