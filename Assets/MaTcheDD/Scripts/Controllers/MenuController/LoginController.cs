﻿using HamerSoft.Audio;
using HamerSoft.UnityAds;
using HamerSoft.Core;
using HamerSoft.UnityMvc;
using MaTcheDD.Entities;
using MaTcheDD.UseCases.Player;
using MaTcheDD.Views;
using PlayFab.Usecases.Login;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.SceneManagement;
using Canvas = HamerSoft.Core.Singletons.Canvas;

namespace MaTcheDD.Controllers
{
    public class LoginController : BaseController<LoginView>
    {
        private readonly string[] _choices = new[] {"Facebook Login", "Remote Login", "Local Login"};
        private BannerAd _banner;
        private AudioPlayer _audioPlayer;
        private string _currentLoginChoice;

        public override void SetMain(Main main)
        {
            base.SetMain(main);
            Screen.SetResolution(1920, 1080, FullScreenMode.ExclusiveFullScreen);
            SceneManager.sceneUnloaded += SceneManagerOnSceneUnloaded;
        }

        public override void Dispose()
        {
            base.Dispose();
            SceneManager.sceneUnloaded -= SceneManagerOnSceneUnloaded;
        }

        private void SceneManagerOnSceneUnloaded(Scene scene)
        {
            if (scene.name != "Menu")
                return;
            _banner?.Hide();
        }

        protected override void AssignViewEvents(LoginView view)
        {
            view.LocalLoggedin += ViewOnLocalLoggedIn;
            view.FaceBookLoggedin += ViewOnFaceBookLoggedIn;
            view.loginChoiceChanged += ViewOnLoginChoiceChanged;
            view.RemoteLoggedin += ViewOnRemoteLoggedin;
        }

        protected override void UnAssignViewEvents(LoginView view)
        {
            view.LocalLoggedin -= ViewOnLocalLoggedIn;
            view.FaceBookLoggedin -= ViewOnFaceBookLoggedIn;
            view.loginChoiceChanged -= ViewOnLoginChoiceChanged;
            view.RemoteLoggedin -= ViewOnRemoteLoggedin;
            _currentLoginChoice = null;
        }

        protected override void OnStarted(View view, object[] outputArguments)
        {
            base.OnStarted(view, outputArguments);
            AudioClip ac = outputArguments.GetArgument<AudioClip>(0);
            if (ac)
                _audioPlayer = Main.GetPlugin<AudioPlugin>().PlayAudioLooping(ac, 1, AudioGroup.Music, false);
            //   _banner = Main.GetPlugin<UnityAdsPlugin>().ShowBannerAd("BannerL", BannerPosition.BOTTOM_CENTER);

            CastTo(view).SetLoginChoices(_choices);
            if (Main.GetPlugin<MatchedPlugin>().Player == null)
                return;
            DestroyView(CastTo(view));
            SpawnCustomView<MenuView>(Canvas.GetInstance().transform);
        }

        protected void ViewOnLocalLoggedIn(View view, object[] outputArguments)
        {
            string username = outputArguments.GetArgument<string>(0);
            if (!string.IsNullOrEmpty(username))
                new LoginPlayer(Main, username).Execute(player => { ShowMenu(view, player); });
        }

        private void ViewOnFaceBookLoggedIn(View view, object[] outputArguments)
        {
            var loginView = CastTo(view);
            var username = outputArguments.GetArgument<string>(0);
            var password = outputArguments.GetArgument<string>(1);

            new FacebookLogin(Main, username, password).Execute((player, message) =>
            {
                if (player == null)
                    loginView.SetFeedbackText(message);
                else
                    ShowMenu(view, player);
            });
        }

        private void ViewOnRemoteLoggedin(View view, object[] outputArguments)
        {
            var loginView = CastTo(view);
            var username = outputArguments.GetArgument<string>(0);
            var password = outputArguments.GetArgument<string>(1);

            new LoginPlayfabUser(Main, username, password).Execute((player, message) =>
            {
                if (player == null)
                    loginView.SetFeedbackText(message);
                else
                    ShowMenu(view, player);
            });
        }

        private void ShowMenu(View view, Player player)
        {
            Main.GetPlugin<MatchedPlugin>().SetPlayer(player);
            DestroyView(CastTo(view));
            SpawnCustomView<MenuView>(Canvas.GetInstance().transform);
        }

        private void ViewOnLoginChoiceChanged(View view, object[] outputArguments)
        {
            _currentLoginChoice = outputArguments.GetArgument<string>(0);
            var loginView = CastTo(view);
            if (_currentLoginChoice.Equals(_choices[0]))
                loginView.ShowViewForFBLogin();
            else if (_currentLoginChoice.Equals(_choices[1]))
                loginView.ShowViewForRemoteLogin();
            else if (_currentLoginChoice.Equals(_choices[2]))
                loginView.ShowViewForLocalLogin();
        }
    }
}