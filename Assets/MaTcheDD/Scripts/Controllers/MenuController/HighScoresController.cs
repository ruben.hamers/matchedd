using System;
using System.Collections.Generic;
using System.Linq;
using HamerSoft.Core;
using HamerSoft.Core.Singletons;
using HamerSoft.PlayFab;
using HamerSoft.UnityAds;
using HamerSoft.UnityMvc;
using MaTcheDD.Entities;
using MaTcheDD.Entities.Repositories;
using MaTcheDD.Views;
using MaTcheDD.Views.Menu;
using UnityEngine.Advertisements;
using UnityEngine.SceneManagement;

namespace MaTcheDD.Controllers
{
    public class HighScoresController : BaseController<HighScoresView>
    {
        private PlayFabPlugin _playFabPlugin;
        private PlayerRepository _playerRepo;
        private BannerAd _banner;

        public override void SetMain(Main main)
        {
            base.SetMain(main);
            _playFabPlugin = main.GetPlugin<PlayFabPlugin>();
            _playerRepo = Main.GetRepository<PlayerRepository>();
            SceneManager.sceneUnloaded += SceneManagerOnSceneUnloaded;
        }

        private void SceneManagerOnSceneUnloaded(Scene scene)
        {
            if (scene.name != "Menu")
                return;
            _banner?.Hide();
        }

        protected override void AssignViewEvents(HighScoresView view)
        {
            view.NormalPressed += ViewOnNormalPressed;
            view.IntermediatePressed += ViewOnIntermediatePressed;
            view.HardPressed += ViewOnHardPressed;
            view.ExtraHardPressed += ViewOnExtraHardPressed;
            view.BackPressed += ViewOnBackPressed;
        }

        protected override void UnAssignViewEvents(HighScoresView view)
        {
            view.NormalPressed -= ViewOnNormalPressed;
            view.IntermediatePressed -= ViewOnIntermediatePressed;
            view.HardPressed -= ViewOnHardPressed;
            view.ExtraHardPressed -= ViewOnExtraHardPressed;
            view.BackPressed -= ViewOnBackPressed;
        }

        protected override void OnStarted(View view, object[] outputArguments)
        {
            base.OnStarted(view, outputArguments);
            Main.GetPlugin<UnityAdsPlugin>().HideBannerAd("BannerL");
            HighScoresView hsv = CastTo(view);
            GetHighScores(3, hsv.SetScores);
        }

        private void ViewOnBackPressed(View view, object[] outputArguments)
        {
            DestroyView(CastTo(view));
     //       _banner = Main.GetPlugin<UnityAdsPlugin>().ShowBannerAd("BannerL", BannerPosition.BOTTOM_CENTER);
            SpawnCustomView<MenuView>(Canvas.GetInstance().transform);
        }

        private void ViewOnExtraHardPressed(View view, object[] outputArguments)
        {
            HighScoresView hsv = CastTo(view);
            GetHighScores(6, hsv.SetScores);
        }

        private void GetHighScores(int tracks, Action<List<HighScore>> callback)
        {
            if (_playFabPlugin.ActivePlayer == null)
                GetLocalHighScores(tracks, callback);
            else
                GetRemoteHighScores(tracks, callback);
        }

        private void GetLocalHighScores(int tracks, Action<List<HighScore>> callback)
        {
            var topSessions = Main.GetRepository<SessionRepository>().GetTopSessions(10, tracks);
            List<HighScore> highScores = new List<HighScore>();
            foreach (Session session in topSessions)
            {
                if (_playerRepo.GetPlayerById(session.GetPlayerId()) != null)
                    highScores.Add(new HighScore(_playerRepo.GetPlayerById(session.GetPlayerId()).GetName(),
                        session.GetRounds() - session.GetRetries()));
            }

            callback?.Invoke(highScores);
        }

        private void GetRemoteHighScores(int tracks, Action<List<HighScore>> callback)
        {
            void FormatHighScores(List<Tuple<string, int>> scores)
            {
                if (scores != null)
                    callback?.Invoke(scores.Select(s => new HighScore(s.Item1, s.Item2))
                        .OrderByDescending(hs => hs.Rounds).ToList());
                else
                    callback?.Invoke(new List<HighScore>());
            }

            new GetLeaderBoard(tracks).Execute(FormatHighScores);
        }

        private void ViewOnHardPressed(View view, object[] outputArguments)
        {
            HighScoresView hsv = CastTo(view);
            GetHighScores(5, hsv.SetScores);
        }

        private void ViewOnIntermediatePressed(View view, object[] outputArguments)
        {
            HighScoresView hsv = CastTo(view);
            GetHighScores(4, hsv.SetScores);
        }

        private void ViewOnNormalPressed(View view, object[] outputArguments)
        {
            HighScoresView hsv = CastTo(view);
            GetHighScores(3, hsv.SetScores);
        }
    }
}