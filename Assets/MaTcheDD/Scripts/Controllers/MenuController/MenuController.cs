using DefaultNamespace;
using HamerSoft.UnityMvc;
using MaTcheDD.Entities.Repositories;
using MaTcheDD.UseCases.Menu;
using MaTcheDD.Views;
using MaTcheDD.Views.Menu;
using UnityEngine;
using Canvas = HamerSoft.Core.Singletons.Canvas;

namespace MaTcheDD.Controllers
{
    public class MenuController : BaseController<MenuView>
    {
        protected override void AssignViewEvents(MenuView view)
        {
            view.Quited += ViewOnQuited;
            view.HighScored += ViewOnHighScored;
            view.GameStarted += ViewOnGameStarted;
            view.Settinged += ViewOnSettinged;
        }

        protected override void UnAssignViewEvents(MenuView view)
        {
            view.Quited -= ViewOnQuited;
            view.HighScored -= ViewOnHighScored;
            view.GameStarted -= ViewOnGameStarted;
            view.Settinged -= ViewOnSettinged;
        }

        protected virtual void ViewOnGameStarted(View view, object[] outputArguments)
        {
            DestroyView(CastTo(view));
            SpawnCustomView<StartGameView>(Canvas.GetInstance().transform, null, new[] {3, 4, 5, 6});
        }

        protected virtual void ViewOnHighScored(View view, object[] outputArguments)
        {
            DestroyView(CastTo(view));
            SpawnCustomView<HighScoresView>(Canvas.GetInstance().transform);
        }

        protected virtual void ViewOnQuited(View view, object[] outputArguments)
        {
            ChoiceView SpawnView(object[] args)
            {
                return SpawnCustomView<ChoiceView>(Canvas.GetInstance().transform, null, args);
            }

            new QuitGame(SpawnView).Execute(() => { Debug.Log("quitted?"); });
        }

        private void ViewOnSettinged(View view, object[] outputArguments)
        {
            DestroyView(CastTo(view));
            var settings = Main.GetRepository<SettingsRepository>()
                .GetSettingsForPlayer(Main.GetPlugin<MatchedPlugin>().Player.Id);
            SpawnCustomView<SettingsView>(Canvas.GetInstance().transform, null,
                settings == null
                    ? null
                    : new object[6]
                    {
                        settings.Audio.MasterVolume, settings.Audio.AmbientVolume, settings.Audio.EffectsVolume,
                        settings.Audio.VoiceVolume, settings.Audio.WorldVolume, settings.Audio.MusicVolume
                    });
        }
    }
}