using HamerSoft.Core;
using HamerSoft.UnityAds;
using HamerSoft.UnityMvc;
using HamerSoft.UnityMvc.Views;
using MaTcheDD.Views;
using MaTcheDD.Views.Menu;
using UnityEngine;
using UnityEngine.SceneManagement;
using Canvas = HamerSoft.Core.Singletons.Canvas;

namespace MaTcheDD.Controllers
{
    public class StartGameController : BaseController<StartGameView>
    {
        private AsyncOperation _sceneLoadOperation;

        protected override void AssignViewEvents(StartGameView view)
        {
            view.GameStarted += ViewOnGameStarted;
            view.BackPressed += ViewOnBackPressed;
        }

        protected override void UnAssignViewEvents(StartGameView view)
        {
            view.GameStarted -= ViewOnGameStarted;
            view.BackPressed -= ViewOnBackPressed;
        }

        protected override void OnDestroyed(View view, object[] outputArguments)
        {
            base.OnDestroyed(view, outputArguments);
            var v = CastTo(view);
            v.GameStarted -= ViewOnGameStarted;
            v.BackPressed -= ViewOnBackPressed;
        }

        private void ViewOnBackPressed(View view, object[] outputArguments)
        {
            DestroyView(CastTo(view));
            SpawnCustomView<MenuView>(Canvas.GetInstance().transform);
        }

        private void ViewOnGameStarted(View view, object[] outputArguments)
        {
            string arg = outputArguments.GetArgument<string>(0);
            int tracks = System.Convert.ToInt32(arg);
            Main.GetPlugin<MatchedPlugin>().SetTracks(tracks);
            LoadGame();
        }

        protected virtual void LoadGame()
        {
            SpawnCustomView<LoadingView>(Canvas.GetInstance().transform);
            Main.GetPlugin<UnityAdsPlugin>().HideBannerAd("BannerL");
            IAd ad = Main.GetPlugin<UnityAdsPlugin>().ShowNonRewardedAd(true);
            _sceneLoadOperation = SceneManager.LoadSceneAsync("Game", LoadSceneMode.Single);
            if (ad == null)
                _sceneLoadOperation.allowSceneActivation = true;
            else
            {
                ad.Failed += AdOnDone;
                ad.Finished += AdOnDone;
                ad.Skipped += AdOnDone;
                _sceneLoadOperation.allowSceneActivation = false;
            }
        }

        private void AdOnDone(IAd ad)
        {
            ad.Failed -= AdOnDone;
            ad.Finished -= AdOnDone;
            ad.Skipped -= AdOnDone;
            _sceneLoadOperation.allowSceneActivation = true;
        }
    }
}