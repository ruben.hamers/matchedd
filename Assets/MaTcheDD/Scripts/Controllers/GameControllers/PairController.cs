using System.Collections;
using HamerSoft.Core;
using HamerSoft.UnityMvc;
using MaTcheDD.Lines;
using MaTcheDD.Views;
using UnityEngine;

namespace MaTcheDD.Controllers
{
    public class PairController : BaseController<PairView>
    {
        protected override void AssignViewEvents(PairView view)
        {
        }

        protected override void UnAssignViewEvents(PairView view)
        {
            
        }

        protected override void OnStarted(View view, object[] outputArguments)
        {
            base.OnStarted(view, outputArguments);
            var staticLine = outputArguments.GetArgument<StaticLine>(0);
            if (!staticLine)
                return;
            PairView pv = CastTo(view);

            IEnumerator SetupStaticLine()
            {
                yield return new WaitForEndOfFrame();
                Vector3 columnHeight = -pv.transform.localPosition;
                if (Main != null)
                {
                    MatchedPlugin mp = Main.GetPlugin<MatchedPlugin>();
                    staticLine.Init(mp.GetLineThickness, mp.GetLineColor);
                    staticLine.Set(Vector3.zero, new Vector3(0, columnHeight.y - pv.GetBase().GetComponent<RectTransform>().rect.height, 0));
                }
            }

            pv.StartCoroutine(SetupStaticLine());
        }
    }
}