using System.Collections.Generic;
using HamerSoft.Audio;
using HamerSoft.Core;
using HamerSoft.Core.Services;
using HamerSoft.UnityAds;
using HamerSoft.UnityMvc;
using HamerSoft.UnityMvc.Views;
using MaTcheDD.Entities;
using MaTcheDD.UseCases;
using MaTcheDD.UseCases.Menu;
using MaTcheDD.Views;
using UnityEngine;
using UnityEngine.SceneManagement;
using Canvas = HamerSoft.Core.Singletons.Canvas;

namespace MaTcheDD.Controllers
{
    public class HeaderController : BaseController<HeaderView>
    {
        private SlideHeads _activeSlideHeads;
        private AsyncOperation _sceneLoadOperation;
        private AudioClip _goClip, _successClip, _failClip, _switchClip;

        protected override void AssignViewEvents(HeaderView view)
        {
            view.Goed += GoSlide;
            view.BackToMenuPressed += ViewOnBackToMenuPressed;
        }

        protected override void UnAssignViewEvents(HeaderView view)
        {
            view.Goed -= GoSlide;
            view.BackToMenuPressed -= ViewOnBackToMenuPressed;
        }

        protected override void OnStarted(View view, object[] outputArguments)
        {
            base.OnStarted(view, outputArguments);
            _goClip = outputArguments.GetArgument<AudioClip>(0);
            _successClip = outputArguments.GetArgument<AudioClip>(1);
            _failClip = outputArguments.GetArgument<AudioClip>(2);
            _switchClip = outputArguments.GetArgument<AudioClip>(3);
        }

        protected virtual void GoSlide(View view, object[] outputArguments)
        {
            if (_activeSlideHeads != null)
                return;
            MatchedPlugin mp = Main.GetPlugin<MatchedPlugin>();
            HeaderView header = CastTo(view);
            DestroyDrawingView();
            SlideSounds ss = new SlideSounds()
            {
                Success = _successClip, Failed = _failClip, Switch = _switchClip, Plugin = Main.GetPlugin<AudioPlugin>()
            };
            _activeSlideHeads = new SlideHeads(mp.NavigationSpeed, FailWarningCallback,ss);
            _activeSlideHeads.Execute(success =>
            {
                Debug.Log($"Sliding heads {(success ? "Succeeded" : "Failed")}!");
                Main.GetPlugin<AudioPlugin>().PlayAudio(_goClip, 1, AudioGroup.Effects, null);
                if (!success && _activeSlideHeads.IsImmortal)
                {
                    _activeSlideHeads.ResetHeadsPreSlide();
                    _activeSlideHeads.Dispose();
                    _activeSlideHeads = null;
                    SpawnDrawingView();
                }
                else
                    new UpdateSession(Main, success).Execute(sessionSucceed =>
                    {
                        if (sessionSucceed)
                            new StartNewRound(header, mp).Execute(() =>
                            {
                                Debug.Log("New round is started!");
                                _activeSlideHeads.Dispose();
                                _activeSlideHeads = null;
                                SpawnDrawingView();
                            });
                        else
                            ShowChoiceDialog(header);
                    });
            });
        }

        private void SpawnDrawingView()
        {
            SpawnCustomView<DrawingView>(Canvas.GetInstance().GetComponentInChildren<DrawingLayer>()
                .transform);
        }

        private static void DestroyDrawingView()
        {
            var drawingView = Canvas.GetInstance().GetComponentInChildren<DrawingView>();
            if (drawingView)
                drawingView.ForceDestroyObject();
        }

        private void FailWarningCallback()
        {
            RewardedAdChoice adChoice = new RewardedAdChoice(Main, SpawnChoiceView,
                "You are about to lose the game! Do not worry, you can try again when you watch an advertisement. Would you like to watch it, or prefer not to?.",
                () =>
                {
                    var s = Main.GetPlugin<MatchedPlugin>().Session;
                    LogRetryEvent(s);
                    LogFirstRetryEvent(s);
                    s.AddRetry();
                    _activeSlideHeads.SetImmortal();
                });
            adChoice.Execute(() =>
            {
                adChoice.Dispose();
                _activeSlideHeads.Slide();
            });
        }

        private static void LogRetryEvent(Session s)
        {
            ServiceProvider.GetService<AnalyticsService>().LogEvent("retry_requested",
                new Dictionary<string, object>
                {
                    {"Rounds", s.GetRounds()},
                    {"Tracks", s.GetTracks()},
                    {"Retries", s.GetRetries()}
                });
        }

        private static void LogFirstRetryEvent(Session s)
        {
            if (s.GetRetries() == 0)
                ServiceProvider.GetService<AnalyticsService>().LogEvent("first_retry_requested",
                    new Dictionary<string, object>
                    {
                        {"Rounds", s.GetRounds()},
                        {"Tracks", s.GetTracks()}
                    });
        }

        ChoiceView SpawnChoiceView(object[] args)
        {
            return SpawnCustomView<ChoiceView>(Canvas.GetInstance().transform, null, args);
        }

        private void ShowChoiceDialog(HeaderView header)
        {
            new LostGame(SpawnChoiceView).Execute(tryAgain =>
            {
                if (tryAgain)
                    new RestartGame(Main, header).Execute(() =>
                    {
                        Debug.Log("Game restarted!");
                        _activeSlideHeads = null;
                        SpawnDrawingView();
                    });
                else
                    LoadMenu();
            });
        }

        private void ViewOnBackToMenuPressed(View view, object[] outputArguments)
        {
            ChoiceView SpawnView(object[] args)
            {
                return SpawnCustomView<ChoiceView>(Canvas.GetInstance().transform, null, args);
            }

            new BackToMenu(SpawnView).Execute(loadMenu =>
            {
                if (loadMenu)
                    LoadMenu();
            });
        }

        protected virtual void LoadMenu()
        {
            SpawnCustomView<LoadingView>(Canvas.GetInstance().transform);
            IAd ad = Main.GetPlugin<UnityAdsPlugin>().ShowNonRewardedAd(true);
            _sceneLoadOperation = SceneManager.LoadSceneAsync("Menu", LoadSceneMode.Single);
            if (ad == null)
                _sceneLoadOperation.allowSceneActivation = true;
            else
            {
                ad.Failed += AdOnDone;
                ad.Finished += AdOnDone;
                ad.Skipped += AdOnDone;
                _sceneLoadOperation.allowSceneActivation = false;
            }
        }

        private void AdOnDone(IAd ad)
        {
            ad.Failed -= AdOnDone;
            ad.Finished -= AdOnDone;
            ad.Skipped -= AdOnDone;
            _sceneLoadOperation.allowSceneActivation = true;
        }
    }
}