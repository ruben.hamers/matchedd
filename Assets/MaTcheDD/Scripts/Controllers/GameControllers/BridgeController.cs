using System.Linq;
using HamerSoft.Core;
using HamerSoft.Core.Singletons;
using HamerSoft.UnityMvc;
using MaTcheDD.Entities;
using MaTcheDD.Entities.Repositories.Bridge;
using MaTcheDD.Views;

namespace MaTcheDD.Controllers
{
    /// <summary>
    /// this controller implements a repository listener for Bridges so it is notified when a bridge model is added to the repo and it will add the view
    /// When a player loses the game, and decides to go for another try, all bridges are deleted.
    /// as for now, bridges cannot be updated, so the method does not implement any logic
    /// </summary>
    public class BridgeController : BaseController<BridgeView>, IRepositoryListener<Bridge>
    {
        private MatchedPlugin _matchedPlugin;

        public override void SetMain(Main main)
        {
            base.SetMain(main);
            _matchedPlugin = main.GetPlugin<MatchedPlugin>();
            main.GetRepository<BridgeRepository>().AddListener(this);
        }

        public override void Dispose()
        {
            Main.GetRepository<BridgeRepository>()?.RemoveListener(this);
            base.Dispose();
        }

        protected override void AssignViewEvents(BridgeView view)
        {
        }

        protected override void UnAssignViewEvents(BridgeView view)
        {
            
        }

        public virtual void AddedObject(Bridge added)
        {
            if (!Canvas.GetInstance())
                return;
            BridgeView bridge = SpawnView(Canvas.GetInstance().GetComponentInChildren<BridgeLayer>().transform);
            bridge.Init(added.Id, added.GetStart(), added.GetEnd(), _matchedPlugin.GetLineThickness,
                _matchedPlugin.GetLineColor);

            foreach (PairView pairView in Canvas.GetInstance().GetComponentsInChildren<PairView>())
                if (pairView.Id == added.GetStart().PairView || pairView.Id == added.GetEnd().PairView)
                    pairView.AddBridge(bridge);
        }

        public virtual void RemovedObject(Bridge removed)
        {
            if (!Canvas.GetInstance())
                return;
            foreach (PairView pairView in Canvas.GetInstance().GetComponentsInChildren<PairView>())
                if (pairView.Id == removed.GetStart().PairView || pairView.Id == removed.GetEnd().PairView)
                    pairView.RemoveBridge(removed.Id);
            DestroyView(Views.FirstOrDefault(v => v.Value.GetBridgeId() == removed.Id).Value);
        }

        public virtual void UpdatedObject(Bridge updated)
        {
            //stub
        }
    }
}