using System.Collections.Generic;
using System.Linq;
using HamerSoft.UnityMvc;
using MaTcheDD.Views;
using UnityEngine;

namespace MaTcheDD.Controllers
{
    public class ColumnController : BaseController<ColumnView>
    {
        protected override void OnStarted(View view, object[] outputArguments)
        {
            base.OnStarted(view, outputArguments);
            if (view != null)
                SpawnCustomView<PairView>(((Component) view).transform, views => (PairView) views[CastTo(view).transform.GetSiblingIndex()] );
        }

        protected override void AssignViewEvents(ColumnView view)
        {
            
        }

        protected override void UnAssignViewEvents(ColumnView view)
        {
            
        }
    }
}