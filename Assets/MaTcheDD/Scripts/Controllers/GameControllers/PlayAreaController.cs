using HamerSoft.Core;
using HamerSoft.UnityMvc;
using MaTcheDD.UseCases;
using MaTcheDD.Views;
using UnityEngine;
using UnityEngine.UI;

namespace MaTcheDD.Controllers
{
    public class PlayAreaController : BaseController<PlayAreaView>
    {
        protected override void AssignViewEvents(PlayAreaView view)
        {
        }

        protected override void UnAssignViewEvents(PlayAreaView view)
        {
        }

        protected override void OnStarted(View view, object[] outputArguments)
        {
            base.OnStarted(view, outputArguments);
            HorizontalLayoutGroup group = outputArguments.GetArgument<HorizontalLayoutGroup>(0);
            AudioClip ac = outputArguments.GetArgument<AudioClip>(1);
            if (!group)
                return;

            ColumnView SpawnColumnView(Transform parent)
            {
                return SpawnCustomView<ColumnView>(parent);
            }

            new GameStartupUseCase(Main, CastTo(view), group, SpawnColumnView, ac).Execute(() =>
            {
                SpawnCustomView<DrawingView>((view as Component).GetComponentInChildren<DrawingLayer>().transform);
            });
        }
    }
}