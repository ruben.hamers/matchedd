using HamerSoft.Core;
using HamerSoft.UnityMvc;
using MaTcheDD.Entities;
using MaTcheDD.Lines;
using MaTcheDD.UseCases.Lines.Bridge;
using MaTcheDD.UseCases.Lines.Drawing;
using MaTcheDD.Views;
using UnityEngine;
using Canvas = HamerSoft.Core.Singletons.Canvas;

namespace MaTcheDD.Controllers
{
    public class DrawingController : BaseController<DrawingView>
    {
        private MatchedPlugin _matchedPlugin;

        public override void SetMain(Main main)
        {
            base.SetMain(main);
            _matchedPlugin = main.GetPlugin<MatchedPlugin>();
        }

        protected override void AssignViewEvents(DrawingView view)
        {
            view.Dragged += PointerDragged;
            view.PointerDown += PointerDown;
            view.PointerUp += PointerUp;
        }

        protected override void UnAssignViewEvents(DrawingView view)
        {
            view.Dragged -= PointerDragged;
            view.PointerDown -= PointerDown;
            view.PointerUp -= PointerUp;
        }

        protected override void OnStarted(View view, object[] outputArguments)
        {
            base.OnStarted(view, outputArguments);
            var plugin = Main.GetPlugin<MatchedPlugin>();
            CastTo(view).Init(plugin.GetLineThickness, plugin.GetLineColor);
        }

        protected override void OnDestroyed(View view, object[] args)
        {
            base.OnDestroyed(view, args);
            DrawingView casted = CastTo(view);
            casted.Dragged -= PointerDragged;
            casted.PointerDown -= PointerDown;
            casted.PointerUp -= PointerUp;
        }

        protected void PointerDown(View view, object[] outputArguments)
        {
            DynamicLine dynamicLine = outputArguments.GetArgument<DynamicLine>(0);
            Vector2 currentPosition = outputArguments.GetArgument<Vector2>(1);

            dynamicLine.PushPosition();
            dynamicLine.EditPosition(currentPosition);
        }

        protected void PointerUp(View view, object[] outputArguments)
        {
            DrawingView drawingView = CastTo(view);
            DynamicLine dynamicLine = outputArguments.GetArgument<DynamicLine>(0);

            new ValidateDynamicLine(Main, dynamicLine, Canvas.GetInstance().GetComponentsInChildren<PairView>())
                .Execute(
                    (start, end) =>
                    {
                        new AddBridge(Main, start, end).Execute(() =>
                        {
                            DestroyView(drawingView);
                            SpawnView(Canvas.GetInstance().GetComponentInChildren<DrawingLayer>().transform);
                        });
                    });
        }

        protected void PointerDragged(View view, object[] outputArguments)
        {
            DynamicLine dynamicLine = outputArguments.GetArgument<DynamicLine>(0);
            Vector2 previousPosition = outputArguments.GetArgument<Vector2>(1);
            Vector2 currentPosition = outputArguments.GetArgument<Vector2>(2);

            if (Vector3.Distance(previousPosition, currentPosition) > _matchedPlugin.GetAddDistance())
                dynamicLine.PushPosition();

            dynamicLine.EditPosition(currentPosition);
        }
    }
}