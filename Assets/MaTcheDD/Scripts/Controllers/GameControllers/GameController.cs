using HamerSoft.UnityMvc;
using MaTcheDD.Views;
using UnityEngine;

namespace MaTcheDD.Controllers
{
    public class GameController : BaseController<GameView>
    {
        private MatchedPlugin _matchedPlugin;
        
        protected override void AssignViewEvents(GameView view)
        {
        }

        protected override void UnAssignViewEvents(GameView view)
        {
            
        }
    }
}