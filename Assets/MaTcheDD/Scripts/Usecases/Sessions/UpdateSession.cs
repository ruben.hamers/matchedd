using System;
using System.Collections.Generic;
using HamerSoft.Core;
using HamerSoft.Core.Services;
using HamerSoft.Core.UseCases;
using HamerSoft.PlayFab;
using MaTcheDD.Entities.Repositories;
using UnityEngine;

namespace MaTcheDD.UseCases
{
    public class UpdateSession : UseCase<bool>
    {
        private readonly Main _main;
        private readonly bool _succeeded;
        private readonly SessionRepository _repository;
        private readonly MatchedPlugin _plugin;

        public UpdateSession(Main main, bool succeeded)
        {
            _main = main;
            _plugin = _main.GetPlugin<MatchedPlugin>();
            _repository = _main.GetRepository<SessionRepository>();
            _succeeded = succeeded;
        }

        public override void Execute(Action<bool> callback)
        {
            base.Execute(callback);
            if (_succeeded)
                StartNewRound();
            else
                FailSession();
        }

        private void FailSession()
        {
            _plugin.Session.SetFinished();
            ServiceProvider.GetService<AnalyticsService>().LogEvent("session_failed",
                new Dictionary<string, object>
                {
                    {"Rounds", _plugin.Session.GetRounds()},
                    {"Tracks", _plugin.Session.GetTracks()},
                    {"Retries", _plugin.Session.GetRetries()}
                });
            _repository.UpdateObject(_plugin.Session);
            _repository.Flush();

            if (_main.GetPlugin<PlayFabPlugin>().ActivePlayer != null)
                new AddHighScore(_main, _plugin.Session).Execute(Debug.Log);
            InvokeCallback(false);
        }

        private void StartNewRound()
        {
            _plugin.Session.AddRound();
            InvokeCallback(true);
        }

        protected override void DisposeUseCase()
        {
        }
    }
}