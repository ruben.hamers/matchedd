using System;
using HamerSoft.Core;
using HamerSoft.Core.UseCases;
using MaTcheDD.Entities;
using MaTcheDD.Entities.Repositories;

namespace MaTcheDD.UseCases
{
    public class CreateSession : UseCase
    {
        private readonly Main _main;
        private readonly Entities.Player _player;
        private readonly int _tracks;
        private readonly SessionRepository _repository;

        public CreateSession(Main main, Entities.Player player, int tracks)
        {
            _main = main;
            _player = player;
            _tracks = tracks;
            _repository = _main.GetRepository<SessionRepository>();
        }

        public override void Execute(Action callback)
        {
            base.Execute(callback);
            Session session = new Session(_player.Id,_tracks);
            _repository.AddObject(session);
            _main.GetPlugin<MatchedPlugin>().Session = session;
            InvokeCallback();
        }

        protected override void DisposeUseCase()
        {
        }
    }
}