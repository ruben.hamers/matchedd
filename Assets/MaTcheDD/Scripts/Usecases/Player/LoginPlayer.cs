using System;
using HamerSoft.Core;
using HamerSoft.Core.UseCases;
using MaTcheDD.Entities.Repositories;

namespace MaTcheDD.UseCases.Player
{
    public class LoginPlayer : UseCase<Entities.Player>
    {
        private readonly Main _main;
        private readonly string _name;
        private PlayerRepository _repository;
        private SettingsRepository _settingsRepository;

        public LoginPlayer(Main main, string name)
        {
            _main = main;
            _name = name;
            _repository = main.GetRepository<PlayerRepository>();
            _settingsRepository = main.GetRepository<SettingsRepository>();
        }

        public override void Execute(Action<Entities.Player> callback)
        {
            base.Execute(callback);
            Entities.Player p = _repository.GetPlayerByName(_name);
            if (p == null)
                CreateNewPlayer();
            else
            {
                _settingsRepository.LoadSettings(p.Id);
                InvokeCallback(p);
            }
        }

        private void CreateNewPlayer()
        {
            new CreatePlayer(_main, _name).Execute(InvokeCallback);
        }

        protected override void DisposeUseCase()
        {
        }
    }
}