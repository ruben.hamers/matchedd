using System;
using HamerSoft.Core;
using HamerSoft.Core.UseCases;
using MaTcheDD.Entities;
using MaTcheDD.Entities.Repositories;

namespace MaTcheDD.UseCases.Player
{
    public class CreatePlayer : UseCase<Entities.Player>
    {
        private readonly string _name;
        private PlayerRepository _repository;
        private SettingsRepository _settingsRepository;

        public CreatePlayer(Main main, string name)
        {
            _name = name;
            _repository = main.GetRepository<PlayerRepository>();
            _settingsRepository = main.GetRepository<SettingsRepository>();
        }

        public override void Execute(Action<Entities.Player> callback)
        {
            base.Execute(callback);
            Entities.Player p = new Entities.Player(_name);
            _settingsRepository.AddObject(new Settings(p.Id));
            _repository.AddObject(p);
            _repository.Flush();
            _settingsRepository.Flush();
            InvokeCallback(p);
        }

        protected override void DisposeUseCase()
        {
        }
    }
}