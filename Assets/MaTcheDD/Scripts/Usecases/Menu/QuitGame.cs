using System;
using System.Collections.Generic;
using HamerSoft.Core.UseCases;
using MaTcheDD.Views;
using UnityEngine;
using UnityEngine.Events;
using Object = UnityEngine.Object;

namespace MaTcheDD.UseCases.Menu
{
    public class QuitGame : UseCase
    {
        private readonly Func<object[], ChoiceView> _spawnAction;
        private ChoiceView _view;

        public QuitGame(Func<object[], ChoiceView> spawnAction)
        {
            _spawnAction = spawnAction;
        }

        public override void Execute(Action callback)
        {
            base.Execute(callback);
            _view = _spawnAction.Invoke(
                new object[]
                {
                    "Quit?",
                    "Are you sure you want to quit the game?",
                    new Dictionary<string, UnityAction>
                    {
                        {"Yes", Quit},
                        {"No", No}
                    }
                });
        }

        private void Quit()
        {
            Application.Quit();
        }

        private void No()
        {
            Object.Destroy(_view.gameObject);
        }

        protected override void DisposeUseCase()
        {
        }
    }
}