using System;
using System.Collections.Generic;
using HamerSoft.Core;
using HamerSoft.Core.UseCases;
using MaTcheDD.Views;
using UnityEngine.Events;

namespace MaTcheDD.UseCases.Menu
{
    public class BackToMenu : UseCase<bool>
    {
        private readonly Func<object[], ChoiceView> _spawnAction;
        private ChoiceView _view;

        public BackToMenu(Func<object[], ChoiceView> spawnAction)
        {
            _spawnAction = spawnAction;
        }

        public override void Execute(Action<bool> callback)
        {
            base.Execute(callback);
            _view = _spawnAction.Invoke(
                new object[]
                {
                    "Back to Menu!",
                    "Are you sure you want to go back to the menu? You will lose your progress.",
                    new Dictionary<string, UnityAction>
                    {
                        {"Back to Menu", GoBackToMenu},
                        {"Resume", Resume}
                    }
                });
        }

        private void Resume()
        {
            InvokeCallback(false);
        }

        private void GoBackToMenu()
        {
            InvokeCallback(true);
        }

        protected override void DisposeUseCase()
        {
            _view.ForceDestroyObject();
        }
    }
}