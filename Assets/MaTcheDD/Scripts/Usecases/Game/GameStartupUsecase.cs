using System;
using HamerSoft.Audio;
using HamerSoft.Core;
using HamerSoft.Core.UseCases;
using MaTcheDD.Views;
using UnityEngine;
using UnityEngine.UI;
using Canvas = HamerSoft.Core.Singletons.Canvas;

namespace MaTcheDD.UseCases
{
    public class GameStartupUseCase : UseCase
    {
        private MatchedPlugin _matchedPlugin;
        private readonly Main _main;
        private PlayAreaView _view;
        private HorizontalLayoutGroup _layoutGroup;
        private Func<Transform, ColumnView> _customSpawnFunction;
        private readonly AudioClip _gameAudio;

        public GameStartupUseCase(Main main, PlayAreaView view, HorizontalLayoutGroup group,
            Func<Transform, ColumnView> customSpawnFunction, AudioClip gameAudio)
        {
            _matchedPlugin = main.GetPlugin<MatchedPlugin>();
            _main = main;
            _view = view;
            _layoutGroup = group;
            _customSpawnFunction = customSpawnFunction;
            _gameAudio = gameAudio;
        }

        public override void Execute(Action callback)
        {
            base.Execute(callback);
            Canvas.GetInstance().GetCanvasScaler.referenceResolution = new Vector2(Canvas.GetInstance().GetCanvas.GetComponent<RectTransform>().sizeDelta.x,Canvas.GetInstance().GetCanvas.GetComponent<RectTransform>().sizeDelta.y);
            if (!_layoutGroup)
                return;
            _main.GetPlugin<AudioPlugin>().PlayAudioLooping(_gameAudio, 1, AudioGroup.Music, false);
            new CreateSession(_main, _matchedPlugin.Player, _matchedPlugin.GetTracks).Execute(SetupScene);
        }

        private void SetupScene()
        {
            SpawnColumns();
            new SwapPairHeads(_view, _matchedPlugin.GetTracks).Execute(InvokeCallback);
        }

        private void SpawnColumns()
        {
            ColumnView previousColumn = null;

            float columnWidth = 0;
            for (int i = 0; i < _matchedPlugin.GetTracks; i++)
            {
                ColumnView current = _customSpawnFunction.Invoke(_layoutGroup.transform);

                columnWidth = current.GetComponentInChildren<LayoutElement>().preferredWidth;
                current.gameObject.name = $"ColumnView_{i}";
                if (i > 0)
                {
                    previousColumn.SetRightAdjacent(current);
                    if (i < _matchedPlugin.GetTracks)
                        current.SetLeftAdjacent(previousColumn);
                }

                previousColumn = current;
            }

            //      SetColumnSpacing(columnWidth);
        }

        private void SetColumnSpacing(float columnWidth)
        {
            float max = ((RectTransform) _view.transform).sizeDelta.x;
            float spacing = (max - (columnWidth * _matchedPlugin.GetTracks)) / (_matchedPlugin.GetTracks + 1);
            _layoutGroup.spacing = spacing;
            _layoutGroup.padding = new RectOffset((int) spacing, (int) spacing, 0, 0);
        }

        protected override void DisposeUseCase()
        {
            _matchedPlugin = null;
            _view = null;
            _layoutGroup = null;
            _customSpawnFunction = null;
        }
    }
}