using System;
using System.Collections.Generic;
using HamerSoft.Core;
using HamerSoft.Core.UseCases;
using MaTcheDD.Views;
using UnityEngine.Events;
using Object = UnityEngine.Object;

namespace MaTcheDD.UseCases
{
    public class LostGame : UseCase<bool>
    {
        private readonly Func<object[], ChoiceView> _spawnAction;
        private ChoiceView _view;

        public LostGame(Func<object[], ChoiceView> spawnAction)
        {
            _spawnAction = spawnAction;
        }

        public override void Execute(Action<bool> callback)
        {
            base.Execute(callback);
            _view = _spawnAction.Invoke(
                new object[]
                {
                    "Lost Game!",
                    "You lost the game, do you want to try again, or go back to the menu?",
                    new Dictionary<string, UnityAction>
                    {
                        {"Back to Menu", BackToMenu},
                        {"Try again!", TryAgain}
                    }
                });
        }

        private void TryAgain()
        {
            InvokeCallback(true);
        }

        private void BackToMenu()
        {
            InvokeCallback(false);
        }


        protected override void DisposeUseCase()
        {
            _view.ForceDestroyObject();
        }
    }
}