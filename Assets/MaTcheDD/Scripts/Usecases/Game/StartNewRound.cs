using System;
using HamerSoft.Core.UseCases;
using MaTcheDD.Views;
using Canvas = HamerSoft.Core.Singletons.Canvas;

namespace MaTcheDD.UseCases
{
    public class StartNewRound : UseCase
    {
        private readonly HeaderView _header;
        private readonly MatchedPlugin _matchedPlugin;

        public StartNewRound(HeaderView header, MatchedPlugin matchedPlugin)
        {
            _header = header;
            _matchedPlugin = matchedPlugin;
        }

        public override void Execute(Action callback)
        {
            base.Execute(callback);
            UpdateRoundsUi();
            SwapHeadsAndSetToLineEnd();
        }

        private void SwapHeadsAndSetToLineEnd()
        {
            new SwapPairHeads(Canvas.GetInstance().GetComponentInChildren<PlayAreaView>(), _matchedPlugin.GetTracks)
                .Execute(InvokeCallback);
        }

        private void UpdateRoundsUi()
        {
            _header.SetRounds(_matchedPlugin.Session.GetRounds());
        }

        protected override void DisposeUseCase()
        {
        }
    }
}