using System;
using HamerSoft.Core;
using HamerSoft.Core.UseCases;
using MaTcheDD.Entities.Repositories.Bridge;
using MaTcheDD.Views;

namespace MaTcheDD.UseCases
{
    public class RestartGame : UseCase
    {
        private readonly Main _main;
        private readonly HeaderView _header;
        private MatchedPlugin _matchedPlugin;

        public RestartGame(Main main, HeaderView header)
        {
            _main = main;
            _matchedPlugin = main.GetPlugin<MatchedPlugin>();
            _header = header;
        }

        public override void Execute(Action callback)
        {
            base.Execute(callback);
            DeleteAllBridges();
            CreateNewSession();
        }

        private void DeleteAllBridges()
        {
            BridgeRepository bridgeRepository = _main.GetRepository<BridgeRepository>();
            bridgeRepository.RemoveAll();
            bridgeRepository.Flush();
        }

        private void CreateNewSession()
        {
            new CreateSession(_main, _matchedPlugin.Player, _matchedPlugin.GetTracks).Execute(StartNewRound);
        }

        private void StartNewRound()
        {
            new StartNewRound(_header, _matchedPlugin).Execute(InvokeCallback);
        }

        protected override void DisposeUseCase()
        {
        }
    }
}