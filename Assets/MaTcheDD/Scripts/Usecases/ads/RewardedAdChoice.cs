using System;
using System.Collections;
using System.Collections.Generic;
using HamerSoft.Core;
using HamerSoft.Core.Services;
using HamerSoft.Core.UseCases;
using HamerSoft.UnityAds;
using MaTcheDD.Views;
using UnityEngine;
using UnityEngine.Events;

namespace MaTcheDD.UseCases
{
    public class RewardedAdChoice : UseCase
    {
        private readonly Func<object[], ChoiceView> _spawnAction;
        private readonly string _message;
        private readonly Action _watchedAdCallback;
        private ChoiceView _view;
        private RewardedAd _rewardedAd;
        private Coroutine _startAdCoroutine;
        private UnityAdsPlugin _adsPlugin;

        public RewardedAdChoice(Main main, Func<object[], ChoiceView> spawnAction, string message,
            Action watchedAdCallback)
        {
            _adsPlugin = main.GetPlugin<UnityAdsPlugin>();
            _spawnAction = spawnAction;
            _message = message;
            _watchedAdCallback = watchedAdCallback;
        }

        public override void Execute(Action callback)
        {
            base.Execute(callback);
            _view = _spawnAction.Invoke(
                new object[]
                {
                    "Warning!",
                    _message,
                    new Dictionary<string, UnityAction>
                    {
                        {"Yes watch ad!", WatchAd},
                        {"No thank you.", InvokeCallback}
                    }
                });
        }

        private void WatchAd()
        {
            _rewardedAd = _adsPlugin.ShowRewardedAd(true);
            _startAdCoroutine = ServiceProvider.GetService<CoroutineService>().StartCoroutine(StartAd());
        }

        private IEnumerator StartAd()
        {
            _rewardedAd.Finished += RewardedAdOnFinished;
            _rewardedAd.Failed += RewardedAdOnFinished;
            _rewardedAd.Skipped += RewardedAdOnFinished;
            while (!_rewardedAd.IsReady && _rewardedAd != null)
                yield return new WaitForEndOfFrame();
            _rewardedAd?.Activate();
        }

        private void RewardedAdOnFinished(IAd ad)
        {
            RemoveAdEvents();
            _watchedAdCallback?.Invoke();
            InvokeCallback();
        }

        protected override void DisposeUseCase()
        {
            RemoveAdEvents();
            _view.ForceDestroyObject();
            if (_startAdCoroutine != null)
            {
                ServiceProvider.GetService<CoroutineService>().StopCoroutine(_startAdCoroutine);
                _startAdCoroutine = null;
            }
        }

        private void RemoveAdEvents()
        {
            if (_rewardedAd == null)
                return;
            _rewardedAd.Finished -= RewardedAdOnFinished;
            _rewardedAd.Failed -= RewardedAdOnFinished;
            _rewardedAd.Skipped -= RewardedAdOnFinished;
        }
    }
}