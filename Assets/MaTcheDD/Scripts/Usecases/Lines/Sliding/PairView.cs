using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using HamerSoft.UnityMvc;
using MaTcheDD.Entities;
using MaTcheDD.Lines;
using UnityEngine;

namespace MaTcheDD.Views
{
    public class PairView : HamerSoft.UnityMvc.BaseView
    {
        private StaticLine _staticLine;
        private Base _base;
        private Head _head;
        private List<BridgeView> _bridges;

        protected override object[] CustomStartArgs => new[] {_staticLine};

        protected override void Awake()
        {
            base.Awake();
            _bridges = new List<BridgeView>();
            _base = GetComponentInChildren<Base>();
            _head = GetComponentInChildren<Head>();
            _staticLine = GetComponentInChildren<StaticLine>();
        }

        protected override void Start()
        {
            SetCharacterPartId(_base);
            SetCharacterPartId(_head);
            base.Start();
        }

        private void SetCharacterPartId(CharacterPart cp)
        {
            if (cp)
                cp.SetId(Id);
        }

        public Base GetBase()
        {
            return _base;
        }

        public Head GetHead()
        {
            return _head;
        }

        public StaticLine GetLine()
        {
            return _staticLine;
        }

        public virtual void SetHead(Head head)
        {
            //check when there are multiple heads
            _head = head;
            if(!head)
                return;
            _head.transform.SetParent(transform);
            _head.transform.SetAsLastSibling();
            _head.ResetStartPos();
        }

        public bool IsMatched()
        {
            return _head &&_head.GetId() == _base.GetId() && !_head.IsSliding;
        }

        public bool ShouldMatch(Head head)
        {
            return _head && head.GetId() == _base.GetId();
        }

        public void MoveHeadToTop()
        {
            if(!_head)
                return;
            if (!_staticLine)
                _staticLine = GetComponentInChildren<StaticLine>();
            _head.transform.localPosition = _staticLine.GetEnd() +
                                            new Vector3(0, (_head.transform as RectTransform).rect.height / 2, 0);
        }

        public void AddBridge(BridgeView bridge)
        {
            _bridges.Add(bridge);
        }

        public void RemoveBridge(string removed)
        {
            var bv = _bridges.FirstOrDefault(b => b.GetBridgeId() == removed);
            if (bv)
                _bridges.Remove(bv);
        }

        public Dictionary<Tuple<Vector3, Vector3>, Tuple<string, string>> GetBridgePositionsOrderedDescending()
        {
            var atStartPos = _bridges.Where(b => b.GetStartPairId() == Id).ToDictionary(
                kvp => new Tuple<Vector3, Vector3>(kvp.GetStartPosition(), kvp.GetEndPosition()),
                kvp => new Tuple<string, string>(Id, kvp.GetEndPairId()));
            var atEndPos = _bridges.Where(b => b.GetEndPairId() == Id).ToDictionary(
                kvp => new Tuple<Vector3, Vector3>(kvp.GetEndPosition(), kvp.GetStartPosition()),
                kvp => new Tuple<string, string>(Id, kvp.GetStartPairId()));
            return atStartPos.Concat(atEndPos).OrderByDescending(pos => pos.Key.Item1.y)
                .ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
        }

        public BridgeView[] GetBridges()
        {
            return _bridges.ToArray();
        }

        public bool HasBridges()
        {
            return _bridges != null && _bridges.Count > 0;
        }
    }
}