using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using HamerSoft.Audio;
using HamerSoft.Core;
using HamerSoft.Core.Services;
using HamerSoft.Core.UseCases;
using MaTcheDD.Lines;
using MaTcheDD.UseCases.Sliding;
using MaTcheDD.Views;
using UnityEngine;
using UnityEngine.UI;
using Canvas = HamerSoft.Core.Singletons.Canvas;

namespace MaTcheDD.UseCases
{
    public struct SlideSounds
    {
        public AudioClip Success, Failed, Switch;
        public AudioPlugin Plugin;
        public bool IsValid => Success != null && Failed != null && Switch != null && Plugin != null;
    }

    public class SlideHeads : UseCase<bool>
    {
        private readonly float _speed;
        private readonly Action _failWarningCallback;
        private readonly SlideSounds _sounds;
        private PairView[] _pairViews;
        private List<Path> _paths;
        private List<int> _loopIds;
        private readonly Canvas _canvas;
        private readonly CanvasScaler _canvasScaler;
        public bool IsImmortal { get; protected set; }

        public SlideHeads(float speed, Action failWarningCallback, SlideSounds sounds)
        {
            _speed = speed;
            _failWarningCallback = failWarningCallback;
            _sounds = sounds;
            _canvas = Canvas.GetInstance();
            _canvasScaler = _canvas.GetCanvasScaler;
            _pairViews = _canvas.GetComponentsInChildren<PairView>();
        }

        public override void Execute(Action<bool> callback)
        {
            base.Execute(callback);
            _paths = CalculatePaths();
            _loopIds = new List<int>();
            if (_failWarningCallback != null && _paths.Any(p => !p.IsValidPath))
                _failWarningCallback.Invoke();
            else
                Slide();
        }

        public void Slide()
        {
            foreach (Path path in _paths)
            {
                int loopId = -1;
                path.SetupNavigate(_canvas.GetComponentInChildren<DrawingLayer>().transform);
                loopId = ServiceProvider.GetService<UpdateService>()
                    .StartUpdate(() => { UpdateHead(path, loopId); });
                _loopIds.Add(loopId);
            }
        }

        protected virtual void UpdateHead(Path p, int loopId)
        {
            if (!p.IsFinished())
                p.Navigate();
            else
                CheckAllPathsFinished(loopId);
        }

        protected virtual void CheckAllPathsFinished(int loopId)
        {
            ServiceProvider.GetService<UpdateService>().StopUpdate(loopId);
            if (!_paths.All(p => p.IsFinished()))
                return;

            foreach (Path path in _paths)
                path.SetHeadToEndBase();

            InvokeCallback(_paths.All(p => p.IsCorrect()));
        }

        protected virtual List<Path> CalculatePaths()
        {
            List<Path> paths = new List<Path>();
            foreach (var pair in _pairViews)
                paths.Add(CalculatePath(pair, _pairViews));
            return paths;
        }

        protected virtual Path CalculatePath(PairView currentPairView, PairView[] otherPairviews)
        {
            Path path = new Path(currentPairView, new Vector3(
                currentPairView.transform.position.x,
                currentPairView.GetHead().transform.position.y), _speed, _sounds);
            Vector3 currentPosition = currentPairView.GetHead().transform.position;
            KeyValuePair<Tuple<Vector3, Vector3>, Tuple<string, string>> lastNode =
                new KeyValuePair<Tuple<Vector3, Vector3>, Tuple<string, string>>(
                    new Tuple<Vector3, Vector3>(Vector3.zero, Vector3.zero),
                    new Tuple<string, string>(string.Empty, String.Empty));

            while (true)
            {
                var firstVectorFoundForBridges = GetNextBridgePos(currentPosition, currentPairView, lastNode);
                if (AreBridgePairViewsEmpty(firstVectorFoundForBridges))
                {
                    break;
                }

                currentPosition = GetFirstBridgePoint(firstVectorFoundForBridges);
                path.AddPosition(currentPosition, currentPairView);

                currentPosition = GetSecondBridgePoint(firstVectorFoundForBridges);
                currentPairView = GetPairViewById(otherPairviews, firstVectorFoundForBridges);
                path.AddPosition(currentPosition, currentPairView);

                lastNode = firstVectorFoundForBridges;
            }

            path.AddPosition(
                currentPairView.GetLine().GetStart() + new Vector3(currentPairView.transform.position.x,
                    (ResolutionToAspectRatio(new Vector3((currentPairView.GetBase().transform as RectTransform).rect.width,(currentPairView.GetBase().transform as RectTransform).rect.height)).y)), currentPairView);
            return path;
        }

        private static PairView GetPairViewById(PairView[] otherPairviews,
            KeyValuePair<Tuple<Vector3, Vector3>, Tuple<string, string>> firstVectorFoundForBridges)
        {
            return otherPairviews.FirstOrDefault(p => p.Id == firstVectorFoundForBridges.Value.Item2);
        }

        private  Vector3 GetSecondBridgePoint(
            KeyValuePair<Tuple<Vector3, Vector3>, Tuple<string, string>> firstVectorFoundForBridges)
        {
            return ResolutionToAspectRatio(firstVectorFoundForBridges.Key.Item2);
        }

        private  Vector3 GetFirstBridgePoint(
            KeyValuePair<Tuple<Vector3, Vector3>, Tuple<string, string>> firstVectorFoundForBridges)
        {
            return ResolutionToAspectRatio(firstVectorFoundForBridges.Key.Item1);
        }

        private static bool AreBridgePairViewsEmpty(
            KeyValuePair<Tuple<Vector3, Vector3>, Tuple<string, string>> firstVectorFoundForBridges)
        {
            return string.IsNullOrEmpty(firstVectorFoundForBridges.Value.Item1) ||
                   string.IsNullOrEmpty(firstVectorFoundForBridges.Value.Item2);
        }

        private KeyValuePair<Tuple<Vector3, Vector3>, Tuple<string, string>> GetNextBridgePos(Vector3 currentPosition,
            PairView view, KeyValuePair<Tuple<Vector3, Vector3>, Tuple<string, string>> lastNode)
        {
            var positions = view.GetBridgePositionsOrderedDescending();
            var found = new KeyValuePair<Tuple<Vector3, Vector3>, Tuple<string, string>>(
                new Tuple<Vector3, Vector3>(Vector3.zero, Vector3.zero), new Tuple<string, string>(null, null));

            if (positions.Count > 1)
                for (int i = 0; i < positions.Count; i++)
                {
                    var current = positions.ElementAt(i);
                    if (IsSmallerOrEqualThan(current, currentPosition) && !AreBridgesEqualOrInverted(current, lastNode))
                        return current;
                }
            else if (positions.Count == 1 && IsSmallerOrEqualThan(positions.ElementAt(0), currentPosition) &&
                     !AreBridgesEqualOrInverted(positions.ElementAt(0), lastNode))
            {
                return positions.ElementAt(0);
            }

            return found;
        }

        public bool AreBridgesEqualOrInverted(KeyValuePair<Tuple<Vector3, Vector3>, Tuple<string, string>> newKvp,
            KeyValuePair<Tuple<Vector3, Vector3>, Tuple<string, string>> addedKvp)
        {
            bool equal = newKvp.Key.Item1 == addedKvp.Key.Item1 && newKvp.Key.Item2 == addedKvp.Key.Item2 &&
                         newKvp.Value.Item1 == addedKvp.Value.Item1 && newKvp.Value.Item2 == addedKvp.Value.Item2;

            bool inverted = newKvp.Key.Item1 == addedKvp.Key.Item2 && newKvp.Key.Item2 == addedKvp.Key.Item1 &&
                            newKvp.Value.Item1 == addedKvp.Value.Item2 && newKvp.Value.Item1 == addedKvp.Value.Item2;

            return equal || inverted;
        }

        private bool IsSmallerOrEqualThan(KeyValuePair<Tuple<Vector3, Vector3>, Tuple<string, string>> kvp,
            Vector3 currentPosition)
        {
            return kvp.Key.Item1.y <= AspectRatioToResolution(currentPosition).y;
        }

        private Vector2 ResolutionToAspectRatio(Vector3 item)
        {
            return (new Vector2(_canvas.GetCanvas.pixelRect.width, _canvas.GetCanvas.pixelRect.height) / _canvasScaler.referenceResolution) *
                item;
        }
        
        private Vector2 AspectRatioToResolution(Vector3 currentPosition)
        {
            return (currentPosition / new Vector2(_canvas.GetCanvas.pixelRect.width, _canvas.GetCanvas.pixelRect.height)) *
                   _canvasScaler.referenceResolution;
        }

        protected override void DisposeUseCase()
        {
            _pairViews = null;
            if (_loopIds != null)
                foreach (var loopId in _loopIds)
                    ServiceProvider.GetService<UpdateService>().StopUpdate(loopId);

            _loopIds = null;
        }

        public void SetImmortal()
        {
            IsImmortal = true;
        }

        public void ResetHeadsPreSlide()
        {
            foreach (Path path in _paths)
                path.ResetHead();
        }
    }
}