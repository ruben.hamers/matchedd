using System.Collections.Generic;
using System.Linq;
using HamerSoft.Audio;
using HamerSoft.Core;
using JetBrains.Annotations;
using MaTcheDD.Views;
using UnityEngine;

namespace MaTcheDD.UseCases.Sliding
{
    public class Path : IIdentifiable
    {
        private Head _head;
        private readonly float _speed;
        private readonly SlideSounds _sounds;
        private Queue<Vector3> _path;
        private readonly PairView _startPairView;
        private PairView _endPairView;
        public string Id => _head != null ? _head.GetId() : string.Empty;
        public bool IsValidPath { get; protected set; }
        private Vector3 _currentPosition;
        private float _movementSpeed;

        public Path(PairView pair, Vector3 startPos, float speed, SlideSounds sounds)
        {
            _head = pair.GetHead();
            _startPairView = pair;
            _speed = speed;
            _sounds = sounds;
            _path = new Queue<Vector3>();
            _path.Enqueue(startPos);
        }

        public void AddPosition(Vector3 position, PairView targetPair)
        {
            _path.Enqueue(position);
            _endPairView = targetPair;
            IsValidPath = _endPairView.ShouldMatch(_head);
        }

        public bool IsFinished()
        {
            return _path.Count == 0;
        }

        /// <summary>
        /// should be called in update / coroutine
        /// </summary>
        public void Navigate()
        {
            if (!_head)
            {
                _path?.Dequeue();
                return;
            }

            if (_head.transform.position.AlmostEquals(_path.Peek(), 0.1f))
            {
                _path.Dequeue();
                PlaySounds();
                if (_path.Count > 0)
                {
                    _currentPosition = _head.transform.position;
                    _movementSpeed = 0;
                }
            }

            _movementSpeed += Time.deltaTime * _speed;
            if (_path.Count > 0)
                _head.transform.position = Vector3.Lerp(_currentPosition, _path.Peek(), _movementSpeed);
        }

        private void PlaySounds()
        {
            if (_path.Count == 0)
            {
                if (_sounds.IsValid)
                    _sounds.Plugin.PlayAudio(IsHeadOnCorrectBase() ? _sounds.Success : _sounds.Failed, 1, AudioGroup.Effects,
                        null);
            }
            else if (_sounds.IsValid)
            {
                _sounds.Plugin.PlayAudio(_sounds.Switch, 1, AudioGroup.Effects,
                    null);
            }
        }

        public void Dispose()
        {
            _head = null;
            _path = null;
        }

        public bool EqualBases()
        {
            return _endPairView && _startPairView.GetInstanceID() == _endPairView.GetInstanceID();
        }

        public void SetHeadToEndBase()
        {
            if (IsFinished()) // && !EqualBases())
                _endPairView.SetHead(_head);
        }

        public bool IsCorrect()
        {
            return _endPairView.IsMatched();
        }

        private bool IsHeadOnCorrectBase()
        {
            return _endPairView.GetBase().GetId() == _head.GetId();
        }

        public void ResetHead()
        {
            _startPairView.SetHead(_head);
            _startPairView.MoveHeadToTop();
        }

        public void SetupNavigate(Transform transform)
        {
            Transform headTransform;
            (headTransform = _head.transform).SetParent(transform);
            _currentPosition = headTransform.position;
        }
    }
}