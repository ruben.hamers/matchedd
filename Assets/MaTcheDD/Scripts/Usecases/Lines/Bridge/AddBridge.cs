﻿using System;
using HamerSoft.Core;
using HamerSoft.Core.UseCases;
using MaTcheDD.Entities;
using MaTcheDD.Entities.Repositories.Bridge;

namespace MaTcheDD.UseCases.Lines.Bridge
{
    public class AddBridge : UseCase
    {
        private readonly Main _main;
        private readonly BridgeNode _start;
        private readonly BridgeNode _end;
        private BridgeRepository _repository;

        public AddBridge(Main main, BridgeNode start, BridgeNode end)
        {
            _main = main;
            _repository = _main.GetRepository<BridgeRepository>();
            _start = start;
            _end = end;
        }

        public override void Execute(Action callback)
        {
            base.Execute(callback);
            if (_start != null && _end != null)
                _repository.AddObject(new Entities.Bridge(_start, _end));

            InvokeCallback();
        }

        protected override void DisposeUseCase()
        {
        }
    }
}