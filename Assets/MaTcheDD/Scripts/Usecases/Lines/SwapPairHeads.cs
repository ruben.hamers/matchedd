using System;
using System.Collections;
using System.Linq;
using HamerSoft.Core;
using HamerSoft.Core.Services;
using HamerSoft.Core.UseCases;
using MaTcheDD.Views;
using UnityEngine;

namespace MaTcheDD.UseCases
{
    public class SwapPairHeads : UseCase
    {
        private readonly PlayAreaView _playAreaView;
        private readonly int _trackAmount;
        private PairView[] _pairs;

        public SwapPairHeads(PlayAreaView playAreaView, int trackAmount)
        {
            _playAreaView = playAreaView;
            _trackAmount = trackAmount;
        }

        public override void Execute(Action callback)
        {
            base.Execute(callback);
            ServiceProvider.GetService<CoroutineService>().StartCoroutine(SwapHeads());
        }

        private IEnumerator SwapHeads()
        {
            _pairs = _playAreaView.GetComponentsInChildren<PairView>();
            while (_pairs.Length < _trackAmount)
            {
                yield return new WaitForEndOfFrame();
                _pairs = _playAreaView.GetComponentsInChildren<PairView>();
            }

            yield return new WaitForEndOfFrame();
            Head[] heads = _pairs.SelectMany(pv => pv.GetComponentsInChildren<Head>()).ToArray();
            Head[] shuffledHeads = heads.Shuffle().ToArray();
            while (heads.SequenceEqual(shuffledHeads))
                shuffledHeads = heads.Shuffle().ToArray();

            for (int i = 0; i < _pairs.Length; i++)
            {
                _pairs[i].SetHead(shuffledHeads[i]);
                _pairs[i].MoveHeadToTop();
            }

            InvokeCallback();
        }

        protected override void DisposeUseCase()
        {
            _pairs = null;
        }
    }
}