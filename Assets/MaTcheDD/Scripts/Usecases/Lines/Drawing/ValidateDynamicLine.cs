using System;
using System.Collections.Generic;
using System.Linq;
using HamerSoft.Core;
using HamerSoft.Core.UseCases;
using MaTcheDD.Entities;
using MaTcheDD.Entities.Repositories.Bridge;
using MaTcheDD.Lines;
using MaTcheDD.Views;
using UnityEngine;
using Canvas = HamerSoft.Core.Singletons.Canvas;

namespace MaTcheDD.UseCases.Lines.Drawing
{
    public class ValidateDynamicLine : UseCase<BridgeNode, BridgeNode>
    {
        private readonly DynamicLine _dynamicLine;
        private readonly PairView[] _pairs;
        private readonly Entities.Bridge[] _bridges;
        private PairView _startPair, _endPair;
        private Vector3 _startPosition, _endPosition;

        public ValidateDynamicLine(Main main, DynamicLine dynamicLine, PairView[] pairs)
        {
            _bridges = main.GetRepository<BridgeRepository>().GetAll().ToArray();
            _dynamicLine = dynamicLine;
            _pairs = pairs;
        }

        public override void Execute(Action<BridgeNode, BridgeNode> callback)
        {
            base.Execute(callback);
            if (_dynamicLine && _dynamicLine.GetPositions()[0] != Vector3.zero)
                FindPositions();
            else
                InvokeCallback(null, null);
        }

        private void FindPositions()
        {
            List<Vector3> previousPositions = new List<Vector3>();
            foreach (Vector3 position in _dynamicLine.GetPositions())
            {
                if (previousPositions.Count == 0)
                {
                    previousPositions.Add(position);
                    continue;
                }

                foreach (PairView pair in _pairs)
                {
                    if (previousPositions.Any(p => IsPositionLeftOfStaticLine(pair.GetLine(), p)) &&
                        IsPositionRightOfStaticLine(pair.GetLine(), position)
                        ||
                        previousPositions.Any(p => IsPositionRightOfStaticLine(pair.GetLine(), p)) &&
                        IsPositionLeftOfStaticLine(pair.GetLine(), position)
                    )
                    {
                        SetPairView(pair, position);
                    }
                }

                if (_startPair && _endPair)
                    break;
            }

            Finish();
        }

        private void Finish()
        {
            if (_startPair == null || _endPair == null)
                InvokeCallback(null, null);
            else if (IntersectWithExistingBridges(_startPosition, _endPosition))
                InvokeCallback(null, null);
            else
                InvokeCallback(new BridgeNode(_startPair.Id, _startPosition),
                    new BridgeNode(_endPair.Id, _endPosition));
        }

        private bool IntersectWithExistingBridges(Vector3 startPosition, Vector3 endPosition)
        {
            return _bridges.Any(b =>
                DoesLineIntersect(b.GetStart().Position, b.GetEnd().Position, startPosition, endPosition));
        }

        private bool DoesLineIntersect(Vector2 startLine1, Vector2 endLine1, Vector2 startLine2, Vector2 endLine2)
        {
            if (startLine1 == startLine2 && endLine1 == endLine2)
                return true;

            Vector2 a = endLine1 - startLine1;
            Vector2 b = startLine2 - endLine2;
            Vector2 c = startLine1 - startLine2;

            float alphaNumerator = b.y * c.x - b.x * c.y;
            float betaNumerator = a.x * c.y - a.y * c.x;
            float denominator = a.y * b.x - a.x * b.y;

            if (denominator == 0)
            {
                return false;
            }
            else if (denominator > 0)
            {
                if (alphaNumerator < 0 || alphaNumerator > denominator || betaNumerator < 0 ||
                    betaNumerator > denominator)
                {
                    return false;
                }
            }
            else if (alphaNumerator > 0 || alphaNumerator < denominator || betaNumerator > 0 ||
                     betaNumerator < denominator)
            {
                return false;
            }

            return true;
        }

        private void SetPairView(PairView pair, Vector3 position)
        {
            if (!_startPair)
                SetPairAndPos(ref _startPair, pair, ref _startPosition, position);

            if (!_endPair && _startPair.Id != pair.Id)
                SetPairAndPos(ref _endPair, pair, ref _endPosition, position);
        }

        private void SetPairAndPos(ref PairView targetPair, PairView valuePair, ref Vector3 targetPosition,
            Vector3 position)
        {
            targetPair = valuePair;
            targetPosition = new Vector3(ToWorldPosX(valuePair.transform.transform), position.y, 0);
        }

        private bool IsPositionRightOfStaticLine(StaticLine line, Vector3 position)
        {
            return position.x > ToWorldPosX(line.transform);
        }

        private bool IsPositionLeftOfStaticLine(StaticLine line, Vector3 position)
        {
            
            return position.x < ToWorldPosX(line.transform);
        }


        private float ToWorldPosX(Transform transform)
        {
            return transform.position.x / Screen.width * 1920;
        }
        protected override void DisposeUseCase()
        {
        }
    }
}