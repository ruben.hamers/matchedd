using System.Linq;
using UnityEngine;

namespace MaTcheDD.Lines
{
    public class StaticLine : Line
    {
        public void Set(Vector3 start, Vector3 end)
        {
            if (UiLine.line.Count != 0)
                return;
            ForcePoint(transform.TransformPoint(start));
            ForcePoint(transform.TransformPoint(end));
        }

        public Vector3 GetStart()
        {
            if (UiLine.line.Count == 0)
                return Vector3.zero;
            return UiLine.line.First().position;
        }

        public Vector3 GetEnd()
        {
            if (UiLine.line.Count <= 1)
                return Vector3.zero;
            return UiLine.line.Last().position;
        }
    }
}