using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MaTcheDD.Lines
{
    public class DynamicLine : Line
    {
        public virtual void PushPosition()
        {
            UiLine.line.Push();
        }

        public virtual void EditPosition(Vector3 pos)
        {
            UiLine.line.EditPoint(pos);
        }

        public Vector3[] GetPositions()
        {
            return UiLine.line.Select(p => p.position).ToArray();
        }
    }
}