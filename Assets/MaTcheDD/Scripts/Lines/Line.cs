﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using geniikw.DataRenderer2D;
using UnityEngine;
using Object = HamerSoft.Core.Object;

namespace MaTcheDD.Lines
{
    [RequireComponent(typeof(UILine))]
    public abstract class Line : Object
    {
        protected UILine UiLine;
        private float _thickness;
        private Gradient _color;
        private bool isInitialized;

        protected override void Awake()
        {
            base.Awake();
            if (isInitialized)
                return;
            SetupUiLine();
        }

        protected override void Start()
        {
            base.Start();
            UiLine.line.owner = this;
        }

        private void SetupUiLine()
        {
            UiLine = GetComponent<UILine>();
            UiLine.line.owner = this;
            UiLine.line.option.startRatio = 0;
            UiLine.line.option.endRatio = 1;
            SetupDefaultSplineAndPopPositions();
            isInitialized = true;
            UiLine.line.owner = this;
        }

        private void SetupDefaultSplineAndPopPositions()
        {
            UiLine.line = Spline.Default;
            UiLine.line.Pop();
            UiLine.line.Pop();
        }

        public void Init(float thickness, Gradient color)
        {
            if (!isInitialized)
                SetupUiLine();
            _color = color;
            _thickness = thickness;
            UiLine.line.option.color = color;
        }

        protected void ForcePoint(Vector3 point)
        {
            if (!UiLine)
                UiLine = GetComponent<UILine>();
            if (UiLine.line.owner == null)
                UiLine.line.owner = this;
            UiLine.line.Push();
            int last = UiLine.line.Count - 1;
            UiLine.line.EditPoint(last, point, _thickness);
        }
    }
}