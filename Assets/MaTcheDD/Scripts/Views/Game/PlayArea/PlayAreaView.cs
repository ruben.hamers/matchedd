using HamerSoft.UnityMvc;
using UnityEngine;
using UnityEngine.UI;

namespace MaTcheDD.Views
{
    public class PlayAreaView : BaseView
    {
        [SerializeField] private AudioClip gameAudio;
        private HorizontalLayoutGroup _layoutGroup;
        private UiLayer _bridgeLayer, _drawingLayer;
        private ColumnView[] _columns;
        protected override object[] CustomStartArgs => new object[] {_layoutGroup, gameAudio};

        protected override void Awake()
        {
            base.Awake();
            Screen.SetResolution(1920, 1080, FullScreenMode.ExclusiveFullScreen);
            _layoutGroup = GetComponentInChildren<HorizontalLayoutGroup>();
            _bridgeLayer = GetComponentInChildren<BridgeLayer>();
            _drawingLayer = GetComponentInChildren<DrawingLayer>();
        }
    }
}