using HamerSoft.UnityMvc;

namespace MaTcheDD.Views
{
    public class GameView : BaseView
    {
        private HeaderView _header;
        private PlayAreaView _playArea;
        public event ViewEvent test;

        protected override object[] CustomStartArgs { get; }

        protected override void Awake()
        {
            base.Awake();
            _header = GetComponentInChildren<HeaderView>();
            _playArea = GetComponentInChildren<PlayAreaView>();
        }

        protected override void Start()
        {
            base.Start();
            test?.Invoke(this);
        }
    }
}