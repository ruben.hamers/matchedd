using HamerSoft.Core;
using UnityEngine;
using UnityEngine.UI;

namespace MaTcheDD.Views
{
    public class Head : CharacterPart
    {
        private Vector3 _startPosition;
        public bool IsSliding { get; protected set; }

        protected override void Awake()
        {
            base.Awake();
            _startPosition = transform.localPosition;
            IsSliding = false;
        }

        public void ResetStartPos()
        {
            transform.localPosition = _startPosition;
        }
    }
}