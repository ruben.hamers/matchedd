using UnityEngine;
using UnityEngine.UI;
using Object = HamerSoft.Core.Object;

namespace MaTcheDD.Views
{
    [RequireComponent(typeof(Image))]
    public abstract class CharacterPart : Object
    {
        private Image _image;
        private string Id;

        protected override void Awake()
        {
            base.Awake();
            _image = GetComponent<Image>();
        }

        public void SetColor(Color color)
        {
            if (!_image)
                _image = GetComponent<Image>();
            _image.color = color;
        }

        public void SetId(string id)
        {
            Id = id;
        }

        public string GetId()
        {
            return Id;
        }
    }

    public class Base : CharacterPart
    {

        public Vector3 GetPivot()
        {
            return new Vector3(0, GetComponent<RectTransform>().sizeDelta.y, 0);
        }
    }
}