using HamerSoft.Core;
using HamerSoft.UnityMvc;
using MaTcheDD.Entities;
using MaTcheDD.Lines;
using TMPro;
using UnityEngine;
using Canvas = HamerSoft.Core.Singletons.Canvas;

namespace MaTcheDD.Views
{
    [RequireComponent(typeof(StaticLine))]
    public class BridgeView : BaseView
    {
        private StaticLine _line;
        private string bridgeId;
        private Vector3 _startPosition;
        private Vector3 _endPosition;
        private string _startPairViewId;
        private string _endPairViewId;
        protected override object[] CustomStartArgs { get; }

        public override void Init(params object[] args)
        {
            base.Init();
            bridgeId = args.GetArgument<string>(0);
            BridgeNode startNode = args.GetArgument<BridgeNode>(1);
            BridgeNode endNode = args.GetArgument<BridgeNode>(2);
            float lineThickness = args.GetArgument<float>(3);
            Gradient lineColor = args.GetArgument<Gradient>(4);

            _line = GetComponent<StaticLine>();
            _line.Init(lineThickness, lineColor);
            _endPosition = endNode.Position;
            _startPosition = startNode.Position;
            _startPairViewId = startNode.PairView;
            _endPairViewId = endNode.PairView;
            _line.Set(_startPosition, _endPosition);
        }

        public Vector3 GetStartPosition()
        {
//            var _canvas = Canvas.GetInstance();
//            var _canvasScaler = _canvas.GetCanvasScaler;
//            return (new Vector2(_canvas.GetCanvas.pixelRect.width, _canvas.GetCanvas.pixelRect.height) / _canvasScaler.referenceResolution) *
//                   _startPosition;
            return _startPosition;
        }

        public string GetStartPairId()
        {
            return _startPairViewId;
        }

        public Vector3 GetEndPosition()
        {
//            var _canvas = Canvas.GetInstance();
//            var _canvasScaler = _canvas.GetCanvasScaler;
//            return (new Vector2(_canvas.GetCanvas.pixelRect.width, _canvas.GetCanvas.pixelRect.height) /
//                    _canvasScaler.referenceResolution) *
//                   _endPosition;
            return _endPosition;
        }

        public string GetEndPairId()
        {
            return _endPairViewId;
        }

        public string GetBridgeId()
        {
            return bridgeId;
        }
    }
}