using HamerSoft.Core;
using HamerSoft.UnityMvc;
using MaTcheDD.Lines;
using UnityEngine;
using UnityEngine.EventSystems;

namespace MaTcheDD.Views
{
    public class DrawingView : BaseView, IDragHandler, IPointerDownHandler, IPointerUpHandler
    {
        public event ViewEvent Dragged, PointerDown, PointerUp;
        protected override object[] CustomStartArgs { get; }

        DynamicLine _dynamicLine;
        Vector2 _previousPosition;

        protected override void Awake()
        {
            transform.SetAsLastSibling();
            base.Awake();
        }

        protected override void Start()
        {
            _dynamicLine = GetComponent<DynamicLine>();
            base.Start();
        }

        public override void Init(params object[] args)
        {
            base.Init();
            float thickness = args.GetArgument<float>(0);
            Gradient color = args.GetArgument<Gradient>(1);
            _dynamicLine.Init(thickness, color);
        }

        public virtual void OnDrag(PointerEventData eventData)
        {
            Dragged?.Invoke(this, _dynamicLine, _previousPosition, eventData.position);
            _previousPosition = eventData.position;
        }

        public virtual void OnPointerDown(PointerEventData eventData)
        {
            PointerDown?.Invoke(this, _dynamicLine, eventData.position);
        }

        public virtual void OnPointerUp(PointerEventData eventData)
        {
            PointerUp?.Invoke(this, _dynamicLine);
        }
    }
}