using HamerSoft.UnityMvc;
using UnityEngine;
using UnityEngine.UI;
using Object = HamerSoft.Core.Object;

namespace MaTcheDD.Views
{
    public class Score : Object
    {
        [SerializeField] private Text Value;

        protected override void Start()
        {
            base.Start();
            SetRounds(0);
        }

        public void SetRounds(int score)
        {
            Value.text = score.ToString();
        }
    }
}