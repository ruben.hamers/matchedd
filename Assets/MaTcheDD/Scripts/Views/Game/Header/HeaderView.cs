using System;
using HamerSoft.UnityMvc;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace MaTcheDD.Views
{
    public class HeaderView : BaseView
    {
        public event ViewEvent Goed, BackToMenuPressed;
        private Score _score;
        [SerializeField] private Button menuButton, goButton;
        [SerializeField] private AudioClip GoClip, SuccessClip, FailClip, SwitchClip;

        protected override object[] CustomStartArgs => new object[] {GoClip, SuccessClip, FailClip, SwitchClip};

        protected override void Awake()
        {
            base.Awake();
            _score = GetComponentInChildren<Score>();
            SetupButton(menuButton, BackToMenu);
            SetupButton(goButton, Go);
        }

        private void SetupButton(Button button, UnityAction action)
        {
            if (!button)
                return;
            button.onClick.AddListener(action);
        }

        private void BackToMenu()
        {
            BackToMenuPressed?.Invoke(this, new object[0]);
        }

        private void Go()
        {
            Goed?.Invoke(this, new object[0]);
        }

        public void SetRounds(int rounds)
        {
            _score?.SetRounds(rounds);
        }
    }
}