using MaTcheDD.Lines;
using UnityEngine;
using BaseView = HamerSoft.UnityMvc.BaseView;

namespace MaTcheDD.Views
{
    public class ColumnView : BaseView
    {   private PairView _pairView;
        private ColumnView LeftAdjacent, RightAdjacent;

        protected override object[] CustomStartArgs { get; }

        public ColumnView GetRightAdjacent()
        {
            return RightAdjacent;
        }

        public ColumnView GetLeftAdjacent()
        {
            return LeftAdjacent;
        }

        public void SetRightAdjacent(ColumnView right)
        {
            if (!RightAdjacent)
                RightAdjacent = right;
        }

        public void SetLeftAdjacent(ColumnView left)
        {
            if (!LeftAdjacent)
                LeftAdjacent = left;
        }
    }
}