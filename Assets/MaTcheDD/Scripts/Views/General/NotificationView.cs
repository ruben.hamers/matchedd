using System.Collections.Generic;
using HamerSoft.Core;
using HamerSoft.UnityMvc;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace MaTcheDD.Views
{
    public abstract class NotificationView : BaseView
    {
        [SerializeField] private NotificationButton ButtonPrefab;
        [SerializeField] private Text Title, Content;
        private LayoutGroup _layoutGroup;

        protected override void Awake()
        {
            base.Awake();
            _layoutGroup = GetComponentInChildren<LayoutGroup>();
            if (!ButtonPrefab)
                Debug.LogWarning($"Notification button prefab is not assigned in {name}!");
        }

        public override void Init(params object[] initArguments)
        {
            base.Init(initArguments);
            string title = initArguments.GetArgument<string>(0);
            string content = initArguments.GetArgument<string>(1);
            Dictionary<string, UnityAction> callbacks = initArguments.GetArgument<Dictionary<string, UnityAction>>(2);

            Title.text = title;
            Content.text = content;
            SetCallbacks(callbacks);
        }

        private void SetCallbacks(Dictionary<string, UnityAction> buttons)
        {
            if (buttons != null && ButtonPrefab && _layoutGroup)
                buttons.SafeForeach(SpawnButton);
        }

        private void SpawnButton(string buttonText, UnityAction callback)
        {
            NotificationButton button = Instantiate(ButtonPrefab, _layoutGroup.transform);
            button.SetupButton(buttonText, callback);
        }
    }
}