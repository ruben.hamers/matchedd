using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Object = HamerSoft.Core.Object;

namespace MaTcheDD.Views
{
    [RequireComponent(typeof(Button))]
    public class NotificationButton : Object
    {
        private Button _button;
        private Text _text;

        protected override void Awake()
        {
            base.Awake();
            _button = GetComponent<Button>();
            _text = _button.GetComponentInChildren<Text>();
        }

        public void SetupButton(string buttonName, UnityAction callback)
        {
            if (_button)
                _button.onClick.AddListener(callback);
            if (_text)
                _text.text = buttonName;
        }
    }
}