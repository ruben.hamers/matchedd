﻿using System.Linq;
using HamerSoft.Core;
using HamerSoft.UnityMvc;
using UnityEngine;
using UnityEngine.UI;

namespace MaTcheDD.Views
{
    public class LoginView : BaseView
    {
        [SerializeField] private Text feedbackText;
        [SerializeField] private AudioClip menuAudio;
        [SerializeField] private InputField userName, passWord;
        public event ViewEvent LocalLoggedin, FaceBookLoggedin, RemoteLoggedin, loginChoiceChanged;
        private Dropdown _loginChoice;
        private Button _loginButton;

        protected override object[] CustomStartArgs => new[] {menuAudio};

        protected override void Awake()
        {
            Screen.SetResolution(1920, 1080, FullScreenMode.ExclusiveFullScreen);
            base.Awake();
            _loginChoice = GetComponentInChildren<Dropdown>();
            _loginButton = GetComponentInChildren<Button>();
            _loginButton.AddListener(FaceBookLogin);
        }

        protected virtual void Login()
        {
            SetFeedbackText("");
            _loginButton.interactable = false;
            if (string.IsNullOrEmpty(userName.text))
                SetFeedbackText("Please enter a username!");
            else
                LocalLoggedin?.Invoke(this, userName.text);
        }

        protected virtual void FaceBookLogin()
        {
            SetFeedbackText("");
            _loginButton.interactable = false;
            FaceBookLoggedin?.Invoke(this, new object[] {userName.text, passWord.text});
        }

        protected virtual void RemoteLogin()
        {
            SetFeedbackText("");
            _loginButton.interactable = false;
            RemoteLoggedin?.Invoke(this, new object[] {userName.text, passWord.text});
        }

        public void SetFeedbackText(string text)
        {
            _loginButton.interactable = true;
            feedbackText.text = text;
        }

        public virtual void SetLoginChoices(string[] choices)
        {
            if (choices == null)
                return;
            _loginChoice.onValueChanged.RemoveAllListeners();
            _loginChoice.options = choices.Select(c => new Dropdown.OptionData(c)).ToList();
            _loginChoice.value = 0;
            _loginChoice.onValueChanged.AddListener(ChoiceChanged);
        }

        private void ChoiceChanged(int index)
        {
            SetFeedbackText("");
            loginChoiceChanged?.Invoke(this, _loginChoice.options[index].text);
        }

        public void ShowViewForFBLogin()
        {
            passWord.transform.parent.gameObject.SetActive(true);
            _loginButton.onClick.RemoveAllListeners();
            _loginButton.onClick.AddListener(FaceBookLogin);
        }

        public void ShowViewForLocalLogin()
        {
            passWord.transform.parent.gameObject.SetActive(false);
            _loginButton.onClick.RemoveAllListeners();
            _loginButton.onClick.AddListener(Login);
        }

        public void ShowViewForRemoteLogin()
        {
            passWord.transform.parent.gameObject.SetActive(true);
            _loginButton.onClick.RemoveAllListeners();
            _loginButton.onClick.AddListener(RemoteLogin);
        }
    }
}