using HamerSoft.Core;
using HamerSoft.UnityMvc;
using UnityEngine;
using UnityEngine.UI;

namespace MaTcheDD.Views
{
    public class MenuView : BaseView
    {
        public event ViewEvent Quited, HighScored, GameStarted, Settinged;
        [SerializeField] private Button startGameButten, highScoresButton, settingsButton, quitButton;
        protected override object[] CustomStartArgs { get; }

        protected override void Awake()
        {
            base.Awake();
            startGameButten.AddListener(ShowStartGameView);
            highScoresButton.AddListener(ShowHighScoreView);
            settingsButton.AddListener(ShowSettingsView);
            quitButton.AddListener(QuitGame);
        }

        private void ShowSettingsView()
        {
            Settinged?.Invoke(this);
        }

        private void QuitGame()
        {
            Quited?.Invoke(this);
        }

        private void ShowHighScoreView()
        {
            HighScored?.Invoke(this);
        }

        private void ShowStartGameView()
        {
            GameStarted?.Invoke(this);
        }
    }
}