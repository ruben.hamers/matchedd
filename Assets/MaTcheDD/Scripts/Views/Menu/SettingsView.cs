using HamerSoft.Core;
using HamerSoft.UnityMvc;
using UnityEngine;
using UnityEngine.UI;

namespace MaTcheDD.Views.Menu
{
    public class SettingsView : BaseView
    {
        public event ViewEvent MasterChanged,
            AmbientChanged,
            EffectsChanged,
            VoiceChanged,
            WorldChanged,
            MusicChanged,
            BackPressed;

        [SerializeField]
        private Slider MasterSlider, AmbientSlider, EffectsSlider, VoiceSlider, WorldSlider, MusicSlider;

        [SerializeField] private Button BackButton;

        protected override object[] CustomStartArgs { get; }

        protected override void Awake()
        {
            base.Awake();
            MasterSlider.onValueChanged.AddListener(volume => MasterChanged?.Invoke(this, new object[] {volume}));
            AmbientSlider.onValueChanged.AddListener(volume => AmbientChanged?.Invoke(this, new object[] {volume}));
            EffectsSlider.onValueChanged.AddListener(volume => EffectsChanged?.Invoke(this, new object[] {volume}));
            VoiceSlider.onValueChanged.AddListener(volume => VoiceChanged?.Invoke(this, new object[] {volume}));
            WorldSlider.onValueChanged.AddListener(volume => WorldChanged?.Invoke(this, new object[] {volume}));
            MusicSlider.onValueChanged.AddListener(volume => MusicChanged?.Invoke(this, new object[] {volume}));
            BackButton.AddListener(OnBackPressed);
        }

        public override void Init(params object[] initArguments)
        {
            float masterVolume = initArguments.GetArgument<float>(0);
            float ambientVolume = initArguments.GetArgument<float>(1);
            float effectsVolume = initArguments.GetArgument<float>(2);
            float voiceVolume = initArguments.GetArgument<float>(3);
            float worldVolume = initArguments.GetArgument<float>(4);
            float musicVolume = initArguments.GetArgument<float>(5);

            MasterSlider.SetValueWithoutNotify(masterVolume);
            AmbientSlider.SetValueWithoutNotify(ambientVolume);
            EffectsSlider.SetValueWithoutNotify(effectsVolume);
            VoiceSlider.SetValueWithoutNotify(voiceVolume);
            WorldSlider.SetValueWithoutNotify(worldVolume);
            MusicSlider.SetValueWithoutNotify(musicVolume);
        }

        private void OnBackPressed()
        {
            BackPressed?.Invoke(this);
        }
    }
}