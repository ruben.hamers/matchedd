using System.Collections.Generic;
using System.Linq;
using HamerSoft.Core;
using HamerSoft.UnityMvc;
using UnityEngine;
using UnityEngine.UI;

namespace MaTcheDD.Views.Menu
{
    public struct HighScore
    {
        public string PlayerName;
        public int Rounds;

        public HighScore(string playerName, int rounds)
        {
            PlayerName = playerName;
            Rounds = rounds;
        }
    }

    public class HighScoresView : BaseView
    {
        public event ViewEvent NormalPressed, IntermediatePressed, HardPressed, ExtraHardPressed, BackPressed;
        [SerializeField] private Button normalButton, intermediateButton, hardButton, extraHardButton, backButton;
        [SerializeField] private HighScoreRow RowPrefab;
        protected override object[] CustomStartArgs { get; }
        private LayoutGroup _layoutGroup;

        protected override void Awake()
        {
            base.Awake();
            _layoutGroup = GetComponentInChildren<VerticalLayoutGroup>();
            normalButton.AddListener(OnNormalPressed);
            intermediateButton.AddListener(OnIntermediatePressed);
            hardButton.AddListener(OnHardPressed);
            extraHardButton.AddListener(OnExtraHardPressed);
            backButton.AddListener(OnBackPressed);
        }

        private void OnBackPressed()
        {
            BackPressed?.Invoke(this, new object[0]);
        }

        private void OnExtraHardPressed()
        {
            ExtraHardPressed?.Invoke(this, new object[0]);
        }

        private void OnHardPressed()
        {
            HardPressed?.Invoke(this, new object[0]);
        }

        private void OnIntermediatePressed()
        {
            IntermediatePressed?.Invoke(this, new object[0]);
        }

        private void OnNormalPressed()
        {
            NormalPressed?.Invoke(this, new object[0]);
        }

        public override void Init(params object[] initArguments)
        {
            base.Init(initArguments);
            List<HighScore> scores = initArguments.GetArgument<List<HighScore>>(0);
            SetScores(scores);
        }

        public virtual void SetScores(List<HighScore> scores)
        {
            _layoutGroup.GetComponentsInChildren<HighScoreRow>().ToList().ForEach(row => { Destroy(row.gameObject); });
            foreach (HighScore score in scores)
                SpawnRow(score);
        }

        private void SpawnRow(HighScore score)
        {
            HighScoreRow row = Instantiate(RowPrefab, _layoutGroup.transform);
            row.Setup(score.PlayerName, score.Rounds.ToString());
        }
    }
}