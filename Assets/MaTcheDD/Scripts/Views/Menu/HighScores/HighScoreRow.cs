using UnityEngine;
using UnityEngine.UI;
using Object = HamerSoft.Core.Object;

namespace MaTcheDD.Views.Menu
{
    public class HighScoreRow : Object
    {
        [SerializeField] private Text NameLabel, RoundsLabel;

        public void Setup(string name, string rounds)
        {
            if (NameLabel)
                NameLabel.text = name;
            if (RoundsLabel)
                RoundsLabel.text = rounds;
        }
    }
}