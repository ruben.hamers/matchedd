using System.Linq;
using HamerSoft.Core;
using HamerSoft.UnityMvc;
using UnityEngine;
using UnityEngine.UI;

namespace MaTcheDD.Views.Menu
{
    public class StartGameView : BaseView
    {
        public event ViewEvent GameStarted, BackPressed;
        [SerializeField] private Button startGameButton, backButton;
        private Dropdown _tracksDropDown;

        protected override object[] CustomStartArgs { get; }

        protected override void Awake()
        {
            base.Awake();
            _tracksDropDown = GetComponentInChildren<Dropdown>();
            startGameButton.AddListener(OnGameStarted);
            backButton.AddListener(OnBackPressed);
        }

        public override void Init(params object[] initArguments)
        {
            base.Init(initArguments);
            int[] tracks = initArguments.GetArgument<int[]>(0);
            SetupDropDown(tracks);
        }

        private void SetupDropDown(int[] tracks)
        {
            if (!_tracksDropDown)
                return;

            _tracksDropDown.ClearOptions();
            _tracksDropDown.AddOptions(tracks.Select(t => new Dropdown.OptionData(t.ToString())).ToList());
            _tracksDropDown.value = 0;
        }

        private void OnBackPressed()
        {
            BackPressed?.Invoke(this, new object[0]);
        }

        private void OnGameStarted()
        {
            GameStarted?.Invoke(this, _tracksDropDown.options[_tracksDropDown.value].text);
        }
    }
}