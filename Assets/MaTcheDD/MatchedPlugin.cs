using System;
using HamerSoft.Core;
using MaTcheDD.Entities;
using MaTcheDD.UseCases.Player;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace MaTcheDD
{
    [Serializable]
    public class RunTimeConfig
    {
        [SerializeField] public int Tracks;
        [SerializeField] public Gradient LineColor;
        [SerializeField] public float LineThickness;
        [SerializeField] public float NavigationSpeed;
        [SerializeField] public float AddDistance;
    }

    [CreateAssetMenu(fileName = "MaTcheDDPlugin", menuName = "HamerSoft/MaTcheDD/MaTcheDDPlugin Create", order = 0)]
    public class MatchedPlugin : Plugin
    {
        public float GetLineThickness => runtimeConfig.LineThickness;
        public Gradient GetLineColor => runtimeConfig.LineColor;
        public int GetTracks => runtimeConfig.Tracks;
        public float NavigationSpeed => runtimeConfig.NavigationSpeed;
        public Player Player { get; protected set; }
        public Session Session { get; set; }

        [SerializeField] private RunTimeConfig runtimeConfig;


        public override void Init()
        {
        }

        public override void DeInit()
        {
        }

        public override void ApplicationQuit()
        {
            Player = null;
            Session = null;
        }

        public override void SetMain(Main main)
        {
            if (Player == null && Application.isEditor && Application.isPlaying && main != null && SceneManager.GetActiveScene().name == "Game")
                new CreatePlayer(main, "Foo").Execute(player => Player = player);
        }

        public void SetTracks(int tracks)
        {
            runtimeConfig.Tracks = tracks;
        }

        public float GetAddDistance()
        {
            return runtimeConfig.AddDistance;
        }

        public void SetPlayer(Player player)
        {
            Player = player;
        }
    }
}