using System;
using HamerSoft.Core;
using HamerSoft.Core.UseCases;
using HamerSoft.PlayFab;
using MaTcheDD.Entities;
using MaTcheDD.Entities.Repositories;

namespace PlayFab.Usecases.Login
{
    public abstract class LoginUsecase : UseCase<Player, string>
    {
        protected readonly Main Main;
        protected readonly string Username;
        protected readonly string Password;
        protected readonly string TitleId;
        protected PlayFabPlugin PlayFabPlugin;
        protected PlayerRepository Repository;
        protected bool AreFieldsValid { get; private set; }

        public LoginUsecase(Main main, string username, string password)
        {
            Main = main;
            Username = username;
            Password = password;
            Repository = main.GetRepository<PlayerRepository>();
            PlayFabPlugin = main.GetPlugin<PlayFabPlugin>();
            TitleId = PlayFabPlugin.GetTitleId();
        }

        public override void Execute(Action<Player, string> callback)
        {
            base.Execute(callback);
            if (string.IsNullOrEmpty(Username))
                InvokeCallback(null, "Please enter a username!");
            else if (string.IsNullOrEmpty(Password))
                InvokeCallback(null, "Please enter a password!");
            else if (Username.Length < 3 || Username.Length > 20)
                InvokeCallback(null, "Your username be between 3 and 20 characters");
            else if (Password.Length < 6)
                InvokeCallback(null, "Your password must be contain at least 6 characters!");
            else
                AreFieldsValid = true;
        }

        protected virtual void LoginSuccess(Player player)
        {
            Main.GetPlugin<PlayFabPlugin>().SetActivePlayer(player);
            InvokeCallback(player, null);
        }

        protected virtual void ErrorCallback(PlayFabError error)
        {
            InvokeCallback(null, error.ErrorMessage);
        }
    }
}