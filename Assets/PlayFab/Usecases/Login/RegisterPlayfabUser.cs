using System;
using HamerSoft.Core;
using MaTcheDD.Entities;
using MaTcheDD.UseCases.Player;
using PlayFab.ClientModels;

namespace PlayFab.Usecases.Login
{
    public class RegisterPlayfabUser : LoginUsecase
    {
        public RegisterPlayfabUser(Main main, string username, string password) : base(main, username, password)
        {
        }

        public override void Execute(Action<Player, string> callback)
        {
            base.Execute(callback);
            if (!AreFieldsValid)
                DisposeUseCase();
            else
                Login();
        }

        private void Login()
        {
            var request = new RegisterPlayFabUserRequest
            {
                TitleId = TitleId,
                Password = Password,
                DisplayName = Username,
                Username = Username,
                RequireBothUsernameAndEmail = false
            };
            PlayFabClientAPI.RegisterPlayFabUser(request, RegisterSuccess,
                ErrorCallback);
        }

        private void RegisterSuccess(RegisterPlayFabUserResult result)
        {
            new LoginPlayer(Main, result.Username)
                .Execute(LoginSuccess);
        }

        protected override void DisposeUseCase()
        {
        }
    }
}