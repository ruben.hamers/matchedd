using System;
using HamerSoft.Core;
using MaTcheDD.Entities;
using MaTcheDD.UseCases.Player;
using PlayFab.ClientModels;

namespace PlayFab.Usecases.Login
{
    public class LoginPlayfabUser : LoginUsecase
    {
        public LoginPlayfabUser(Main main, string username, string password) : base(main, username, password)
        {
        }

        public override void Execute(Action<Player, string> callback)
        {
            base.Execute(callback);
            if (!AreFieldsValid)
                DisposeUseCase();
            else
                Login();
        }

        private void Login()
        {
            var request = new LoginWithPlayFabRequest()
            {
                TitleId = TitleId,
                Password = Password,
                Username = Username,
            };
            PlayFabClientAPI.LoginWithPlayFab(request, LoggedIn
                , ErrorCallback);
        }

        private void LoggedIn(LoginResult result)
        {
            if (result.NewlyCreated)
                new RegisterPlayfabUser(Main, Username, Password).Execute(InvokeCallback);
            else
                new LoginPlayer(Main, Username)
                    .Execute(LoginSuccess);
        }

        protected override void ErrorCallback(PlayFabError error)
        {
            if (error.Error == PlayFabErrorCode.AccountNotFound)
                new RegisterPlayfabUser(Main, Username, Password).Execute(PlayedRegistered);
            else
                base.ErrorCallback(error);
        }

        private void PlayedRegistered(Player player, string message)
        {
            if (player != null)
                LoginSuccess(player);
            else
                InvokeCallback(null, message);
        }

        protected override void DisposeUseCase()
        {
        }
    }
}