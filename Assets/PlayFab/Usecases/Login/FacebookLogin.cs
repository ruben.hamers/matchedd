using System;
using Facebook.Unity;
using HamerSoft.Core;
using MaTcheDD.Entities;
using MaTcheDD.UseCases.Player;
using PlayFab.ClientModels;
using UnityEditor;
using LoginResult = PlayFab.ClientModels.LoginResult;

namespace PlayFab.Usecases.Login
{
    public class FacebookLogin : LoginUsecase
    {
        public FacebookLogin(Main main, string username, string password) : base(main, username, password)
        {
        }

        public virtual void Execute(Action<Player, string> callback)
        {
            base.Execute(callback);
            if (!AreFieldsValid)
                DisposeUseCase();
            else if (FB.IsInitialized)
                OnFacebookInitialized();
            else
                FB.Init(OnFacebookInitialized);
        }

        private void OnFacebookInitialized()
        {
            FB.LogInWithReadPermissions(null, OnFacebookLoggedIn);
        }

        private void OnFacebookLoggedIn(ILoginResult result)
        {
            if (result == null || string.IsNullOrEmpty(result.Error))
            {
                var request = new LoginWithFacebookRequest
                {
                    TitleId = TitleId,
                    CreateAccount = true,
                    AccessToken = AccessToken.CurrentAccessToken.TokenString
                };
                PlayFabClientAPI.LoginWithFacebook(request, FaceBookLoginSuccess, ErrorCallback);
            }
            else
                InvokeCallback(null, $"Facebook Auth Failed: {result.Error}, {result.RawResult}");
        }

        private void FaceBookLoginSuccess(LoginResult result)
        {
            if (result.NewlyCreated)
                new RegisterPlayfabUser(Main, Username, Password).Execute(InvokeCallback);
            else
                new LoginPlayer(Main, Username)
                    .Execute(LoginSuccess);
        }

        protected override void DisposeUseCase()
        {
        }
    }
}