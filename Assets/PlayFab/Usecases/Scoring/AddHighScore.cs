using System;
using System.Collections.Generic;
using HamerSoft.Core;
using HamerSoft.Core.UseCases;
using MaTcheDD.Entities;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine;

namespace HamerSoft.PlayFab
{
    public class AddHighScore: UseCase<string>
    {
        private readonly Session _session;

        public AddHighScore(Main main, Session session)
        {
            _session = session;
        }

        public override void Execute(Action<string> callback)
        {
            base.Execute(callback);
            PlayFabClientAPI.UpdatePlayerStatistics(new UpdatePlayerStatisticsRequest {
                Statistics = new List<StatisticUpdate> {
                    new StatisticUpdate {
                        StatisticName = $"ValidRounds_{_session.GetTracks()}",
                        Value = _session.GetRounds() - _session.GetRetries()
                    }
                }
            }, OnStatisticsUpdated, FailureCallback);
        }

        private void OnStatisticsUpdated(UpdatePlayerStatisticsResult updateResult) {
          InvokeCallback("Successfully updated HighScore");
        }

        private void FailureCallback(PlayFabError error){
            
            InvokeCallback(error.GenerateErrorReport());
        }

        protected override void DisposeUseCase()
        {
            
        }
    }
}