using System;
using System.Collections.Generic;
using HamerSoft.Core.UseCases;
using PlayFab;
using PlayFab.ClientModels;

namespace HamerSoft.PlayFab
{
    public class GetLeaderBoard : UseCase<List<Tuple<string, int>>>
    {
        private readonly int _tracks;

        public GetLeaderBoard(int tracks)
        {
            _tracks = tracks;
        }

        public override void Execute(Action<List<Tuple<string, int>>> callback)
        {
            base.Execute(callback);
            PlayFabClientAPI.GetLeaderboard(new GetLeaderboardRequest
            {
                StatisticName = $"ValidRounds_{_tracks}",
                StartPosition = 0,
                MaxResultsCount = 10
            }, SuccessCallBack, FailureCallback);
        }

        private void SuccessCallBack(GetLeaderboardResult result)
        {
            List<Tuple<string, int>> leaderBoard = new List<Tuple<string, int>>();
            foreach (PlayerLeaderboardEntry leaderboardEntry in result.Leaderboard)
                leaderBoard.Add(
                    new Tuple<string, int>(leaderboardEntry.Profile.DisplayName, leaderboardEntry.StatValue));
            InvokeCallback(leaderBoard);
        }


        private void FailureCallback(PlayFabError error)
        {
            InvokeCallback(null);
        }

        protected override void DisposeUseCase()
        {
        }
    }
}