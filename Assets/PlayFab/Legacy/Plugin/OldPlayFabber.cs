﻿using System;
using System.Collections.Generic;
using System.Text;
using Facebook.Unity;
using HamerSoft.Core;
using PlayFab;
using PlayFab.ClientModels;
using PlayFab.SharedModels;
using UnityEngine;
using LoginResult = PlayFab.ClientModels.LoginResult;

namespace HamerSoft.PlayFab
{
    /// <summary>
    /// the method of login
    /// </summary>
    public enum PlayFabLoginMethod
    {
        LoginWithCustomId,
        LoginWithFacebook,
        LoginWithPlayFab,
        RegisterPlayFabUser
    }

    /// <summary>
    /// plugin for everything related to Playfab
    /// </summary>
    public class OldPlayFabber : Plugin
    {
        /// <summary>
        /// the playFab GameTitle titleId, set this according to the playFab webpage
        /// </summary>
        [SerializeField] private string TitleId;

        /// <summary>
        /// the login Method
        /// </summary>
        public PlayFabLoginMethod playFabLoginMethod;

        /// <summary>
        /// all playfabUserInfo
        /// </summary>
        public PlayFabUserInfo playFabUserInfo;

        /// <summary>
        /// is playfab loggedin
        /// </summary>
        public bool IsLoggedIn { get; protected set; }

        /// <summary>
        /// on user data received
        /// </summary>
        public event Action<PlayFabResultCommon, PlayFabError> OnUserDataReceived;

        /// <summary>
        /// events for login methods
        /// </summary>
        public event Action<PlayFabResultCommon, PlayFabError> OnPlayFabUserLogin;

        /// <summary>
        /// Update catalog event
        /// </summary>
        public event EventHandler UpdateCatalogEvent;

        private User CurrentUser;

        public class User
        {
            public string Id, Firstname, Lastname, Username, Password, Email;

            public void SetUserName(string username)
            {
                Username = username;
            }

            public void SetPassword(string password)
            {
                Password = password;
            }

            public void SetEmail(string email)
            {
                Email = email;
            }
        }


        public override void Init()
        {
            IsLoggedIn = false;
            playFabUserInfo = new PlayFabUserInfo();
            PlayFabSettings.TitleId = TitleId;
        }

        public override void DeInit()
        {
        }

        public override void ApplicationQuit()
        {
            IsLoggedIn = false;
            playFabUserInfo = new PlayFabUserInfo();
        }

        public override void SetMain(Main main)
        {
        }

        /// <summary>
        /// login with payfab
        /// </summary>
        /// <param name="method"></param>
        /// <param name="errorCallBack"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="emailAddress"></param>
        /// <param name="name"></param>
        /// <param name="successCallBack"></param>
        public virtual void LoginPlayfab(PlayFabLoginMethod method, Action<object> successCallBack,
            Action<PlayFabError> errorCallBack, string userName = null,
            string password = null,
            string emailAddress = null, string name = null)
        {
            switch (method)
            {
                case PlayFabLoginMethod.LoginWithCustomId:
                    LoginWithCustomId(userName, successCallBack, errorCallBack);
                    break;
                case PlayFabLoginMethod.LoginWithFacebook:
                    LoginWithFacebook(successCallBack, errorCallBack);
                    break;
                case PlayFabLoginMethod.RegisterPlayFabUser:
                    if (password == null) break;
                    if (userName == null && emailAddress == null) break;
                    RegisterWithPlayFabUser(userName, password, emailAddress, null, successCallBack, errorCallBack);
                    break;
                case PlayFabLoginMethod.LoginWithPlayFab:
                    if (password == null) break;
                    if (userName == null && emailAddress == null) break;
                    LoginPlayFabUser(userName, password, successCallBack, errorCallBack);
                    break;
            }
        }

        /// <summary>
        /// add credentials to an existing account
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="email"></param>
        public virtual void AddAccountCredentials(string username, string password, string email)
        {
            var request = new AddUsernamePasswordRequest
            {
                Email = email,
                Password = password,
                Username = username
            };

            PlayFabClientAPI.AddUsernamePassword(request,
                result =>
                {
                    Debug.Log("account info added, Username = " + result.Username);
                    CurrentUser.Username = username;
                    CurrentUser.Password = password;
                    CurrentUser.SetEmail(email);
                    if (playFabUserInfo.AccountInfo != null)
                    {
                        playFabUserInfo.AccountInfo.Username = result.Username;
                    }

                    GetUserData();
                },
                error => { Debug.Log("Something went wrong during adding of credentials: " + error.ErrorMessage); });
        }

        /// <summary>
        /// login with custom Id
        /// </summary>
        /// <returns></returns>
        protected virtual void LoginWithCustomId(string id, Action<object> successCallBack,
            Action<PlayFabError> errorCallBack)
        {
            var request = new LoginWithCustomIDRequest
            {
                TitleId = TitleId,
                CreateAccount = true,
                CustomId = id ?? SystemInfo.deviceUniqueIdentifier
            };
            PlayFabClientAPI.LoginWithCustomID(request, result =>
            {
                OnPlayFabUserLogin_Handler(result);
                CurrentUser.Username = id;
                successCallBack.Invoke(result);
            }, error =>
            {
                OnPlayFabUserLoginError_Handler(error);
                errorCallBack(error);
            });
        }

        /// <summary>
        /// login with facebook (!CHECK THIS METHOD)
        /// </summary>
        protected virtual void LoginWithFacebook(Action<object> successCallBack, Action<PlayFabError> errorCallBack)
        {
            var request = new LoginWithFacebookRequest
            {
                TitleId = TitleId,
                CreateAccount = true,
                AccessToken = AccessToken.CurrentAccessToken.TokenString
            };
            PlayFabClientAPI.LoginWithFacebook(request, result =>
            {
                OnPlayFabUserLogin_Handler(result);
                successCallBack?.Invoke(result);
            }, error =>
            {
                OnPlayFabUserLoginError_Handler(error);
                errorCallBack?.Invoke(error);
            });
        }

        /// <summary>
        /// Login as a app user by username and password
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="email"></param>
        /// <param name="name"></param>
        /// <param name="successCallBack"></param>
        /// <param name="errorCallBack"></param>
        protected virtual void RegisterWithPlayFabUser(string username, string password, string email, string name,
            Action<object> successCallBack, Action<PlayFabError> errorCallBack)
        {
            var request = new RegisterPlayFabUserRequest
            {
                TitleId = TitleId,
                Password = password,
                DisplayName = username ?? name
            };
            if (username != null) request.Username = username;
            if (email != null) request.Email = email;
            PlayFabClientAPI.RegisterPlayFabUser(request, result =>
            {
                OnPlayFabUserRegister_Handler(result);
                if (email != null) CurrentUser.SetEmail(email);
                CurrentUser.SetUserName(result.Username);
                CurrentUser.Password = password;
                successCallBack.Invoke(result);
            }, error =>
            {
                Debug.Log(error.ErrorMessage);
                OnPlayFabUserLoginError_Handler(error);
                errorCallBack(error);
            });
        }

        /// <summary>
        /// login playfab user
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="successCallBack"></param>
        /// <param name="errorCallBack"></param>
        protected virtual void LoginPlayFabUser(string username, string password,
            Action<object> successCallBack, Action<PlayFabError> errorCallBack)
        {
            var request = new LoginWithPlayFabRequest()
            {
                TitleId = TitleId,
                Password = password,
                Username = username
            };
            PlayFabClientAPI.LoginWithPlayFab(request, result =>
            {
                ///update the catelog items
                UpdateCatalogEvent?.Invoke(this, null);
                successCallBack.Invoke(result);
            }, error =>
            {
                Debug.Log(error.ErrorMessage);
                OnPlayFabUserLoginError_Handler(error);
                errorCallBack?.Invoke(error);
            });
        }

        /// <summary>
        /// get the playFab account info
        /// </summary>
        public virtual void GetAccountInfo(Action<object> successCallBack = null,
            Action<PlayFabError> errorCallBack = null)
        {
            var request = new GetAccountInfoRequest
            {
                PlayFabId = playFabUserInfo.PlayFabId
            };

            PlayFabClientAPI.GetAccountInfo(request, result =>
            {
                playFabUserInfo.AccountInfo = result.AccountInfo;
                successCallBack?.Invoke(playFabUserInfo);
            }, error
                =>
            {
                Debug.Log("Something went wrong with getting the account info");
                Debug.Log(error.ErrorMessage);
                errorCallBack?.Invoke(error);
            });
        }

        /// <summary>
        /// Login and set the userdata
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public virtual User Login(string username, string password = null)
        {
            // todo check this mehtod..
            var user = new User();
            var data = new Dictionary<string, string>
            {
                {"userId", user.Id},
                {"userName", user.Username},
                {"firstName", user.Firstname},
                {"lastName", user.Lastname},
                {"password", user.Password}
            };
            SetUserData(data);
            return user;
        }

        /// <summary>
        /// Login by ID and set the userdata
        /// </summary>
        /// <returns></returns>
        public virtual User LoginWithId(string id)
        {
            //todo check this method
            var user = new User();
            var data = new Dictionary<string, string>
            {
                {"userId", user.Id},
                {"userName", user.Username},
                {"firstName", user.Firstname},
                {"lastName", user.Lastname},
                {"password", user.Password}
            };
            if (IsLoggedIn) SetUserData(data);
            playFabUserInfo.PlayFabId = user.Id;
            IsLoggedIn = true;
            return user;
        }

        /// <summary>
        /// register and login a user using all credentials
        /// </summary>
        /// <param name="id"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        public virtual User RegisterUser(string id, string username, string password, string email = null)
        {
            //todo check this method
            var user = new User();
            user.Password = password;
            user.SetUserName(username);
            user.SetEmail(email);
            var data = new Dictionary<string, string>
            {
                {"userId", user.Id},
                {"userName", user.Username},
                {"firstName", user.Firstname},
                {"lastName", user.Lastname},
                {"password", user.Password}
            };
            if (IsLoggedIn) SetUserData(data);
            playFabUserInfo.PlayFabId = user.Id;
            IsLoggedIn = true;
            return user;
        }

        /// <summary>
        /// with an existing user, get all data from playfab
        /// </summary>
        /// <param name="user"></param>
        public virtual void CombinedUserLogin(User user)
        {
            GetAccountInfo();
        }

        /// <summary>
        /// set Userdata
        /// </summary>
        /// <param name="data"></param>
        /// <param name="successCallBack"></param>
        /// <param name="errorCallBack"></param>
        public virtual void SetUserData(Dictionary<string, string> data, Action<object> successCallBack = null,
            Action<PlayFabError> errorCallBack = null)
        {
            var request = new UpdateUserDataRequest
            {
                Data = data
            };

            PlayFabClientAPI.UpdateUserData(request, result =>
            {
                Debug.Log("Successfully updated user data");
                playFabUserInfo.DataVersion = result.DataVersion;
                successCallBack?.Invoke(result);
            }, error =>
            {
                Debug.Log("Got error setting user data Ancestor to Arthur");
                Debug.Log(error.ErrorDetails);
                errorCallBack?.Invoke(error);
            });
        }

        /// <summary>
        /// set single
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="successCallBack"></param>
        /// <param name="errorCallBack"></param>
        public virtual void SetUserData(string key, string value, Action<object> successCallBack = null,
            Action<PlayFabError> errorCallBack = null)
        {
            var request = new UpdateUserDataRequest
            {
                Data = new Dictionary<string, string> {{key, value}}
            };

            PlayFabClientAPI.UpdateUserData(request, result =>
            {
                Debug.Log("Successfully updated user data");
                playFabUserInfo.DataVersion = result.DataVersion;
                successCallBack?.Invoke(result);
            }, error =>
            {
                Debug.Log("Got error setting user data Ancestor to Arthur");
                Debug.Log(error.ErrorDetails);
                errorCallBack?.Invoke(error);
            });
        }

        /// <summary>
        /// retrieve userdata from PlayFab
        /// </summary>
        public virtual void GetUserData(Action<object> successCallBack = null,
            Action<PlayFabError> errorCallBack = null)
        {
            var request = new GetUserDataRequest
            {
                PlayFabId = playFabUserInfo.PlayFabId,
                Keys = null
            };
            PlayFabClientAPI.GetUserData(request, result =>
            {
                playFabUserInfo.Data = result.Data;
                playFabUserInfo.DataVersion = result.DataVersion;
                UserDataReceived_Handler(result);
                successCallBack?.Invoke(playFabUserInfo);
            }, error =>
            {
                UserDataErrorReceived_Handler(error);
                errorCallBack?.Invoke(error);
            });
        }

        /// <summary>
        /// get playFab Ids by facebook ids
        /// </summary>
        public virtual void GetPlayFabIDsFromFacebookIDs(List<string> ids,
            Action<List<FacebookPlayFabIdPair>> successCallBack = null, Action<string> errorCallBack = null)
        {
            var request = new GetPlayFabIDsFromFacebookIDsRequest
            {
                FacebookIDs = ids
            };

            PlayFabClientAPI.GetPlayFabIDsFromFacebookIDs(request,
                result => { successCallBack?.Invoke(result.Data); },
                error => { errorCallBack?.Invoke(error.ErrorMessage); }
            );
        }

        /// <summary>
        /// link the facebook account to playFab account
        /// </summary>
        public virtual void LinkFacebookAccount(Action<object> successCallBack = null,
            Action<PlayFabError> errorCallBack = null)
        {
            var request = new LinkFacebookAccountRequest
            {
                AccessToken = AccessToken.CurrentAccessToken.TokenString
            };

            PlayFabClientAPI.LinkFacebookAccount(request, result =>
            {
                Debug.Log("linking the facebook account success");
                successCallBack?.Invoke(true);
            }, error =>
            {
                Debug.Log("Error with linking the facebook account");
                Debug.Log(error.ErrorMessage);
                errorCallBack?.Invoke(error);
            });
        }

        /// <summary>
        /// report a player
        /// </summary>
        /// <param name="reportPlayerId"></param>
        /// <param name="comment"></param>
        /// <param name="successCallBack"></param>
        /// <param name="errorCallBack"></param>
        public virtual void ReportPlayer(string reportPlayerId, string comment, Action<object> successCallBack = null,
            Action<PlayFabError> errorCallBack = null)
        {
            var request = new ReportPlayerClientRequest
            {
                ReporteeId = reportPlayerId,
                Comment = comment
            };

            PlayFabClientAPI.ReportPlayer(request,
                result => { successCallBack?.Invoke(result); }, error =>
                {
                    Debug.Log("Error reporting player");
                    Debug.Log(error.ErrorMessage);
                    errorCallBack?.Invoke(error);
                });
        }

        /// <summary>
        /// send a account recovery email
        /// </summary>
        /// <param name="email"></param>
        /// <param name="successCallBack"></param>
        /// <param name="errorCallBack"></param>
        public virtual void SendAccountRecoveryEmail(string email, Action<object> successCallBack = null,
            Action<PlayFabError> errorCallBack = null)
        {
            var request = new SendAccountRecoveryEmailRequest
            {
                Email = email,
                TitleId = TitleId
            };

            PlayFabClientAPI.SendAccountRecoveryEmail(request,
                result => { successCallBack?.Invoke(result); }, error =>
                {
                    Debug.Log("Error sending recovery email");
                    Debug.Log(error.ErrorMessage);
                    errorCallBack?.Invoke(error);
                });
        }

        /// <summary>
        /// get the LeaderBoard
        /// </summary>
        /// <param name="statisticName"></param>
        /// <param name="startPosition"></param>
        /// <param name="maxResult"></param>
        /// <param name="successCallBack"></param>
        /// <param name="errorCallBack"></param>
        public virtual void GetLeaderBoard(string statisticName, int startPosition, int maxResult,
            Action<object> successCallBack = null,
            Action<PlayFabError> errorCallBack = null)
        {
            var request = new GetLeaderboardRequest
            {
                StatisticName = statisticName,
                StartPosition = startPosition,
                MaxResultsCount = maxResult
            };

            PlayFabClientAPI.GetLeaderboard(request,
                result => { successCallBack?.Invoke(result); }, error =>
                {
                    Debug.Log("Error getting LeaderBoard");
                    Debug.Log(error.ErrorMessage);
                    errorCallBack?.Invoke(error);
                });
        }

        #region Title-Wide-Datamanagement

        /// <summary>
        /// get the catalog items
        /// </summary>
        /// <param name="catalogVersion"></param>
        /// <param name="successCallBack"></param>
        /// <param name="errorCallBack"></param>
        public virtual void GetCatalogItems(string catalogVersion, Action<object> successCallBack = null,
            Action<PlayFabError> errorCallBack = null)
        {
            var request = new GetCatalogItemsRequest
            {
                CatalogVersion = catalogVersion
            };
            PlayFabClientAPI.GetCatalogItems(request,
                result => { successCallBack?.Invoke(result); }, error =>
                {
                    Debug.Log("Error getting catalog items");
                    Debug.Log(error.ErrorMessage);
                    errorCallBack?.Invoke(error);
                });
        }

        /// <summary>
        /// get the publishers data
        /// </summary>
        /// <param name="keys"></param>
        /// <param name="successCallBack"></param>
        /// <param name="errorCallBack"></param>
        public virtual void GetPublisherData(List<string> keys, Action<object> successCallBack = null,
            Action<PlayFabError> errorCallBack = null)
        {
            var request = new GetPublisherDataRequest
            {
                Keys = keys
            };

            PlayFabClientAPI.GetPublisherData(request,
                result => { successCallBack?.Invoke(result); }, error =>
                {
                    Debug.Log("Error getting publisher data");
                    Debug.Log(error.ErrorMessage);
                    errorCallBack?.Invoke(error);
                });
        }

        /// <summary>
        /// get the store items
        /// </summary>
        /// <param name="storeId"></param>
        /// <param name="catalogVersion"></param>
        /// <param name="successCallBack"></param>
        /// <param name="errorCallBack"></param>
        public virtual void GetStoreItems(string storeId, string catalogVersion = null,
            Action<object> successCallBack = null,
            Action<PlayFabError> errorCallBack = null)
        {
            var request = new GetStoreItemsRequest
            {
                StoreId = storeId
            };

            if (catalogVersion != null) request.CatalogVersion = catalogVersion;

            PlayFabClientAPI.GetStoreItems(request,
                result => { successCallBack?.Invoke(result); }, error =>
                {
                    Debug.Log("Error getting store items");
                    Debug.Log(error.ErrorMessage);
                    errorCallBack?.Invoke(error);
                });
        }

        /// <summary>
        /// get the title data
        /// </summary>
        /// <param name="successCallBack"></param>
        /// <param name="errorCallBack"></param>
        public virtual void GetTitleData(Action<object> successCallBack = null,
            Action<PlayFabError> errorCallBack = null)
        {
            var request = new GetTitleDataRequest
            {
                Keys = null
            };

            PlayFabClientAPI.GetTitleData(request,
                result => { successCallBack?.Invoke(result); }, error =>
                {
                    Debug.Log("Error getting title data");
                    Debug.Log(error.ErrorMessage);
                    errorCallBack?.Invoke(error);
                });
        }

        /// <summary>
        /// get news
        /// </summary>
        /// <param name="entries"></param>
        /// <param name="successCallBack"></param>
        /// <param name="errorCallBack"></param>
        public virtual void GetTitleNews(int entries, Action<object> successCallBack = null,
            Action<PlayFabError> errorCallBack = null)
        {
            var request = new GetTitleNewsRequest
            {
                Count = entries
            };

            PlayFabClientAPI.GetTitleNews(request,
                result => { successCallBack?.Invoke(result); }, error =>
                {
                    Debug.Log("Error getting news");
                    Debug.Log(error.ErrorMessage);
                    errorCallBack?.Invoke(error);
                });
        }

        #endregion

        #region Player-Item-Management

        /// <summary>
        /// Add user currency
        /// </summary>
        /// <param name="virtualCurrency"></param>
        /// <param name="amount"></param>
        /// <param name="successCallBack"></param>
        /// <param name="errorCallBack"></param>
        public virtual void AddUserVirtualCurrency(string virtualCurrency, int amount,
            Action<object> successCallBack = null,
            Action<PlayFabError> errorCallBack = null)
        {
            var request = new AddUserVirtualCurrencyRequest
            {
                VirtualCurrency = virtualCurrency,
                Amount = amount
            };

            PlayFabClientAPI.AddUserVirtualCurrency(request, result =>
            {
                playFabUserInfo.SetVirtualCurrency(result.VirtualCurrency, result.Balance);
                successCallBack?.Invoke(result);
            }, error =>
            {
                Debug.Log("Error adding virtual currency");
                Debug.Log(error.ErrorMessage);
                errorCallBack?.Invoke(error);
            });
        }

        /// <summary>
        /// confirm a purchase
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="successCallBack"></param>
        /// <param name="errorCallBack"></param>
        public virtual void ConfirmPurchase(string orderId, Action<object> successCallBack = null,
            Action<PlayFabError> errorCallBack = null)
        {
            var request = new ConfirmPurchaseRequest
            {
                OrderId = orderId
            };

            PlayFabClientAPI.ConfirmPurchase(request,
                result => { successCallBack?.Invoke(result); }, error =>
                {
                    Debug.Log("Error purchasing items");
                    Debug.Log(error.ErrorMessage);
                    errorCallBack?.Invoke(error);
                });
        }

        /// <summary>
        /// consume an item
        /// </summary>
        /// <param name="instanceId"></param>
        /// <param name="consumeCount"></param>
        /// <param name="characterId"></param>
        /// <param name="successCallBack"></param>
        /// <param name="errorCallBack"></param>
        public virtual void ConsumeItem(string instanceId, int consumeCount, string characterId = null,
            Action<object> successCallBack = null,
            Action<PlayFabError> errorCallBack = null)
        {
            var request = new ConsumeItemRequest
            {
                ItemInstanceId = instanceId,
                ConsumeCount = consumeCount
            };
            if (characterId != null) request.CharacterId = characterId;

            PlayFabClientAPI.ConsumeItem(request,
                result => { successCallBack?.Invoke(result); }, error =>
                {
                    Debug.Log("Error consuming item");
                    Debug.Log(error.ErrorMessage);
                    errorCallBack?.Invoke(error);
                });
        }


        /// <summary>
        /// Get a characters inventory
        /// </summary>
        /// <param name="characterId"></param>
        /// <param name="catalogVersion"></param>
        /// <param name="successCallBack"></param>
        /// <param name="errorCallBack"></param>
        public virtual void GetCharacterInventory(string characterId, string catalogVersion = null,
            Action<object> successCallBack = null,
            Action<PlayFabError> errorCallBack = null)
        {
            var request = new GetCharacterInventoryRequest
            {
                CharacterId = characterId
            };
            if (catalogVersion != null) request.CatalogVersion = catalogVersion;

            PlayFabClientAPI.GetCharacterInventory(request, result =>
            {
                PlayFabCharacterInfo playFabCharacterInfo;
                if ((playFabCharacterInfo = playFabUserInfo.GetCharacterInfo(result.CharacterId)) == null)
                {
                    playFabCharacterInfo = new PlayFabCharacterInfo
                    {
                        CharacterId = result.CharacterId,
                        Inventory = result.Inventory,
                        VirtualCurrencyRechargeTimes = result.VirtualCurrencyRechargeTimes,
                        VirtualCurrency = result.VirtualCurrency
                    };
                    playFabUserInfo.CharacterInfos.Add(playFabCharacterInfo);
                }
                else
                {
                    playFabCharacterInfo.Inventory = result.Inventory;
                    playFabCharacterInfo.VirtualCurrencyRechargeTimes = result.VirtualCurrencyRechargeTimes;
                    playFabCharacterInfo.VirtualCurrency = result.VirtualCurrency;
                }

                successCallBack?.Invoke(result);
            }, error =>
            {
                Debug.Log("Error getting character inventory");
                Debug.Log(error.ErrorMessage);
                errorCallBack?.Invoke(error);
            });
        }

        /// <summary>
        /// the the users full inventory
        /// </summary>
        /// <param name="successCallBack"></param>
        /// <param name="errorCallBack"></param>
        public virtual void GetUserInventory(Action<object> successCallBack = null,
            Action<PlayFabError> errorCallBack = null)
        {
            var request = new GetUserInventoryRequest();

            PlayFabClientAPI.GetUserInventory(request, result =>
            {
                playFabUserInfo.Inventory = result.Inventory;
                playFabUserInfo.VirtualCurrencyRechargeTimes = result.VirtualCurrencyRechargeTimes;
                playFabUserInfo.VirtualCurrency = result.VirtualCurrency;

                successCallBack?.Invoke(result);
            }, error =>
            {
                Debug.Log("Error getting user inventory");
                Debug.Log(error.ErrorMessage);
                errorCallBack?.Invoke(error);
            });
        }

        /// <summary>
        /// get a purchase
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="successCallBack"></param>
        /// <param name="errorCallBack"></param>
        public virtual void GetPurchase(string orderId, Action<object> successCallBack = null,
            Action<PlayFabError> errorCallBack = null)
        {
            var request = new GetPurchaseRequest
            {
                OrderId = orderId
            };

            PlayFabClientAPI.GetPurchase(request,
                result => { successCallBack?.Invoke(result); }, error =>
                {
                    Debug.Log("Error getting purchase");
                    Debug.Log(error.ErrorMessage);
                    errorCallBack?.Invoke(error);
                });
        }

        /// <summary>
        ///Pay for a purchase
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="providerName"></param>
        /// <param name="currency"></param>
        /// <param name="providerTransactionId"></param>
        /// <param name="successCallBack"></param>
        /// <param name="errorCallBack"></param>
        public virtual void PayForPurchase(string orderId, string providerName, string currency,
            string providerTransactionId = null, Action<object> successCallBack = null,
            Action<PlayFabError> errorCallBack = null)
        {
            var request = new PayForPurchaseRequest
            {
                OrderId = orderId,
                ProviderName = providerName,
                Currency = currency,
                ProviderTransactionId = providerTransactionId
            };

            PlayFabClientAPI.PayForPurchase(request, result =>
            {
                Debug.Log("Check the return values... this must be altered");
                successCallBack?.Invoke(result);
            }, error =>
            {
                Debug.Log("Error paying for purchase");
                Debug.Log(error.ErrorMessage);
                errorCallBack?.Invoke(error);
            });
        }

        /// <summary>
        /// purchase an item
        /// </summary>
        /// <param name="itemId"></param>
        /// <param name="virtualCurrency"></param>
        /// <param name="price"></param>
        /// <param name="catalogVersion"></param>
        /// <param name="storeId"></param>
        /// <param name="characterId"></param>
        /// <param name="successCallBack"></param>
        /// <param name="errorCallBack"></param>
        public virtual void PurchaseItem(string itemId, string virtualCurrency, int price, string catalogVersion = null,
            string storeId = null, string characterId = null, Action<object> successCallBack = null,
            Action<PlayFabError> errorCallBack = null)
        {
            var request = new PurchaseItemRequest
            {
                ItemId = itemId,
                VirtualCurrency = virtualCurrency,
                Price = price
            };
            if (catalogVersion != null) request.CatalogVersion = catalogVersion;
            if (storeId != null) request.StoreId = storeId;
            if (characterId != null) request.CharacterId = characterId;

            PlayFabClientAPI.PurchaseItem(request, result =>
            {
                if (characterId == null)
                {
                    playFabUserInfo.Inventory.AddRange(result.Items);
                }
                else
                {
                    playFabUserInfo.GetCharacterInfo(characterId).Inventory.AddRange(result.Items);
                }

                successCallBack?.Invoke(result);
            }, error =>
            {
                Debug.Log("Error purchasing item");
                Debug.Log(error.ErrorMessage);
                errorCallBack?.Invoke(error);
            });
        }

        /// <summary>
        /// redeem a coupon
        /// </summary>
        /// <param name="couponCode"></param>
        /// <param name="catalogVersion"></param>
        /// <param name="successCallBack"></param>
        /// <param name="errorCallBack"></param>
        public virtual void RedeemCoupon(string couponCode, string catalogVersion = null,
            Action<object> successCallBack = null,
            Action<PlayFabError> errorCallBack = null)
        {
            var request = new RedeemCouponRequest
            {
                CouponCode = couponCode
            };
            if (catalogVersion != null) request.CatalogVersion = catalogVersion;
            PlayFabClientAPI.RedeemCoupon(request, result =>
            {
                result.GrantedItems.ForEach(playFabUserInfo.Inventory.Add);
                successCallBack?.Invoke(result);
            }, error =>
            {
                Debug.Log("Error redeeming coupon");
                Debug.Log(error.ErrorMessage);
                errorCallBack?.Invoke(error);
            });
        }


        /// <summary>
        /// start a purchase
        /// </summary>
        /// <param name="itemRequests"></param>
        /// <param name="storeId"></param>
        /// <param name="catalogVersion"></param>
        /// <param name="successCallBack"></param>
        /// <param name="errorCallBack"></param>
        public virtual void StartPurchase(List<ItemPurchaseRequest> itemRequests, string storeId = null,
            string catalogVersion = null, Action<object> successCallBack = null,
            Action<PlayFabError> errorCallBack = null)
        {
            var request = new StartPurchaseRequest
            {
                Items = itemRequests
            };
            if (storeId != null) request.StoreId = storeId;
            if (catalogVersion != null) request.CatalogVersion = catalogVersion;

            PlayFabClientAPI.StartPurchase(request, result =>
            {
                Debug.Log("do something with purchased items");
                successCallBack?.Invoke(result);
            }, error =>
            {
                Debug.Log("Error starting purchase");
                Debug.Log(error.ErrorMessage);
                errorCallBack?.Invoke(error);
            });
        }

        /// <summary>
        /// subtract virtual currency
        /// </summary>
        /// <param name="virtualCurrency"></param>
        /// <param name="amount"></param>
        /// <param name="successCallBack"></param>
        /// <param name="errorCallBack"></param>
        public virtual void SubtractUserVirtualCurrency(string virtualCurrency, int amount,
            Action<object> successCallBack = null,
            Action<PlayFabError> errorCallBack = null)
        {
            var request = new SubtractUserVirtualCurrencyRequest
            {
                VirtualCurrency = virtualCurrency,
                Amount = amount
            };

            PlayFabClientAPI.SubtractUserVirtualCurrency(request, result =>
            {
                playFabUserInfo.SetVirtualCurrency(result.VirtualCurrency, result.Balance);
                successCallBack?.Invoke(result);
            }, error =>
            {
                Debug.Log("Error starting purchase");
                Debug.Log(error.ErrorMessage);
                errorCallBack?.Invoke(error);
            });
        }

        /// <summary>
        /// unlock a container
        /// </summary>
        /// <param name="containerItemInstanceId"></param>
        /// <param name="characterId"></param>
        /// <param name="keyItemInstanceId"></param>
        /// <param name="catalogVersion"></param>
        /// <param name="successCallBack"></param>
        /// <param name="errorCallBack"></param>
        public virtual void UnlockContainerInstance(string containerItemInstanceId, string characterId = null,
            string keyItemInstanceId = null, string catalogVersion = null, Action<object> successCallBack = null,
            Action<PlayFabError> errorCallBack = null)
        {
            var request = new UnlockContainerInstanceRequest
            {
                ContainerItemInstanceId = containerItemInstanceId
            };
            if (characterId != null) request.CharacterId = characterId;
            if (keyItemInstanceId != null) request.KeyItemInstanceId = keyItemInstanceId;
            if (catalogVersion != null) request.CatalogVersion = catalogVersion;

            PlayFabClientAPI.UnlockContainerInstance(request, result =>
            {
                if (characterId != null)
                {
                    //fix the virtual currency adding by dictionary
                    playFabUserInfo.GetCharacterInfo(characterId).Inventory.AddRange(result.GrantedItems);
                    playFabUserInfo.GetCharacterInfo(characterId).AddVirtualCurrency(result.VirtualCurrency);
                }
                else
                {
                    playFabUserInfo.Inventory.AddRange(result.GrantedItems);
                    playFabUserInfo.AddVirtualCurrency(result.VirtualCurrency);
                }

                successCallBack?.Invoke(result);
            }, error =>
            {
                Debug.Log("Error unlocking container instance");
                Debug.Log(error.ErrorMessage);
                errorCallBack?.Invoke(error);
            });
        }

        /// <summary>
        /// unlock a container item
        /// </summary>
        /// <param name="containerItemId"></param>
        /// <param name="characterId"></param>
        /// <param name="catalogVersion"></param>
        /// <param name="successCallBack"></param>
        /// <param name="errorCallBack"></param>
        public virtual void UnlockContainerItem(string containerItemId, string characterId = null,
            string catalogVersion = null, Action<object> successCallBack = null,
            Action<PlayFabError> errorCallBack = null)
        {
            var request = new UnlockContainerItemRequest
            {
                ContainerItemId = containerItemId
            };
            if (characterId != null) request.CharacterId = characterId;
            if (catalogVersion != null) request.CatalogVersion = catalogVersion;

            PlayFabClientAPI.UnlockContainerItem(request, result =>
            {
                if (characterId != null)
                {
                    //fix the virtual currency adding by dictionary
                    playFabUserInfo.GetCharacterInfo(characterId).Inventory.AddRange(result.GrantedItems);
                    playFabUserInfo.GetCharacterInfo(characterId).AddVirtualCurrency(result.VirtualCurrency);
                }
                else
                {
                    playFabUserInfo.Inventory.AddRange(result.GrantedItems);
                    playFabUserInfo.AddVirtualCurrency(result.VirtualCurrency);
                }

                successCallBack?.Invoke(result);
            }, error =>
            {
                Debug.Log("Error unlocking container item");
                Debug.Log(error.ErrorMessage);
                errorCallBack?.Invoke(error);
            });
        }

        /// <summary>
        /// create a item purchase request to user in purchasing methods
        /// </summary>
        /// <param name="itemId"></param>
        /// <param name="quantity"></param>
        /// <param name="annotation"></param>
        /// <param name="upgradeFromItems"></param>
        /// <returns></returns>
        public virtual ItemPurchaseRequest CreateItemPurchaseRequest(string itemId, uint quantity,
            string annotation = null, List<string> upgradeFromItems = null)
        {
            var itemPurchaseRequest = new ItemPurchaseRequest
            {
                ItemId = itemId,
                Quantity = quantity
            };
            if (annotation != null) itemPurchaseRequest.Annotation = annotation;
            if (upgradeFromItems != null) itemPurchaseRequest.UpgradeFromItems = upgradeFromItems;
            return itemPurchaseRequest;
        }

        #endregion

        #region Friend-List-Management

        /// <summary>
        /// add a friend
        /// </summary>
        /// <param name="friendPlayFabId"></param>
        /// <param name="friendUsername"></param>
        /// <param name="friendEmail"></param>
        /// <param name="friendTitleDisplayName"></param>
        /// <param name="successCallBack"></param>
        /// <param name="errorCallBack"></param>
        public virtual void AddFriend(string friendPlayFabId = null, string friendUsername = null,
            string friendEmail = null, string friendTitleDisplayName = null,
            Action<object> successCallBack = null, Action<PlayFabError> errorCallBack = null)
        {
            var request = new AddFriendRequest();
            if (friendPlayFabId != null) request.FriendPlayFabId = friendPlayFabId;
            if (friendUsername != null) request.FriendUsername = friendUsername;
            if (friendEmail != null) request.FriendEmail = friendEmail;
            if (friendTitleDisplayName != null) request.FriendTitleDisplayName = friendTitleDisplayName;

            PlayFabClientAPI.AddFriend(request,
                result => { successCallBack?.Invoke(result); }, error =>
                {
                    Debug.Log("Error adding friend");
                    Debug.Log(error.ErrorMessage);
                    errorCallBack?.Invoke(error);
                });
        }

        /// <summary>
        /// remove a friend
        /// </summary>
        /// <param name="friendPlayFabId"></param>
        /// <param name="successCallBack"></param>
        /// <param name="errorCallBack"></param>
        public virtual void RemoveFriend(string friendPlayFabId, Action<object> successCallBack = null,
            Action<PlayFabError> errorCallBack = null)
        {
            var request = new RemoveFriendRequest
            {
                FriendPlayFabId = friendPlayFabId
            };

            PlayFabClientAPI.RemoveFriend(request,
                result => { successCallBack?.Invoke(result); }, error =>
                {
                    Debug.Log("Error removing friend");
                    Debug.Log(error.ErrorMessage);
                    errorCallBack?.Invoke(error);
                });
        }

        /// <summary>
        /// Get the friends list
        /// </summary>
        /// <param name="includeSteamFriends"></param>
        /// <param name="includeFacebookFriends"></param>
        /// <param name="successCallBack"></param>
        /// <param name="errorCallBack"></param>
        public virtual void GetFriendsList(bool includeSteamFriends = false, bool includeFacebookFriends = false,
            Action<object> successCallBack = null, Action<PlayFabError> errorCallBack = null)
        {
            var request = new GetFriendsListRequest
            {
                IncludeFacebookFriends = includeFacebookFriends,
                IncludeSteamFriends = includeSteamFriends
            };

            PlayFabClientAPI.GetFriendsList(request, result =>
            {
                playFabUserInfo.Friends = result.Friends;
                successCallBack?.Invoke(result);
            }, error =>
            {
                Debug.Log("Error starting purchase");
                Debug.Log(error.ErrorMessage);
                errorCallBack?.Invoke(error);
            });
        }

        #endregion

        #region characters

        /// <summary>
        /// get all users characters
        /// </summary>
        /// <param name="successCallBack"></param>
        /// <param name="errorCallBack"></param>
        public virtual void GetAllUsersCharacters(Action<object> successCallBack = null,
            Action<PlayFabError> errorCallBack = null)
        {
            var request = new ListUsersCharactersRequest
            {
                PlayFabId = playFabUserInfo.PlayFabId
            };

            PlayFabClientAPI.GetAllUsersCharacters(request, result =>
            {
                playFabUserInfo.CharacterList = result.Characters;
                successCallBack?.Invoke(result);
            }, error =>
            {
                Debug.Log("Error starting purchase");
                Debug.Log(error.ErrorMessage);
                errorCallBack?.Invoke(error);
            });
        }

        /// <summary>
        /// Get the characters leaderboard
        /// </summary>
        /// <param name="statisticsName"></param>
        /// <param name="startPosition"></param>
        /// <param name="characterType"></param>
        /// <param name="maxResult"></param>
        /// <param name="successCallBack"></param>
        /// <param name="errorCallBack"></param>
        public virtual void GetCharacterLeaderBoard(string statisticsName, int startPosition,
            string characterType = null, int maxResult = -1, Action<object> successCallBack = null,
            Action<PlayFabError> errorCallBack = null)
        {
            var request = new GetCharacterLeaderboardRequest
            {
                StatisticName = statisticsName,
                StartPosition = startPosition
            };
            if (characterType != null) request.CharacterType = characterType;
            if (maxResult != -1) request.MaxResultsCount = maxResult;

            PlayFabClientAPI.GetCharacterLeaderboard(request,
                result => { successCallBack?.Invoke(result); }, error =>
                {
                    Debug.Log("Error starting purchase");
                    Debug.Log(error.ErrorMessage);
                    errorCallBack?.Invoke(error);
                });
        }

        #endregion

        #region Character-Data

        /// <summary>
        ///  get character data
        /// </summary>
        /// <param name="characterId"></param>
        /// <param name="successCallBack"></param>
        /// <param name="errorCallBack"></param>
        public virtual void GetCharacterData(string characterId, Action<object> successCallBack = null,
            Action<PlayFabError> errorCallBack = null)
        {
            var request = new GetCharacterDataRequest
            {
                CharacterId = characterId,
                PlayFabId = playFabUserInfo.PlayFabId
            };

            PlayFabClientAPI.GetCharacterData(request, result =>
            {
                playFabUserInfo.GetCharacterInfo(result.CharacterId).Data = result.Data;
                playFabUserInfo.GetCharacterInfo(result.CharacterId).DataVersion = result.DataVersion;
                successCallBack?.Invoke(result);
            }, error =>
            {
                Debug.Log("Error starting purchase");
                Debug.Log(error.ErrorMessage);
                errorCallBack?.Invoke(error);
            });
        }


        /// <summary>
        /// update user data
        /// </summary>
        /// <param name="characterId"></param>
        /// <param name="data"></param>
        /// <param name="keysToRemove"></param>
        /// <param name="userDataPermission"></param>
        /// <param name="successCallBack"></param>
        /// <param name="errorCallBack"></param>
        public virtual void UpdateCharacterData(string characterId, Dictionary<string, string> data = null,
            List<string> keysToRemove = null, UserDataPermission userDataPermission = UserDataPermission.Private,
            Action<object> successCallBack = null, Action<PlayFabError> errorCallBack = null)
        {
            var request = new UpdateCharacterDataRequest
            {
                CharacterId = characterId,
                Permission = userDataPermission
            };
            if (data != null) request.Data = data;
            if (keysToRemove != null) request.KeysToRemove = keysToRemove;

            PlayFabClientAPI.UpdateCharacterData(request, result =>
            {
                playFabUserInfo.GetCharacterInfo(characterId).DataVersion = result.DataVersion;
                successCallBack?.Invoke(result);
            }, error =>
            {
                Debug.Log("Error starting purchase");
                Debug.Log(error.ErrorMessage);
                errorCallBack?.Invoke(error);
            });
        }

        /// <summary>
        /// method to log an error using a custom string builder.
        /// </summary>
        /// <param name="error"></param>
        public static void ErrorLogger(PlayFabError error)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("An error occured with PlayFab control:");
            stringBuilder.AppendLine();
            stringBuilder.Append(error.ErrorMessage);
            stringBuilder.AppendLine();
            stringBuilder.Append("HTTP Code: " + error.HttpCode + " HTTP Status: " + error.HttpStatus);
            stringBuilder.AppendLine();
            stringBuilder.Append("ErrorDetails:");
            if (error.ErrorDetails != null)
            {
                foreach (KeyValuePair<string, List<string>> keyValuePair in error.ErrorDetails)
                {
                    stringBuilder.Append(keyValuePair.Key + " " + keyValuePair.Value);
                    stringBuilder.AppendLine();
                    for (int i = 1; i < keyValuePair.Value.Count; i++)
                    {
                        stringBuilder.Append(keyValuePair.Value[i].PadRight(keyValuePair.Key.Length + 1));
                        stringBuilder.AppendLine();
                    }
                }
            }

            UnityEngine.Debug.Log(stringBuilder);
        }

        #endregion

        #region event_handlers

        /// <summary>
        /// event handler when  userdata is received
        /// </summary>
        /// <param name="dataResult"></param>
        protected virtual void UserDataReceived_Handler(GetUserDataResult dataResult)
        {
            OnUserDataReceived?.Invoke(dataResult, null);
        }

        /// <summary>
        /// event handler when userdata error is received
        /// </summary>
        /// <param name="error"></param>
        protected virtual void UserDataErrorReceived_Handler(PlayFabError error)
        {
            Debug.Log("Got error retrieving user data:");
            Debug.Log(error.ErrorMessage);
            OnUserDataReceived?.Invoke(null, error);
        }

        #region Login_Handlers

        /// <summary>
        /// event handler for custom id login
        /// </summary>
        /// <param name="playFabResultCommon"></param>
        protected virtual void OnPlayFabUserLogin_Handler(PlayFabResultCommon playFabResultCommon)
        {
            playFabUserInfo.PlayFabId = ((LoginResult) playFabResultCommon).PlayFabId;
            LoginWithId(playFabUserInfo.PlayFabId);
            IsLoggedIn = true;
            OnPlayFabUserLogin?.Invoke((LoginResult) playFabResultCommon, null);
            UpdateCatalogEvent?.Invoke(this, null);
        }

        /// <summary>
        /// playfab register event handler
        /// </summary>
        /// <param name="playFabResultCommon"></param>
        protected virtual void OnPlayFabUserRegister_Handler(RegisterPlayFabUserResult playFabResultCommon)
        {
            playFabUserInfo.PlayFabId = playFabResultCommon.PlayFabId;
            RegisterUser(playFabResultCommon.PlayFabId, playFabResultCommon.Username, null, null);
            //user settings!?
            IsLoggedIn = true;
            OnPlayFabUserLogin?.Invoke(playFabResultCommon, null);
            UpdateCatalogEvent?.Invoke(this, null);
        }

        /// <summary>
        /// event handler for custom id login errors
        /// </summary>
        /// <param name="playFabError"></param>
        protected virtual void OnPlayFabUserLoginError_Handler(PlayFabError playFabError)
        {
            Debug.Log("Error logging in player:");
            Debug.Log(playFabError.ErrorMessage);
            OnPlayFabUserLogin?.Invoke(null, playFabError);
        }

        #endregion

        #endregion
    }
}