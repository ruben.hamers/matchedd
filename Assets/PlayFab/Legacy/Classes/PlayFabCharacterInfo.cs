﻿namespace HamerSoft.PlayFab
{
    /// <summary>
    /// object holding character specific info
    /// </summary>
    public class PlayFabCharacterInfo : PlayfabUserBase
    {
        /// <summary>
        /// Thc character Id
        /// </summary>
        public string CharacterId;
       
    }
}