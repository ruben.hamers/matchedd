﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine;

namespace HamerSoft.PlayFab
{
    /// <summary>
    /// baseclass for playfab user settings
    /// </summary>
    public class PlayfabUserBase
    {
        /// <summary>
        ///  User specific custom data.
        /// </summary>
        public Dictionary<string, UserDataRecord> Data { get; set; }
        /// <summary>
        /// The version of the UserData that was returned.
        /// </summary>
        public uint DataVersion { get; set; }
        /// <summary>
        /// Array of inventory items in the user's current inventory.
        /// </summary>
        public List<ItemInstance> Inventory { get; set; }
        /// <summary>
        /// Array of virtual currency balance(s) belonging to the user.
        /// </summary>
        public Dictionary<string, int> VirtualCurrency { get; set; }

        /// <summary>
        /// Array of remaining times and timestamps for virtual currencies.
        /// </summary>
        public Dictionary<string, VirtualCurrencyRechargeTime> VirtualCurrencyRechargeTimes { get; set; }

        /// <summary>
        /// set virtual currency
        /// </summary>
        /// <param name="virtualCurrency"></param>
        /// <returns></returns>
        public virtual PlayfabUserBase SetVirtualCurrency(string currency, int amount)
        {
            try
            {
                VirtualCurrency[currency] = amount;
            }
            catch (Exception)
            {
                Debug.Log("you are trying to set currency but the key does not exists in the dictionary.");
            }
            return this;
        }

        /// <summary>
        /// add virtual currency
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public virtual PlayfabUserBase AddVirtualCurrency(string key, int value)
        {
            try
            {
                int currentValue = VirtualCurrency[key];
                currentValue += value;
                VirtualCurrency[key] = currentValue;
            }
            catch (Exception)
            {
                Debug.Log("you are trying to set currency but the key does not exists in the dictionary.");
            }
            return this;
        }

        /// <summary>
        /// add virtual currency
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public virtual PlayfabUserBase AddVirtualCurrency(Dictionary<string,uint> currency )
        {
            try
            {
                foreach (KeyValuePair<string, uint> valuePair in currency)
                {
                    uint currentValue = (uint)VirtualCurrency[valuePair.Key];
                    currentValue += valuePair.Value;
                    VirtualCurrency[valuePair.Key] = (int)currentValue;
                }
            }
            catch (Exception)
            {
                Debug.Log("you are trying to set currency but the keys does not exists in the dictionary.");
            }
            return this;
        }

        /// <summary>
        /// add virtual currency
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public virtual PlayfabUserBase AddVirtualCurrency(Dictionary<string, int> currency)
        {
            try
            {
                foreach (KeyValuePair<string, int> valuePair in currency)
                {
                    int currentValue = VirtualCurrency[valuePair.Key];
                    currentValue += valuePair.Value;
                    VirtualCurrency[valuePair.Key] = currentValue;
                }
            }
            catch (Exception)
            {
                Debug.Log("you are trying to set currency but the keys does not exists in the dictionary.");
            }

            return this;
        }

        /// <summary>
        /// Get a type of virtual currency by key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public virtual int GetVirtualCurrency(string key)
        {
            if (VirtualCurrency == null) return 0;
            int output;
            VirtualCurrency.TryGetValue(key, out output);
            return output;
        }
    }
}