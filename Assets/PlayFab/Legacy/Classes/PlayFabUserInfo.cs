﻿using System.Collections.Generic;
using System.Linq;
using PlayFab.ClientModels;

namespace HamerSoft.PlayFab
{
    /// <summary>
    /// class containing all userInfo
    /// </summary>
    public class PlayFabUserInfo : PlayfabUserBase
    {
        /// <summary>
        /// Unique PlayFab identifier of the owner of the combined info.
        /// </summary>
        public string PlayFabId {get;set;}
        /// <summary>
        /// Account information for the user.
        /// </summary>
        public UserAccountInfo AccountInfo {get;set;}
       
        /// <summary>
        /// User specific read-only data.
        /// </summary>
        public Dictionary<string, UserDataRecord> ReadOnlyData {get;set;}
        /// <summary>
        /// The version of the Read-Only UserData that was returned.
        /// </summary>
        public uint ReadOnlyDataVersion {get;set;}
        /// <summary>
        /// list with all characterInfo
        /// </summary>
        public List<PlayFabCharacterInfo> CharacterInfos { get; set; }
        /// <summary>
        /// the friendList
        /// </summary>
        public List<FriendInfo> Friends { get; set; }
        /// <summary>
        /// the list of all characters
        /// </summary>
        public List<CharacterResult> CharacterList { get; set; } 

        /// <summary>
        /// constructor
        /// </summary>
        public PlayFabUserInfo()
        {
            CharacterInfos = new List<PlayFabCharacterInfo>();
        }

        /// <summary>
        /// returns the character info matching the id, else null
        /// </summary>
        /// <param name="characterId"></param>
        /// <returns></returns>
        public virtual PlayFabCharacterInfo GetCharacterInfo(string characterId)
        {
            return CharacterInfos.FirstOrDefault(ci => ci.CharacterId == characterId);
        }
    }
}