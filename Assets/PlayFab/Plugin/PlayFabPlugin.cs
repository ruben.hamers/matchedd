using System;
using HamerSoft.Core;
using MaTcheDD.Entities;
using PlayFab;
using UnityEngine;

namespace HamerSoft.PlayFab
{
    [CreateAssetMenu(fileName = "PlayFabPlugin", menuName = "HamerSoft/PlayFab/PlayFabPlugin Create", order = 0)]
    public class PlayFabPlugin : Plugin
    {
        [SerializeField] private string TitleId;
        public event Action<Player> LoggedIn, LoggedOut;
        public Player ActivePlayer { get; protected set; }

        public string GetTitleId()
        {
            return TitleId;
        }
        public override void Init()
        {
            PlayFabSettings.TitleId = TitleId;
        }

        public override void DeInit()
        {
        }

        public override void ApplicationQuit()
        {
            ActivePlayer = null;
        }

        public override void SetMain(Main main)
        {
            
        }

        public void SetActivePlayer(Player player)
        {
            ActivePlayer = player;
        }
    }
}