using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DefaultNamespace
{
    [Serializable]
    public struct LineOption
    {
        public enum Mode
        {
            Noraml = 0,
            Loop = 1,
            RoundEdge = 2
        }

        public enum LineJointOption
        {
            round,
            intersect
        }

        [Range(0, 1)] public float startRatio;
        [Range(0, 1)] public float endRatio;
        public Mode mode;
        [Range(0.1f, 100f)] public float divideLength;
        [Range(5, 180)] public float divideAngle;

        public Gradient color; //class reference type;

        public LineJointOption jointOption;

        public float DivideAngle
        {
            get { return Mathf.Clamp(divideAngle, 5, 180); }
        }

        public float DivideLength
        {
            get { return Mathf.Clamp(divideLength, 0.1f, 100); }
        }

        public static LineOption Default
        {
            get
            {
                return new LineOption()
                {
                    startRatio = 0f,
                    endRatio = 1f,
                    mode = Mode.Noraml,
                    divideLength = 1f,
                    divideAngle = 10f,
                    color = new Gradient()
                };
            }
        }
    }

    public static class Curve
    {
        public static Vector3 Auto(Vector3 p0, Vector3 c0, Vector3 c1, Vector3 p1, float t)
        {
            t = Mathf.Clamp01(t);
            if (c0 == p0 && c1 == p1)
                return Vector3.Lerp(p0, p1, t);

            if (c0 == p0 || c1 == p1)
                return Quadratic(p0, c0 == p0 ? c1 : c0, p1, t);

            return Cubic(p0, c0, c1, p1, t);
        }

        public static Vector3 Auto(Point n0, Point n1, float t)
        {
            var p0 = n0.position;
            var c0 = n0.NextControlPosition;
            var c1 = n1.PreviousControlPoisition;
            var p1 = n1.position;

            return Auto(p0, c0, c1, p1, t);
        }

        public static Vector3 AutoDirection(Point n0, Point n1, float t)
        {
            var p0 = n0.position;
            var c0 = n0.NextControlPosition;
            var c1 = n1.PreviousControlPoisition;
            var p1 = n1.position;

            return AutoDirection(p0, c0, c1, p1, t);
        }


        public static Vector3 AutoDirection(Vector3 p0, Vector3 c0, Vector3 c1, Vector3 p1, float t)
        {
            var dif0 = p0 - c0;
            var dif1 = p1 - c1;

            if (dif0 == Vector3.zero && dif1 == Vector3.zero)
                return (p1 - p0).normalized;

            if (dif0 == Vector3.zero || dif1 == Vector3.zero)
                return QuadraticDirection(p0, dif0 == Vector3.zero ? c1 : c0, p1, t);
            t = Mathf.Clamp01(t);
            return CubicDirection(p0, c0, c1, p1, t);
        }

        public static Vector3 CubicDirection(Point n0, Point n1, float t)
        {
            var p0 = n0.position;
            var c0 = n0.NextControlPosition;
            var c1 = n1.PreviousControlPoisition;
            var p1 = n1.position;
            return CubicDirection(p0, c0, c1, p1, t);
        }

        public static Vector3 CubicDirection(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t)
        {
            float mt = 1f - t;
            return (3f * mt * mt * (p1 - p0) + 6f * mt * t * (p2 - p1) + 3f * t * t * (p3 - p2));
        }

        public static Vector3 Cubic(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t)
        {
            float mt = 1f - t;
            return p0 * mt * mt * mt + 3f * p1 * mt * mt * t + 3f * p2 * mt * t * t + p3 * t * t * t;
        }

        /* it doesn't use*/
        public static Vector3 QuadraticDirection(Vector3 p0, Vector3 p1, Vector3 p2, float t)
        {
            float mt = 1f - t;
            return 2f * mt * (p1 - p0) + 2f * t * (p2 - p1);
        }

        public static Vector3 Quadratic(Vector3 p0, Vector3 c, Vector3 p1, float t)
        {
            float mt = 1f - t;
            return p0 * mt * mt + 2f * c * mt * t + p1 * t * t;
        }
    }

    public static class CurveLength
    {
        /// <summary>
        /// for test
        /// </summary>
        public static float SumDirections(Point n0, Point n1)
        {
            return SumDirections(n0.position, n0.NextControlPosition, n1.PreviousControlPoisition, n1.position);
        }

        /// <summary>
        /// for test
        /// </summary>
        public static float SumDirections(Vector3 p0, Vector3 c0, Vector3 c1, Vector3 p1)
        {
            var length = 0f;
            var dt = 0.01f;
            for (float t = dt; t < 1f; t += dt)
            {
                var fst = Curve.Auto(p0, c0, c1, p1, t - dt);
                var sec = Curve.Auto(p0, c0, c1, p1, t);

                length += Vector3.Distance(fst, sec);
            }

            return length;
        }


        public static float Auto(Point n0, Point n1)
        {
            return Auto(n0.position, n0.NextControlPosition, n1.PreviousControlPoisition, n1.position);
        }

        public static float Auto(Vector3 p0, Vector3 c0, Vector3 c1, Vector3 p1)
        {
            if (c0 == p0 && p1 == c1)
                return Vector3.Distance(p0, p1);
            if (c0 == p0 || c1 == p1)
                return Quadratic(p0, c0 == p0 ? c1 : c0, p1);

            return QuadraticApproximation(p0, c0, c1, p1);
        }

        /// <summary>
        /// ref : https://github.com/HTD/FastBezier/blob/master/Program.cs
        /// </summary>
        static float Quadratic(Vector3 p0, Vector3 cp, Vector3 p1)
        {
            if (p0 == p1)
                if (p0 == cp)
                    return 0f;
                else
                    return Vector3.Distance(p0, cp);
            if (p0 == cp || p1 == cp)
                return Vector3.Distance(p0, p1);

            Vector3 A0 = cp - p0;
            Vector3 A1 = p0 - 2.0f * cp + p1;

            if (A1 == Vector3.zero)
                return 2.0f * A0.magnitude;

            var c = 4f * Vector3.Dot(A1, A1);
            var b = 8f * Vector3.Dot(A0, A1);
            var a = 4f * Vector3.Dot(A0, A0);
            var q = 4f * a * c - b * b;
            var twoCpB = 2f * c + b;
            var sumCBA = c + b + a;
            var l0 = (0.25f / c) * (twoCpB * Mathf.Sqrt(sumCBA) - b * Mathf.Sqrt(a));

            if (q == 0f)
                return l0;

            var l1 = (q / (8f * Mathf.Pow(c, 1.5f))) *
                     (Mathf.Log(2.0f * Mathf.Sqrt(c * sumCBA) + twoCpB) - Mathf.Log(2.0f * Mathf.Sqrt(c * a) + b));
            return l0 + l1;
        }

        static float QuadraticApproximation(Vector3 p0, Vector3 c0, Vector3 c1, Vector3 p1)
        {
            return Quadratic(p0, (3f * c1 - p1 + 3f * c0 - p0) / 4f, p1);
        }
    }

    [Serializable]
    public struct Point
    {
        public Vector3 position;
        public Vector3 previousControlOffset;
        public Vector3 nextControlOffset;

        [Range(0, 100)] public float width;
        //todo : move normalVector from LineOption to Point.
        //public float normal;

        public Point(Vector3 pos, Vector3 next, Vector3 prev, float width = 2)
        {
            position = pos;
            previousControlOffset = prev;
            nextControlOffset = next;

            this.width = width;
        }

        public Vector3 PreviousControlPoisition
        {
            get { return previousControlOffset + position; }
        }

        public Vector3 NextControlPosition
        {
            get { return nextControlOffset + position; }
        }

        public static Point Zero
        {
            get { return new Point(Vector3.zero, Vector3.zero, Vector3.zero); }
        }
    }

    public struct Triple
    {
        Point previous;
        Point target;
        Point next;
        Color color;

        public Triple(Point p, Point c, Point n, Color cl)
        {
            previous = p;
            target = c;
            next = n;
            color = cl;
        }

        public Vector3 ForwardDirection
        {
            get { return Curve.AutoDirection(target, next, 0f); }
        }

        public Vector3 BackDirection
        {
            get { return Curve.AutoDirection(previous, target, 1f); }
        }

        public Vector3 Position
        {
            get { return target.position; }
        }

        public float CurrentWidth
        {
            get { return target.width; }
        }

        public Color CurrentColor
        {
            get { return color; }
        }
    }


    [Serializable]
    public struct LinePair
    {
        public Point n0;
        public Point n1;
        [NonSerialized] public float sRatio;
        [NonSerialized] public float eRatio;

        public float RatioLength
        {
            get { return eRatio - sRatio; }
        }

        [NonSerialized] public float start;
        [NonSerialized] public float end;

        public LinePair(Point n0, Point n1, float s, float e, float sr, float er)
        {
            this.n0 = n0;
            this.n1 = n1;
            start = s;
            end = e;
            sRatio = sr;
            eRatio = er;
        }

        public float Length
        {
            get { return CurveLength.Auto(n0, n1) * (end - start); }
        }

        public float GetDT(float divideLength)
        {
            return (divideLength / Length) * (end - start);
        }

        public Vector3 GetPoisition(float r)
        {
            return Curve.Auto(n0, n1, Mathf.Lerp(start, end, r));
        }

        public Vector3 GetDirection(float r)
        {
            return Curve.AutoDirection(n0, n1, Mathf.Lerp(start, end, r));
        }

        public float GetWidth(float t)
        {
            return Mathf.Lerp(n0.width, n1.width, Mathf.Lerp(start, end, t));
        }
    }

    [Serializable]
    public partial struct lineStruct : IEnumerable<Point>
    {
        IEnumerable<Point[]> AllPair
        {
            get
            {
                bool first = true;
                Point firstPoint = Point.Zero;
                Point lastPoint = Point.Zero;
                Point prev = Point.Zero;
                int count = 0;

                foreach (var p in this)
                {
                    count++;
                    if (first)
                    {
                        firstPoint = p;
                        lastPoint = p;
                        first = false;
                        continue;
                    }

                    yield return new Point[] {lastPoint, p};

                    lastPoint = p;
                }

                if (option.mode == LineOption.Mode.Loop && count > 1)
                {
                    yield return new Point[] {lastPoint, firstPoint};
                }
            }
        }

        public IEnumerable<LinePair> TargetPairList
        {
            get
            {
                var l = AllLength;
                var ls = l * option.startRatio;
                var le = l * option.endRatio;
                var ps = 0f;
                var pe = 0f;
                var pl = 0f;

                if (ls >= le)
                    yield break;

                foreach (var pair in AllPair)
                {
                    pl = CurveLength.Auto(pair[0], pair[1]);
                    pe = ps + pl;

                    if (le < ps)
                        yield break;
                    if (ls < pe)
                        yield return new LinePair(pair[0], pair[1], Mathf.Max(0f, (ls - ps) / pl),
                            Mathf.Min(1f, (le - ps) / pl), ps / l, pe / l);
                    ps = pe;
                }
            }
        }

        public enum LineMode
        {
            SplineMode,
            BezierMode
        }

        [SerializeField] LineMode mode;
        [SerializeField] LinePair pair;
        [SerializeField] List<Point> points;

        public event Action EditCallBack;
        public MonoBehaviour owner;

        public LineOption option;

        public static lineStruct Default
        {
            get
            {
                return new lineStruct
                {
                    points = new List<Point>() {Point.Zero, new Point(Vector3.right * 10, Vector3.zero, Vector3.zero)},
                    mode = LineMode.SplineMode,
                    pair = new LinePair(Point.Zero, new Point(Vector3.right, Vector3.zero, Vector3.zero), 0, 1, 0, 1),
                    option = LineOption.Default
                };
            }
        }

        Point GetFirstPoint()
        {
            if (mode == LineMode.BezierMode)
                return pair.n0;

            if (points.Count < 1)
                throw new Exception("need more point");

            return points[0];
        }

        Point GetLastPoint()
        {
            if (mode == LineMode.BezierMode)
                return pair.n1;

            if (points.Count < 1)
                throw new Exception("need more point");

            return points[points.Count - 1];
        }

        int GetCount()
        {
            if (mode == LineMode.BezierMode)
                return 2;

            return points.Count;
        }

        public Vector3 GetPosition(float ratio)
        {
            ratio = Mathf.Clamp01(ratio);

            var cl = ratio * Length;

            foreach (var pair in TargetPairList)
            {
                if (cl > pair.Length)
                    cl -= pair.Length;
                else
                    return pair.GetPoisition(cl / pair.Length);
            }

            return option.mode == LineOption.Mode.Loop ? GetFirstPoint().position : GetLastPoint().position;
        }

        public Vector3 GetDirection(float ratio)
        {
            ratio = Mathf.Clamp01(ratio);
            var cl = ratio * Length;
            Vector3 dir = Vector3.zero;
            foreach (var pair in TargetPairList)
            {
                dir = pair.GetDirection(cl / pair.Length);
                if (cl > pair.Length)
                    cl -= pair.Length;
                else
                    break;
            }

            return dir;
        }

        public IEnumerator<Point> GetEnumerator()
        {
            if (mode == LineMode.BezierMode)
            {
                yield return pair.n0;
                yield return pair.n1;
            }
            else
            {
                foreach (var p in points)
                    yield return p;
            }
        }

        public IEnumerable<Triple> TripleList
        {
            get
            {
                if (GetCount() < 2)
                    yield break;

                var mode = option.mode;
                var sr = option.startRatio;
                var er = option.endRatio;
                var color = option.color;


                var l = AllLength;
                var ls = sr * l;
                var le = er * l;
                var c = 0f;

                var fB = Point.Zero;
                var ff = true;
                var sB = Point.Zero;
                var sf = true;

                var index = 0;
                foreach (var p in TripleEnumerator())
                {
                    if (ff)
                    {
                        ff = false;
                        fB = p;
                        continue;
                    }

                    if (sf)
                    {
                        if (mode == LineOption.Mode.Loop && sr == 0f && er == 1f)
                            yield return new Triple(GetLastPoint(), GetFirstPoint(), p, color.Evaluate(0));

                        sf = false;
                        sB = p;
                        continue;
                    }

                    c += CurveLength.Auto(fB, sB);
                    if (ls < c && c < le)
                    {
                        if (index == GetCount() - 1 && mode != LineOption.Mode.Loop)
                            break;

                        yield return new Triple(fB, sB, p, color.Evaluate(c / l));
                    }

                    fB = sB;
                    sB = p;
                    index++;
                }
            }
        }

        public IEnumerable<Point> TripleEnumerator()
        {
            if (mode == LineMode.BezierMode)
            {
                yield return pair.n0;
                yield return pair.n1;
            }
            else
            {
                foreach (var p in points)
                    yield return p;

                if (option.mode == LineOption.Mode.Loop)
                {
                    yield return points[0];
                }
            }
        }


        public float AllLength
        {
            get
            {
                var length = 0f;
                foreach (var pair in AllPair)
                    length += CurveLength.Auto(pair[0], pair[1]);

                return length;
            }
        }

        public float Length
        {
            get
            {
                var length = 0f;
                foreach (var pair in TargetPairList)
                    length = pair.Length;

                return length;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    public interface IUnitSize
    {
        Vector2 Size { get; }
    }

    public interface IMesh
    {
        IEnumerable<Vertex> Vertices { get; }
        IEnumerable<int> Triangles { get; }
    }

    public interface IMeshDrawer
    {
        IEnumerable<IMesh> Draw();
    }

    public struct Quad : IMesh
    {
        Vertex _p0;
        Vertex _p1;
        Vertex _p2;
        Vertex _p3;


        public Quad(Vertex p0, Vertex p1, Vertex p2, Vertex p3)
        {
            _p0 = p0;
            _p1 = p1;
            _p2 = p2;
            _p3 = p3;
        }

        public Quad(Vector2 size, Vector2 center, Color color)
        {
            _p0 = Vertex.New(center - new Vector2(-size.x, -size.y) / 2f, new Vector2(0, 0), color);
            _p1 = Vertex.New(center - new Vector2(size.x, -size.y) / 2f, new Vector2(0, 0), color);
            _p2 = Vertex.New(center - new Vector2(-size.x, size.y) / 2f, new Vector2(0, 0), color);
            _p3 = Vertex.New(center - new Vector2(size.x, size.y) / 2f, new Vector2(0, 0), color);
        }

        public IEnumerable<Vertex> Vertices
        {
            get
            {
                yield return _p0;
                yield return _p1;
                yield return _p2;
                yield return _p3;
            }
        }

        public IEnumerable<int> Triangles
        {
            get
            {
                var list = new int[] {0, 2, 1, 1, 2, 3};
                foreach (var number in list)
                    yield return number;
            }
        }
    }

    public struct Vertex
    {
        public Vector3 position;
        public Vector2 uv;
        public Color color;

        public Vertex(Vector3 pos, Vector2 u, Color c)
        {
            position = pos;
            uv = u;
            color = c;
        }

        public static Vertex New(Vector3 pos, Vector2 uv, Color color)
        {
            return new Vertex(pos, uv, color);
        }

        public static Vertex New(float x, float y, float z, float u, float v, Color color)
        {
            return new Vertex(new Vector3(x, y, z), new Vector2(u, v), color);
        }
    }

    public struct Triangle : IMesh
    {
        Vertex _p0;
        Vertex _p1;
        Vertex _p2;

        public Triangle(Vertex p0, Vertex p1, Vertex p2)
        {
            _p0 = p0;
            _p1 = p1;
            _p2 = p2;
        }

        public IEnumerable<Vertex> Vertices
        {
            get
            {
                yield return _p0;
                yield return _p1;
                yield return _p2;
            }
        }

        public IEnumerable<int> Triangles
        {
            get
            {
                var list = new int[] {0, 2, 1};
                foreach (var number in list)
                    yield return number;
            }
        }
    }

    public class BaseLine : Image
    {
        public lineStruct line;

        /// <summary>
        /// hard copy.
        /// </summary>
        public lineStruct Line
        {
            get { return line; }
        }

        bool m_geometryUpdateFlag = false;

        IEnumerable<IMesh> _mesh;

        IEnumerable<IMesh> Mesh
        {
            get { return _mesh ?? (_mesh = DrawerFactory); }
        }

        protected override void Awake()
        {
            base.Awake();
        }

        public void GeometyUpdateFlagUp()
        {
            m_geometryUpdateFlag = true;
        }

        public void LateUpdate()
        {
            if (m_geometryUpdateFlag)
            {
                UpdateGeometry();
                m_geometryUpdateFlag = false;
            }
        }

        public void ModifyMesh(Mesh mesh)
        {
            using (var vh = new VertexHelper(mesh))
            {
                ModifyMesh(vh);
                vh.FillMesh(mesh);
            }
        }

        public void ModifyMesh(VertexHelper verts)
        {
            verts.Clear();

            Queue<int> buffer = new Queue<int>();

            foreach (var m in Mesh)
            {
                foreach (var t in m.Triangles)
                {
                    buffer.Enqueue(t);
                    if (buffer.Count == 3)
                    {
                        var c = verts.currentVertCount;
                        verts.AddTriangle(c + buffer.Dequeue(), c + buffer.Dequeue(), c + buffer.Dequeue());
                    }
                }

                foreach (var v in m.Vertices)
                    verts.AddVert(v.position, v.color, v.uv);
            }
        }


        IEnumerable<IMesh> m_Drawer = null;

        protected virtual IEnumerable<IMesh> DrawerFactory
        {
            get { return m_Drawer ?? (m_Drawer = LineBuilder.Factory.Normal(this, transform).Draw()); }
        }

        protected override void Start()
        {
            base.Start();
            line.owner = this;
            line.EditCallBack += GeometyUpdateFlagUp;
        }

        public static BaseLine CreateLine(Transform parent = null)
        {
            var go = new GameObject("UILine");
            if (parent != null)
                go.transform.SetParent(parent);
            go.transform.localPosition = Vector3.zero;

            var line = go.AddComponent<BaseLine>();
            line.line = lineStruct.Default;
            line.Start();
            return line;
        }
    }

    public class RoundCapDrawer
    {
        BaseLine _line;

        public RoundCapDrawer(BaseLine target)
        {
            _line = target;
        }

        public IEnumerable<IMesh> Build(LinePair pair, bool isEnd)
        {
            var normal = Vector3.back;

            var Line = _line.Line; // hardCopy.

            var divideAngle = Line.option.DivideAngle;

            var t = isEnd ? 1f : 0f;

            var color = Line.option.color.Evaluate(isEnd ? Line.option.endRatio : Line.option.startRatio);
            var position = pair.GetPoisition(t);

            var radian = pair.GetWidth(t);

            var direction = pair.GetDirection(isEnd ? 1 : 0) * (isEnd ? -1 : 1);

            var wv = Vector3.Cross(direction, normal).normalized * radian;

            var dc = Mathf.Max(1, Mathf.Floor(180f / divideAngle));
            var da = 180f / dc;
            var rot = Quaternion.Euler(-normal * da);


            var uv = new Vector2[] {new Vector2(0, 1), new Vector2(1, 1), Vector2.zero, new Vector2(1, 0)};
            if (_line is Image && ((Image) _line).sprite != null)
                uv = ((Image) _line).sprite.uv;

            var center = (uv[0] + uv[1] + uv[2] + uv[3]) / 4f;

            for (float a = 0f; a < 179f; a += da)
            {
                var v0 = Vertex.New(position, center, color);
                var v1 = Vertex.New(position + wv, (!isEnd ? a > 90 : a < 90) ? uv[1] : uv[0], color);
                var v2 = Vertex.New(position + rot * wv, (!isEnd ? a > 90 : a < 90) ? uv[3] : uv[2], color);
                yield return new Triangle(v0, v1, v2);

                wv = rot * wv;
            }
        }
    }

    public class LineBuilder : IMeshDrawer
    {
        readonly NormalBezierDrawer _bezierDrawer;
        readonly NormalJointDrawer _jointDrawer;
        readonly RoundCapDrawer _capDrawer;
        readonly BaseLine _line;

        public LineBuilder(NormalBezierDrawer b, NormalJointDrawer j, RoundCapDrawer c, BaseLine line)
        {
            _bezierDrawer = b;
            _jointDrawer = j;
            _capDrawer = c;
            _line = line;
        }

        public IEnumerable<IMesh> Draw()
        {
            if (_line.Line.option.endRatio - _line.Line.option.startRatio <= 0)
                yield break;

            //todo : merge Pairlist with TripleList to one iteration.
            var ff = true;
            LinePair last = new LinePair();
            foreach (var pair in _line.Line.TargetPairList)
            {
                if (ff)
                {
                    ff = false;
                    if (_line.Line.option.mode == LineOption.Mode.RoundEdge) // && pairList.Count() > 0)
                    {
                        foreach (var mesh in _capDrawer.Build(pair, false))
                        {
                            yield return mesh;
                        }
                    }
                }


                foreach (var mesh in _bezierDrawer.Build(pair))
                {
                    yield return mesh;
                }

                last = pair;
            }

            foreach (var triple in _line.Line.TripleList)
            {
                var joint = _jointDrawer;

                foreach (var mesh in joint.Build(triple))
                {
                    yield return mesh;
                }
            }

            if (_line.Line.option.mode == LineOption.Mode.RoundEdge)
            {
                foreach (var mesh in _capDrawer.Build(last, true))
                {
                    yield return mesh;
                }
            }
        }

        public class Factory
        {
            public static IMeshDrawer Normal(BaseLine line, Transform trans)
            {
                var builder = new LineBuilder
                (
                    new NormalBezierDrawer(line),
                    new NormalJointDrawer(line),
                    new RoundCapDrawer(line),
                    line
                );

                return builder;
            }
        }
    }

    public class NormalJointDrawer
    {
        readonly BaseLine _line;

        public NormalJointDrawer(BaseLine target)
        {
            _line = target;
        }

        public IEnumerable<IMesh> Build(Triple triple)
        {
            ///부채꼴에서 가운데점
            var p0 = triple.Position;

            var bd = triple.BackDirection;
            var fd = triple.ForwardDirection;
            //부채꼴에서 양끝점.
            Vector3 p1;
            Vector3 p2;
            var nv = Vector3.back;

            if ((Vector3.Cross(bd, fd).normalized + nv).magnitude < nv.magnitude)
            {
                p1 = p0 + Vector3.Cross(nv, bd).normalized * triple.CurrentWidth;
                p2 = p0 + Vector3.Cross(nv, fd).normalized * triple.CurrentWidth;
            }
            else
            {
                p1 = p0 - Vector3.Cross(nv, fd).normalized * triple.CurrentWidth;
                p2 = p0 - Vector3.Cross(nv, bd).normalized * triple.CurrentWidth;
            }

            var angle = Vector3.Angle(p1 - p0, p2 - p0);
            var dc = Mathf.Max(1, Mathf.Floor(angle / _line.Line.option.DivideAngle));
            var da = angle / dc;

            var rot = Quaternion.Euler(-nv * da);
            var d = p1 - p0;

            var uv = new Vector2[] {new Vector2(0, 1), new Vector2(1, 1), Vector2.zero, new Vector2(1, 0)};
            if (_line is Image && ((Image) _line).sprite != null)
                uv = ((Image) _line).sprite.uv;

            var center = (uv[0] + uv[1] + uv[2] + uv[3]) / 4f;

            for (float a = 0f; a < angle; a += da)
            {
                var v0 = Vertex.New(p0, center, triple.CurrentColor);
                var v1 = Vertex.New(p0 + d, uv[1], triple.CurrentColor);
                var v2 = Vertex.New(p0 + rot * d, uv[3], triple.CurrentColor);
                yield return new Triangle(v0, v1, v2);

                d = rot * d;
            }
        }
    }

    public class NormalBezierDrawer
    {
        readonly BaseLine _line;

        public NormalBezierDrawer(BaseLine line)
        {
            _line = line;
        }

        public IEnumerable<IMesh> Build(LinePair pair)
        {
            var LineData = _line.Line; //hard copy.

            float dt = pair.GetDT(LineData.option.DivideLength);
            Vector3 prv1 = Vector3.zero;
            Vector3 prv2 = Vector3.zero;
            /// todo : too complicate need split code for test or something.
            for (float t = pair.start; t < pair.end; t += dt)
            {
                var ws = Mathf.Lerp(pair.n0.width, pair.n1.width, t);
                var we = Mathf.Lerp(pair.n0.width, pair.n1.width, t + dt);

                var ps = Curve.Auto(pair.n0, pair.n1, t);

                var pe = Curve.Auto(pair.n0, pair.n1, Mathf.Min(pair.end, t + dt));


                var cs = LineData.option.color.Evaluate(pair.sRatio + t * pair.RatioLength);
                var ce = LineData.option.color.Evaluate(pair.sRatio + (t + dt) * pair.RatioLength);

                var d = pe - ps;
                var wd = Vector3.Cross(d, Vector3.back).normalized;
                var wds = t == 0f
                    ? Vector3.Cross(Curve.AutoDirection(pair.n0, pair.n1, 0), Vector3.back).normalized
                    : wd;
                var wde = Vector3.Cross(pair.GetDirection(1f), Vector3.back).normalized;

                var uv = new Vector2[] {new Vector2(0, 1), new Vector2(1, 1), Vector2.zero, new Vector2(1, 0)};

                if (_line is Image && ((Image) _line).sprite != null)
                    uv = ((Image) _line).sprite.uv;

                var p0 = Vertex.New(t == pair.start ? ps + wds * ws : prv1, uv[0], cs);
                var p1 = Vertex.New(t == pair.start ? ps - wds * ws : prv2, uv[1], cs);

                var end = Mathf.Abs(t - pair.end) < dt;

                var p2 = Vertex.New(end ? pe + wde * we : pe + wd * we, uv[2], ce);
                var p3 = Vertex.New(end ? pe - wde * we : pe - wd * we, uv[3], ce);

                prv1 = pe + wd * we;
                prv2 = pe - wd * we;

                yield return new Quad(p0, p1, p2, p3);
            }
        }
    }
}